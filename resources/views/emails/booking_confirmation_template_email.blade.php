<html>
<head>
</head>

<body>

<div class="main-wrapper" style="width: 800px; margin:0 auto;">

     <div style="width: 800px;"><img src="{{asset('images/email-banner.jpg')}}?v={{Config::get('version.mail_img')}}"  width="800" height="303" alt="" /></div>
     
     <div style="border-left: 1px solid #ccc; border-right: 1px solid #ccc; width: 798px;">
	 <div style="width: 600px; height:auto; margin: 0 auto; padding: 0px 0px 0px 0px;">
     
     <div style="width: 600px; height:auto; padding: 0px 0px 30px 0px; margin-bottom: 30px;">
          <p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding:20px 0px 0px 0px; font-weight: bold; margin:0px;">Dear {{$customer->customer_name}},</p>
		  <p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 0px 15px 0px; margin:0px;">Thank you for your Payment!</p>
          <p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 0px 15px 0px; margin:0px;">Welcome to Dubai Housekeeping Cleaning Services. Thank You for booking with us. You can find your booking ID below. We hope you have a great experience with our services...</p>
		  @if(isset($online) && ($online != null ) && ($online->transaction_id != '0' && $online->transaction_id !='' ))
		  <p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 0px 15px 0px; font-weight: bold; margin:0px;">Payment transaction id - {{$online->transaction_id}}></p>
		  @endif
          <p style="font-family: Tahoma, Geneva, sans-serif; font-size:15px; line-height:20px; color: #FFF; padding: 12px 0px 0px 0px; margin:0px;">
          <span style="font-size:15px; background: #62a6c9; padding: 12px 30px; border-radius: 50px; margin:0px; font-weight: bold;">Booking Reference ID - {{$bookings->reference_id}}</span>
          </p>
     </div>
     
     
     <div style="width: 560px; height:auto; background: #fafafa; border:1px solid #e5e2e2; padding: 20px 20px; ">
          <p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding:0px 0px 0px 0px; font-weight: bold; margin:0px;">Address Summary</p>
          <p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding:5px 0px 0px 0px; margin:0px;">Address : {{$customerAddress->customer_address}}<br />
            
             Area -  {{$areaName}}</p>
     </div>
     
     
     
     <div style="width: 600px; height:auto; background: #fafafa; border:1px solid #e5e2e2; border-top:0px; padding:0px 0px;">
          <table width="600" border="0" cellspacing="0" cellpadding="0" bordercolor="#FFF">
		  <tr style="background: #FFF;">
              <td width="298"><p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px;">Service Name</p></td>
              <td width="4">:</td>
              <td width="298"><p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px;">{{$services->service_type_name}}</p></td>
            </tr>
			@if($services->service_type_name == 'Maid Service')
		    <tr style="background: #FFF;">
              <td width="298"><p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px;">No of Maids</p></td>
              <td width="4">:</td>
              <td width="298"><p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px;">{{$bookings->no_of_maids}}</p></td>
            </tr>
			@endif
			<tr style="  ">
              <td><p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px;">Service Date & Time</p></td>
              <td style="">:</td>
			  <?php
                                            $date = @$bookings->service_start_date;
                                            
                                            $old_date_timestamp = strtotime($date);

											$dateFormat = date('l jS, F',$old_date_timestamp); 
											if(@$bookings->booking_type == 'OD') {
												$val = 'one-time';
											} else if($bookings->booking_type == 'WE') {
												$val = 'weekly';
											} else {
												$val = 'bi-weekly';
											}
											$start = date("g:i a", strtotime(@$bookings->time_from)); 
											$end = date("g:i a", strtotime(@$bookings->time_to)); 
                                            ?>
              <td><p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px;">{{$dateFormat}} ({{$val}}) <br/>{{$start}} - {{$end}}</p></td>
            </tr>
			
            
            <tr style="background: #FFF;">
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px;">Crew In</p></td>
              <td style="border-top:1px solid #e5e2e2;">:</td>
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px;">{{$bookings->crew_in}}</p></td>
            </tr>
            
            
            <tr>
              <td colspan="3">
              <p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px 0px; margin:0px;">Instructions</p>
              
              <p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 5px 20px 15px; margin:0px;">{{$bookings->booking_note}}</p></td>
            </tr>
			@if($services->service_type_name == 'Maid Service')

			<?php
			if($bookings->cleaning_material=="Y"): ?>
			<tr style="background: #FFF;">
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px;">Cleaning Material</p></td>
              <td style="border-top:1px solid #e5e2e2;">:</td>
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px;">Yes</p></td>
            </tr>
			<?php endif; ?>
			
			<?php
			if($bookings->interior_window_clean == 1)
			{
				$intrior = "Yes";
			} else {
				$intrior = "No";
			}
			if($bookings->fridge_cleaning  == 1)
			{
				$fridge = "Yes";
			} else {
				$fridge = "No";
			}
			if($bookings->ironing_services  == 1)
			{
				$ironing = "Yes";
			} else {
				$ironing = "No";
			}
			if($bookings->oven_cleaning  == 1)
			{
				$oven = "Yes";
			} else {
				$oven = "No";
			}
			?>
			<?php
			if($bookings->interior_window_clean == 1 || $bookings->fridge_cleaning == 1 || $bookings->ironing_services == 1 || $bookings->oven_cleaning == 1)
			{
			?>
			<tr>
              <td colspan="3">
              <p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px 0px; margin:0px;">Extra Services</p>
              
              <p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 5px 20px 15px; margin:0px;">
			  
			  <?php
				if($bookings->interior_window_clean == 1)
				{
				?>
				Interior Windows - <?php echo $intrior; ?><br>
				<?php } ?>
				<?php
				if($bookings->fridge_cleaning == 1)
				{
				?>
				Fridge Cleaning - <?php echo $fridge; ?>&nbsp;<br>
				<?php
				}
				?>
				<?php
				if($bookings->ironing_services  == 1)
				{
				?>
				Ironing - <?php echo $ironing; ?>&nbsp;<br>
				<?php } ?>
				<?php
				if($bookings->oven_cleaning  == 1)
				{
				?>
				Oven Cleaning - <?php echo $oven; ?><br />
				<?php
				}
				?>
			  
			  </p></td>
            </tr>
			<?php
			}
			?>
			
			
			<tr style="background: #FFF;">
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px;">Total Hours</p></td>
              <td style="border-top:1px solid #e5e2e2;">:</td>
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px;">{{$bookings->no_of_hrs}}</p></td>
            </tr>
			@endif
            
            <tr>
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px;">Payment Method</p></td>
              <td style="border-top:1px solid #e5e2e2;">:</td>
			  <?php $payby=ucfirst($bookings->pay_by);?>
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px;">{{$payby}}</p></td>
            </tr>
			
			
			
		
			<tr>
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px;">Total Cost</p></td>
              <td style="border-top:1px solid #e5e2e2;">:</td>
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px;">AED {{@$bookings->service_charge}}</p></td>
            </tr>
			
			<tr>
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px;">Vat Cost</p></td>
              <td style="border-top:1px solid #e5e2e2;">:</td>
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px;">AED {{@$bookings->vat_charge}}</p></td>
            </tr>
           
            <tr style="background: #FFF;">
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px;">Net Payable</p></td>
              <td style="border-top:1px solid #e5e2e2;">:</td>
              <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; font-weight: bold; padding: 12px 10px; margin:0px;">AED {{@$bookings->total_amount}}</p></td>
            </tr>
          </table>
     </div>
     
     
     
     
     <div style="width: 600px; height:auto; padding: 20px 0px 0px 0px; margin-bottom: 30px; text-align: center;">
          <p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 0px 15px 0px; margin:0px;">
          <strong>Grace Quest Cleaning Services</strong><br />
          P. O. Box : 236887, Office #4201B,<br />
          ASPiN Commercial Tower,Sheikh Zayed Road,<br />
		  Dubai, United Arab Emirates<br />
          For Bookings : +971 4 263 1976<br />
          Email : office@dubaihousekeeping.com</p> 
     </div>
     
     

     
     </div>
     
<div style="width: 798px; height:auto; padding: 0px 0px 0px 0px; background: #fafafa; text-align: center;">
          <p style="font-family: Tahoma, Geneva, sans-serif; font-size:12px; color: #555; line-height:20px; padding: 12px 0px 10px 0px; margin:0px;">© <?php echo date('Y'); ?> Dubai Housekeeping All Rights Reserved.</p>
     </div>

</div>
	 
	 </div>


</body>
</html>
