@extends('emails.layouts.main')
@section('content')
    @include('emails.includes.text_body')
    @include('emails.includes.contact-mail-section')
    @include('emails.includes.footer_app_inks')
    @include('emails.includes.footer_banner')
    @include('emails.includes.footer_social_media_section')
@endsection
