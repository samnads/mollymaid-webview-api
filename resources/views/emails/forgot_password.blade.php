

<html>
    <head>
    </head>
	<body>
		<div class="main-wrapper" style="width: 800px; margin:0 auto;">
			<div style="width: 800px;"><img src="{{asset('images/email-banner.jpg')}}?v={{Config::get('version.mail_img')}}" width="800" height="303" alt="" /></div>
			<div style="border-left: 1px solid #ccc; border-right: 1px solid #ccc; border-bottom: 1px solid #ccc; width: 798px;">
			<div style="width: 600px; height:auto; margin: 0 auto; padding: 0px 0px 0px 0px;">
				<div style="width: 600px; height:auto; padding: 0px 0px 30px 0px; margin-bottom: 30px;">
					<p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding:20px 0px 0px 0px; font-weight: bold; margin:0px;">Dear {{$name}},</p>
					<p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 0px 15px 0px; margin:0px;">We received a request to get your password on Dubai Housekeeping Cleaning Services.</p>
					<p style="font-family: Tahoma, Geneva, sans-serif; font-size:15px; line-height:20px; color: #FFF; padding: 12px 0px 0px 0px; margin:0px;">
						<span style="font-size:15px; background: #62a6c9; padding: 12px 30px; border-radius: 50px; margin:0px; font-weight: bold;">Your  password - {{$password}}</span>
					</p>
				</div>
				<div style="width: 600px; height:auto; padding: 20px 0px 0px 0px; margin-bottom: 30px; text-align: center;">
					<p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 0px 15px 0px; margin:0px;">
					<strong>Grace Quest Cleaning Services</strong><br />
          P. O. Box : 236887, Office #4201B,<br />
          ASPiN Commercial Tower,Sheikh Zayed Road,<br />
		  Dubai, United Arab Emirates<br />
          For Bookings : +971 4 263 1976<br />
          Email : office@dubaihousekeeping.com</p> 
				</div>
			</div>
 
			<div style="width: 798px; height:auto; padding: 0px 0px 0px 0px; background: #fafafa; text-align: center;">
				<p style="font-family: Tahoma, Geneva, sans-serif; font-size:12px; color: #555; line-height:20px; padding: 12px 0px 10px 0px; margin:0px;">© <?php echo date('Y'); ?> Dubai Housekeeping All Rights Reserved.</p>
			</div>
			</div>
		</div>
	</body>
</html>


