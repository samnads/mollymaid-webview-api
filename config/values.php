<?php

return [
    'vat_amount' => env('VAT_AMOUNT'),
    'cleaning_amount' => env('CLEANING_AMOUNT'),
    'to_mail' => env('MAIL_TO'),
    'mail_admin' => env('MAIL_ADMIN'),
    'house_cleaning' => env('HOUSE_CLEANING'),
    'disinfection' => env('DISINFECTION'),
    'deep_cleaning' => env('DEEP_CLEANING'),
    'carpet' => env('CARPET_CLEANING'),
    'sofa' => env('SOFA_CLEANING'),
    'mattress' => env('MATTRESS_CLEANING'),
    'web_url' => env('WEB_URL'),
    'pay_by_cash_charge' => env('PAY_BY_CASH_CHARGE'),
    'company_name' => env('COMPANY_NAME', "Default Company Name"),
    'file_server_url' => env('FILE_SERVER_URL'),
    'web_app_url' => env('WEB_APP_URL'),
    'telr_auth_key' => env('TELR_AUTH_KEY'),
    'telr_store_id' => env('TELR_STORE_ID'),
    'telr_test_environment' => env('TELR_TEST_ENVIRONMENT'),
];
