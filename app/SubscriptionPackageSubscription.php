<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionPackageSubscription extends Model
{
     protected $table = 'package_subscription';
    public $timestamps = false;
    protected $primaryKey = 'id';
}
