<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserActivity extends Model
{
    protected $table = 'user_activity';
    public $timestamps = false;
    protected $primaryKey = 'activity_id';
}
