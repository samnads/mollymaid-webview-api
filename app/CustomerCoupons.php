<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerCoupons extends Model
{
    protected $table = 'customer_coupons';
    public $timestamps = false;
}
