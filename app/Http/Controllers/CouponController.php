<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CustomerCoupons;
use App\CouponCode;
use App\Bookings;
use Illuminate\Support\Facades\Config;

class CouponController extends Controller
{
    function checkCoupon(Request $request)
    {
        $coupon = $request['coupon'];
        $selectedAmount = $request['selectedAmount'];
        $bookedDate = $request['bookedDate'];
        $serviceId = $request['serviceId'];
        $today = date('Y-m-d');   
        $customerId = $request['customerId'];
        $no_hrs = $request['numberHours'];
        $no_maids = $request['no_maids'];
        $cleaningStatus = $request['cleaningStatus'];
        //if($customerId == '') {
            //$vatAmount =  number_format(($selectedAmount*(Config::get('values.vat_amount')/100)),2,'.', '');
            //$total = number_format(($vatAmount + $selectedAmount),2,'.', '');     
            //$msg = 'User not logged in.';
            //return response()->json([
                //'status' => "failed",
                //'data' => array('vat_charge' => $vatAmount ,'gross_amount' => $total),
                //'message' =>$msg
                //]);
        //}
       
        $couponCode = CouponCode::where('coupon_name',$coupon)->where('service_id',$serviceId)
            ->where('type','C')
            ->where('status',1)->where('expiry_date' ,'>=',$today)->first();
        if($couponCode && $request['booking_type'] == 'OD')   {
            if($serviceId != $couponCode->service_id)
            {
                $vatAmount =  number_format(($selectedAmount*(Config::get('values.vat_amount')/100)),2,'.', '');
                $total = number_format(($vatAmount + $selectedAmount),2,'.',''); 
                return response()->json([
                    'status' => "failed",
                    'message' =>'Coupon not valid for the selected service!',
                    'data' =>array('vat_charge' => $vatAmount ,'gross_amount' => $total),
                    ]);
            }
            if($no_hrs && ($couponCode->min_hrs !=0) && $no_hrs<$couponCode->min_hrs)
            {
                $vatAmount =  number_format(($selectedAmount*(Config::get('values.vat_amount')/100)),2,'.', '');
                $total = number_format(($vatAmount + $selectedAmount),2,'.',''); 
                return response()->json([
                    'status' => "failed",
                    'message' =>'Coupon only valid for minimum '.$couponCode->min_hrs.' hours booking!',
                    'data' =>array('vat_charge' => $vatAmount ,'gross_amount' => $total),
                    ]);
            }
            $coupon_type=$couponCode->coupon_type;
            $expiry_date=$couponCode->expiry_date;
            $offer_type=$couponCode->offer_type;
            if($expiry_date >= $bookedDate)
            {
                if($coupon_type == "FT")
                {
                    if($customerId != "")
                    {
                        $checkbooking =Bookings::where('customer_id',$customerId)->get();
                        if(count($checkbooking )!= 0)
                        {
                            $msg = 'Coupon valid for first booking only.';
                            $vatAmount =  number_format(($selectedAmount*(Config::get('values.vat_amount')/100)),2,'.','');
                            $total = number_format(($vatAmount + $selectedAmount),2,'.',''); 
                            return response()->json([
                                'status' => "failed",
                                'data' =>array('vat_charge' => $vatAmount ,'gross_amount' => $total),
                                'message' =>$msg
                                ]);
                        }
                    }
                }
                $weekday = date('w', strtotime($bookedDate));
    
                $v_week_day = $couponCode->valid_week_day;
                $weekArray = explode(',', $v_week_day);
                if( in_array( $weekday ,$weekArray ) )
                {
                    if ( ($serviceId ==  Config::get('values.house_cleaning')) && $offer_type == "P")
                    {
                        if($cleaningStatus == "N")
                        {
                            $cleaning_material_rate = 0;
                        } else {
                            $cleaning_material_rate = 10;
                        } 
                        $cleaning_rates_new = ((($no_hrs * $cleaning_material_rate) * $no_maids));
                        $perhrrate = number_format($couponCode->percentage,2,'.','');
                        $aservicerate_new = ((($no_hrs * $perhrrate) * $no_maids));
                        $serviceratenew = ($aservicerate_new + $cleaning_rates_new);

                        $discount = ($selectedAmount - $serviceratenew);
                        $getfee = number_format(($selectedAmount - $discount),2,'.','');
                        $total_service_rate = $getfee;
                        $vat_charge =  number_format(($getfee*(Config::get('values.vat_amount')/100)),2,'.','');
                        $gross_amount = number_format(($getfee + $vat_charge),2,'.','');
                        $msg = 'Coupon applied successfully.';
                        return response()->json([
                            'status' => "success",
                            'data' => array('vat_charge' => $vat_charge ,'gross_amount' => $gross_amount,'net_amount'=>$total_service_rate,'discount'=>$discount,'coupon_id' => $couponCode->coupon_id),
                            'message' =>$msg
                            ], 200);
                    }
                    if($couponCode->discount_type == 0) { //percenatge
                        $discountPercenatge = $couponCode->percentage;
                        $discount = number_format((($discountPercenatge/100)*$selectedAmount),2,'.','');
                    } else {
                        $discount = number_format($couponCode->percentage,2,'.','');
                    }
                    $getfee = number_format(($selectedAmount - $discount),2,'.','');
                    $total_service_rate = $getfee;
                    $vat_charge =  number_format(($getfee*(Config::get('values.vat_amount')/100)),2,'.','');
                    $gross_amount = number_format(($getfee + $vat_charge),2,'.','');
                    $msg = 'Coupon applied successfully.';
                    return response()->json([
                        'status' => "success",
                        'data' => array('vat_charge' => $vat_charge ,'gross_amount' => $gross_amount,'net_amount'=>$total_service_rate,'discount'=>$discount,'coupon_id' => $couponCode->coupon_id),
                        'message' =>$msg
                        ], 200);
                } else {
                    $vatAmount =  number_format(($selectedAmount*(Config::get('values.vat_amount')/100)),2,'.','');
                    $total = number_format(($vatAmount + $selectedAmount),2,'.',''); 
                    $days = [];
                    foreach($weekArray as $week)
                    {
                        if((int)$week == 0) {
                            $days[] =' Sunday'; 
                        }
                        if((int)$week == 1) {
                            $days[] =' Monday'; 
                        }
                        if((int)$week == 2) {
                            $days[] =' Tuesday'; 
                        }
                        if((int)$week == 3) {
                            $days[] =' Wednesday'; 
                        }
                        if((int)$week == 4) {
                            $days[] =' Thursday'; 
                        }
                        if((int)$week == 5) {
                            $days[] =' Friday'; 
                        }
                        if((int)$week == 6) {
                            $days[] =' Saturday'; 
                        }
                    }

                    $dayArray= implode(',', $days);
                    $msg = 'This voucher code is only valid for bookings that take place on '.$dayArray.'.';
                    // $msg = 'Coupon not valid for selected day.';
                    return response()->json([
                        'status' => "failed",
                        'data' => array('vat_charge' => $vatAmount ,'gross_amount' => $total),
                        'message' =>$msg
                        ]);
                }
            } else {
                $vatAmount =  number_format(($selectedAmount*(Config::get('values.vat_amount')/100)),2,'.','');
                $total = number_format(($vatAmount + $selectedAmount),2,'.',''); 
                $msg = 'Coupon expired.';
                return response()->json([
                    'status' => "failed",
                    'data' => array('vat_charge' => $vatAmount ,'gross_amount' => $total),
                    'message' =>$msg
                    ]);
            }
        } else {
            $vatAmount =  number_format(($selectedAmount*(Config::get('values.vat_amount')/100)),2,'.','');
            $total = number_format(($vatAmount + $selectedAmount),2,'.',''); 
            $msg = 'Coupon not valid.';
            return response()->json([
                'status' => "failed",
                'data' => array('vat_charge' => $vatAmount ,'gross_amount' => $total,'input' => $request->all()),
                'message' =>$msg
                ]);
        }
    }
}
