<?php

namespace App\Http\Controllers;

use App\CustomerSupport;
use Illuminate\Http\Request;
use DB;

class ContactUsController extends Controller
{
    public function contact_form_send(Request $request)
    {
        $response['request'] = $request->all();
        try {   
            //throw new \Exception('Test custom exception.');
            DB::beginTransaction();
            $CustomerSupport = new CustomerSupport();
            $CustomerSupport->email = $request->email;
            $CustomerSupport->phone = $request->phone;
            $CustomerSupport->message = $request->message;
            $CustomerSupport->save();
            /********************************************************* */
            $response['status'] = true;
            $response['message'] = "Your enquiry send successfully !";
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $response['status'] = false;
            $response['message'] = "Failed to send, please try again !";
            $response['error'] = $e->getMessage();
        }
        return response()->json($response, 200, array(), JSON_PRETTY_PRINT);
    }
}
