<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SubscriptionPackageSubscription;
use App\SubscriptionPackage;
use App\SubscriptionPackagePayment;
use App\Customers;
use App\CustomerAddress;
use App\Area;





use Response;

class SubscriptionPackageSubscriptionController extends Controller
{
    public function getSubscriptions()
    {
        return 'This API Endpoint Disabled !';
        $packages = SubscriptionPackageSubscription::where('status', 1)->get();
        return Response::json($packages, 200, ['Content-type' => 'application/json; charset=utf-8'], JSON_PRETTY_PRINT);
    }
    public function getSubscriptionById($id)
    {
        return 'This API Endpoint Disabled !';
        $subscription = SubscriptionPackageSubscription::where('status', 1)->where('id', $id)->first();
        return Response::json($subscription, 200, ['Content-type' => 'application/json; charset=utf-8'], JSON_PRETTY_PRINT);
    }
    public function getSubscriptionsByPackageId($id)
    {
        return 'This API Endpoint Disabled !';
        $subscriptions = SubscriptionPackageSubscription::where('status', 1)->where('package_id', $id)->get();
        return Response::json($subscriptions, 200, ['Content-type' => 'application/json; charset=utf-8'], JSON_PRETTY_PRINT);
    }
    public function getSubscriptionData($subId)
    {
        try {
            $data['booking'] = SubscriptionPackageSubscription::where('id', $subId)->first();
            $data['package'] = SubscriptionPackage::where('package_id', $data['booking']['package_id'])->first();
            $data['payment'] = SubscriptionPackagePayment::where('package_subscription_id', $data['booking']['id'])->where('status', 1)->first();
            $data['customer'] = Customers::where('customer_id', $data['booking']['customer_id'])->first();
            $data['address'] = CustomerAddress::where('customer_id', $data['booking']['customer_id'])->first();
            $data['area'] = Area::find($data['address']['area_id']);
            return response()->json(['status' => 'success', 'message' => 'Subscription Data Retrieved successfully !', 'data' => $data], 200);
        } catch (\Exception $e) {
          return response()->json(['status' => 'error', 'message' => $e->getMessage(), 'data' => $data], 200);
        }
    }
	public function getSubscriptionDataNew($subId)
    {
        try {
            $data['booking'] = SubscriptionPackageSubscription::where('reference_id', $subId)->first();
            $data['package'] = SubscriptionPackage::where('package_id', $data['booking']['package_id'])->first();
            $data['payment'] = SubscriptionPackagePayment::where('package_subscription_id', $data['booking']['id'])->where('status', 1)->first();
            $data['customer'] = Customers::where('customer_id', $data['booking']['customer_id'])->first();
            $data['address'] = CustomerAddress::where('customer_id', $data['booking']['customer_id'])->first();
            $data['area'] = Area::find($data['address']['area_id']);
            return response()->json(['status' => 'success', 'message' => 'Subscription Data Retrieved successfully !', 'data' => $data], 200);
        } catch (\Exception $e) {
          return response()->json(['status' => 'error', 'message' => $e->getMessage(), 'data' => $data], 200);
        }
    }
    public function updatePackageMailStatus(Request $request)
    {
        // for saving mail send or not
        // helpful for stop sending duplicate mails
        $booking = SubscriptionPackageSubscription::find($request['package_subscription_id']);
        if ($request['mail_package_confirmation_to_customer_status'] != null) {
            $booking->mail_package_confirmation_to_customer_status = $request['mail_package_confirmation_to_customer_status'];
        }
        if ($request['mail_package_confirmation_to_admin_status'] != null) {
            $booking->mail_package_confirmation_to_admin_status = $request['mail_package_confirmation_to_admin_status'];
        }
        $booking->save();
        return response()->json([
            'status' => "success",
            'message' => 'Package mail(s) send status updated!',
        ], 200, array(), JSON_PRETTY_PRINT);
    }
}
