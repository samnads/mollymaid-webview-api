<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Config;
use Response;
use App\OnlinePayment;
use App\Bookings;
use App\CustomerPayments;

class TelrPaymentController extends Controller
{
    public function create_order_test(Request $request)
    {
        $storeid = "29320";  //Store ID here
        $authkey = Config::get('values.telr_auth_key');
        $cartid_1 = 'TESTing_' . rand(10, 199999999);
        $params = [
            'method' => 'create',
            'store' => $storeid,
            'authkey' => $authkey,
            'framed' => '0',
            "language" => "en",
            "ivp_applepay" => "1",
            "order" => [
                "cartid" => $cartid_1,
                "test" => "1",
                "amount" => "1",
                "currency" => "AED",
                "description" => "My purchase",
                "trantype" => "sale"
            ],
            "customer" => [
                "ref" => "custref11",
                "email" => "samnad.s@azinova.info",
                "name" => [
                    "forenames" => "Samnad",
                    "surname" => "S"
                ],
                "address" => [
                    "line1" => "5 th street, address line 234, next address",
                    "city" => "Riyadh",
                    "country" => "UAE"
                ],
                "phone" => "568678589"
            ],
            "return" => [
                "authorised" => "https://www.mysite.com/authorised",
                "declined" => "https://www.mysite.com/declined",
                "cancelled" => "https://www.mysite.com/cancelled"
            ]
        ];
        //printing request
        //echo '<pre>';
        //print_r($params);
        //echo '</pre>';
        $client = new \GuzzleHttp\Client(['verify' => false]);
        $response = $client->request('POST', 'https://secure.telr.com/gateway/order.json', [
            'body' => json_encode($params),
            'headers' => [
                'Content-Type' => 'application/json',
                'accept' => 'application/json',
            ],
        ]);
        $response_body['response'] = json_decode((string) $response->getBody(), true);
        return Response::json($response_body, 200, [], JSON_PRETTY_PRINT);
    }
    public function process_order(Request $request)
    {
        try {
            /**
             * 
             * process order status from here
             * send email or other tasks
             * 
             */
            $response['status'] = 'success';
            $response['request'] = $request->all();
            $online_payment = OnlinePayment::find($request->payment_id);
            $response['online_payment'] = $online_payment;
            /*************************************************** */
            $params = [
                'method' => 'check',
                'store' => Config::get('values.telr_store_id'),
                'authkey' => Config::get('values.telr_auth_key'),
                "order" => [
                    "ref" => $online_payment->transaction_id,
                ],
            ];
            $client = new \GuzzleHttp\Client(['verify' => false]);
            $response1 = $client->request('POST', 'https://secure.telr.com/gateway/order.json', [
                'body' => json_encode($params),
                'http_errors' => false,
                'headers' => [
                    'Content-Type' => 'application/json',
                    'accept' => 'application/json',
                ],
            ]);
            $response['telr'] = json_decode((string) $response1->getBody(), true);
            if(@$response['telr']['order']['status']['code'] == 3){
                /**
                 * 
                 * save transaction sttaus
                 * send mail to admin or customer
                 * 
                 */
                $booking = Bookings::where('booking_id', $online_payment->booking_id)->first();
                $online = OnlinePayment::where('transaction_id', $online_payment->transaction_id)->first();
                if ($online && $online->payment_status != "success") {
                    $online->transaction_id = $response['telr']['order']['transaction']['ref'];
                    $online->payment_status = "success";
                    $online->ip_address = $request->ip();
                    $online->user_agent = $request->header('User-Agent');
                    $online->post_data = json_encode($params);
                    $online->return_data = json_encode($response['telr']);
                    $online->save();
                    if ($booking->booking_status == 3 || $booking->booking_status == 0) {
                        $booking->booking_status = 0;
                    }
                    $booking->save();
                    /***************************************************************** */
                    // save also in customer payments
                    $customerPay = new CustomerPayments();
                    $customerPay->verified_status = 1;
                    $customerPay->customer_id = $online_payment->customer_id;
                    $customerPay->paid_amount = $online_payment->amount;
                    $customerPay->paid_at = 'O';
                    if ($online->reference_id == '') {
                        $customerPay->ps_no = 'MM-ON/'.date('Y').'/' . $response['telr']['order']['transaction']['ref'];;
                        //$customerPay->receipt_no = 'DHK-ON/2021/' . $refid;

                    } elseif ($online->reference_id != '') {
                        $customerPay->ps_no = $online->reference_id;
                        //$customerPay->receipt_no = $online->reference_id;
                    }
                    $customerPay->receipt_no = $response['telr']['order']['transaction']['ref'];;
                    $customerPay->paid_at_id = 1;
                    $customerPay->payment_method = 1;
                    $customerPay->day_service_id = 0;
                    $customerPay->paid_datetime = date('Y-m-d H:i:s');
                    $customerPay->balance_amount = $online->amount;
                    $customerPay->save();
                    /***************************************************************** */
                }
                $mail = new AdminApiMailController;
                $mail->booking_confirmation_to_customer($booking->booking_id);
            }
            /*************************************************** */
        } catch (\GuzzleHttp\Exception\ServerException $e) {
            $response['status'] = 'failed';

        }catch (\Exception $e) {
            $response['status'] = 'failed';
            throw new \ErrorException($e->getMessage());
        }
        return Response::json($response, 200, [], JSON_PRETTY_PRINT);
    }
}
