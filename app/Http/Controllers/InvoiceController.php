<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bookings;
use App\ServiceType;
use App\ExtraServices;
use App\CouponCode;
use App\BookingSlots;
use App\CustomerAddress;
use App\BookMapping;
use App\Customers;
use App\OnlinePayment;
use App\CustomerPayments;
use App\Area;
use App\CustomerCoupons;
use App\BookingCancel;
use App\UserActivity;
use DateTime;
use Illuminate\Support\Facades\Config;
use DateInterval;
use Illuminate\Support\Facades\Mail;
use App\Mail\SuccessMailAdmin;
use App\Mail\SuccessMail;
use App\Http\Controllers\LoginController;



class InvoiceController extends Controller
{
    /**
     * function to save payment details
     * Author:Karthika
     * Date:04/09/2020
     */
    public function makePayment(Request $request)
    {
        $customerId = $request['customerId'];
        $payment_mode = $request['payment_mode'];
        $instructions = $request['instructions'];
        $crew_in = $request['crew_in'];
        $serviceId = $request['serviceId'];
        $noOfHours = $request['noOfHours'];
        $cleaningMaterialStatus = $request['cleaningMaterialStatus'];
        $noOfMaids = $request['noOfMaids'];
        // $extraServices=$request['extraServices'];
        $how_often = $request['how_often'];
        $cleaning_date = $request['cleaning_date'];
        $toTime = $request['toTime'];
        $fromTime = $request['fromTime'];
        $monthDurations = $request['monthDurations'];
        $hour_rate = $request['hour_rate'];
        // $discount=$request['discount'];
        $service_charge = $request['service_charge'];
        $vat_charge = $request['vat_charge'];
        $total_amount = $request['total_amount'];
        // $net_cleaning_fee=$request['net_cleaning_fee'];
        // $net_service_charge=$request['net_service_charge'];
        // $net_discount=$request['net_discount'];
        // $net_vat_charge=$request['net_vat_charge'];
        // $total_net_amount=$request['total_net_amount'];
        $cleaning_material_fee = $request['cleaning_material_fee'];
        $coupon_code = $request['coupon_code'];
        $interior = $request['interior'];
        $ironing = $request['ironing'];
        $fridge = $request['fridge'];
        $oven = $request['oven'];


        $startDate = new DateTime($cleaning_date);

        $dateIntervel = new DateTime($cleaning_date);
        if ($how_often != 'OD') {
            if ($monthDurations == '') {
                $monthDurations = 1;
            }
            if ($monthDurations == 'Continue') {
                $actual_ending_date = $startDate;
                $ending_date = $startDate;
            } else {
                $dateIntervel->add(new DateInterval('P' . $monthDurations . 'M'));
                $actual_ending_date = $dateIntervel->format('Y-m-d');
                $ending_date = $dateIntervel->format('Y-m-d');  // 2016-01-02
                $endDate = new DateTime($ending_date);
            }
        } else {
            $actual_ending_date = $startDate;
            $ending_date = $startDate;
        }

        $bookings = new Bookings();
        $bookings->booking_note = $instructions;
        $bookings->booking_type = $how_often;
        $bookings->booking_category = 'C';
        if ($monthDurations == 'Continue') {
            $bookings->service_end = 0;
        } else {
            $bookings->service_end = 1;
        }
        $bookings->service_end_date = $ending_date;
        $bookings->service_actual_end_date = $actual_ending_date;
        $bookings->pending_amount = 0;
        $bookings->discount = 0;
        if ($payment_mode == 'cash') {
            $bookings->total_amount =  $request['total_amount'] + 5;
        } else {
            $bookings->total_amount =  $request['total_amount'];
        }
        $bookings->price_per_hr = $request['hour_rate'];
        $bookings->service_charge = $request['service_charge'];
        $bookings->vat_charge = $request['vat_charge'];
        $bookings->net_cleaning_fee = 0;
        $bookings->net_service_charge = 0;
        $bookings->net_discount = 0;
        $bookings->crew_in = $crew_in;
        $bookings->net_vat_charge = 0;
        $bookings->total_net_amount = 0;
        $bookings->cleaning_material_fee = $request['cleaning_material_fee'];

        $bookings->month_durations = $request['monthDurations']; //new field
        $bookings->service_start_date = $request['cleaning_date']; //
        $bookings->service_week_day = date('w', strtotime($request['cleaning_date']));

        $bookings->time_from = $fromTime; //
        $bookings->time_to = $toTime; //
        $bookings->customer_id = $customerId; //
        $customer = Customers::where('customer_id', $customerId)->first();
        $customerAddress = CustomerAddress::where('customer_id', $customerId)->where('default_address',1)->where('address_status',0)->first();
        $bookings->customer_address_id = $customerAddress['customer_address_id'];
        // if($request['extraServices'] != 0) {
        // $extraService = ExtraServices::find($request['extraServices']);
        // $extraServiceName = $extraService->service;
        if ($ironing == 2) {
            $bookings->ironing_services = 1;
        }
        if ($fridge == 1) {
            $bookings->fridge_cleaning = 1;
        }
        if ($oven == 3) {
            $bookings->oven_cleaning = 1;
        }
        if ($interior == 4) {
            $bookings->interior_window_clean = 1;
        }
        // }
        $bookings->service_type_id = $request['serviceId'];
        $bookings->no_of_maids = $request['noOfMaids'];
        $bookings->no_of_hrs = $request['noOfHours'];
        $bookings->cleaning_material = $request['cleaningMaterialStatus'];
        $dt = new DateTime;
        $bookings->reference_id = '';
        $bookings->maid_id = 0; //
        $bookings->is_locked = 0;
        $bookings->booked_by = 1;
       if($request['is_device']=='mobile') {
		    $bookings->booked_from = 'M';
		} else if($request['is_device']=='web') {
            $bookings->booked_from = 'W';
		}

        $bookings->booking_status = 0;
        $bookings->booked_datetime = $dt->format('y-m-d H:i:s');
        $bookings->save();
        $referenceNumber = sprintf('%04d', $bookings->booking_id);
        $bookings->reference_id = "DHK-".date("Y")."-".$referenceNumber;
        $bookings->coupon_id = $request['coupon_code'];
        $bookings->save();
        if ($request['coupon_code'] != 0) {
            $customerCoupon = new CustomerCoupons();
            $customerCoupon->customer_id = $customerId;
            $customerCoupon->coupon_id = $request['coupon_code'];
            $customerCoupon->reference_id = $bookings->reference_id;
            $customerCoupon->discount = $request['discount_price'];
            $customerCoupon->save();
            $bookings->discount = $request['discount_price'];
            $bookings->save();
        }
        if ($serviceId != Config::get('values.house_cleaning')) //not equal to house cleaning
        {
            // $category_id=$request['category_id'];
            // $sub_category_id=$request['sub_category_id'];
            // $service_sub_funish_id=$request['service_sub_funish_id'];
            // $is_scrubbing=$request['is_scrubbing'];
            // $name=$request['name'];
            // $service_cost=$request['service_cost'];
            // $status=$request['status'];
            $cate_first = unserialize($request['cate_first']);
            // if($serviceId != Config::get('values.carpet') && $serviceId != Config::get('values.disinfection') && $serviceId != Config::get('values.deep_cleaning')) {
            //     $cate_fourth = unserialize($request['cate_fourth']); //4 seal L sofa
            //     if($serviceId == Config::get('values.sofa')) {
            //         $cate_fifth = unserialize($request['cate_fifth']); //  1 seat sofa
            //         $cate_sixth = unserialize($request['cate_sixth']); // 2 seat sofa
            //     }
            // }
            // if($serviceId != Config::get('values.disinfection') && $serviceId != Config::get('values.deep_cleaning') ) {
            //     $cate_second = unserialize($request['cate_second']);
            //     $cate_third = unserialize($request['cate_third']);
            // }
            if ($cate_first['category_id1'] != '0') {
                $bookingMap = new BookMapping();
                $bookingMap->category_id = $cate_first['category_id1'];
                $bookingMap->sub_category_id = $cate_first['sub_category_id1'];
                $bookingMap->service_sub_furnish_id = $cate_first['service_sub_funish_id1'];
                $bookingMap->name = $cate_first['name1'];
                $bookingMap->is_scrubbing = $cate_first['is_scrubbing1'];
                $bookingMap->service_cost = $cate_first['service_cost1'];
                $bookingMap->status = $cate_first['status1'];
                $bookingMap->service_id     = $serviceId;
                if ($cate_first['name1'] == 1 && $request['officesqure'] != 0 && $cate_first['service_sub_funish_id1'] == 0) {
                    $bookingMap->squarefeet_count =  $request['officesqure'];
                }
                $bookingMap->booking_id =  $bookings->booking_id;
                $bookingMap->save();
            }
            // if($serviceId != Config::get('values.disinfection') && $serviceId != Config::get('values.deep_cleaning')  && $cate_second['category_id2'] != '0') {
            //     $bookingMap = new BookMapping();
            //     $bookingMap->category_id = $cate_second['category_id2'];
            //     $bookingMap->sub_category_id = $cate_second['sub_category_id2'];
            //     $bookingMap->service_sub_furnish_id = $cate_second['service_sub_funish_id2'];
            //     $bookingMap->name = $cate_second['name2'];
            //     $bookingMap->is_scrubbing = $cate_second['is_scrubbing2'];
            //     $bookingMap->service_cost = $cate_second['service_cost2'];
            //     $bookingMap->status = $cate_second['status2'];
            //     $bookingMap->service_id	 = $serviceId;
            //     $bookingMap->booking_id =  $bookings->booking_id;
            //     $bookingMap->save();
            // }
            // if( $serviceId != Config::get('values.disinfection') && $serviceId != Config::get('values.deep_cleaning') && $cate_third['category_id3'] != '0') {
            //     $bookingMap = new BookMapping();
            //     $bookingMap->category_id = $cate_third['category_id3'];
            //     $bookingMap->sub_category_id = $cate_third['sub_category_id3'];
            //     $bookingMap->service_sub_furnish_id = $cate_third['service_sub_funish_id3'];
            //     $bookingMap->name = $cate_third['name3'];
            //     $bookingMap->is_scrubbing = $cate_third['is_scrubbing3'];
            //     $bookingMap->service_cost = $cate_third['service_cost3'];
            //     $bookingMap->status = $cate_third['status3'];
            //     $bookingMap->service_id	 = $serviceId;
            //     $bookingMap->booking_id =  $bookings->booking_id;
            //     $bookingMap->save();
            // }
            //     if($serviceId != Config::get('values.carpet') && $serviceId != Config::get('values.disinfection') && $serviceId != Config::get('values.deep_cleaning') ) {
            //         if($cate_fourth['category_id4'] != '0') {
            //             $bookingMap = new BookMapping();
            //             $bookingMap->category_id = $cate_fourth['category_id4'];
            //             $bookingMap->sub_category_id = $cate_fourth['sub_category_id4'];
            //             $bookingMap->service_sub_furnish_id = $cate_fourth['service_sub_funish_id4'];
            //             $bookingMap->name = $cate_fourth['name4'];
            //             $bookingMap->is_scrubbing = $cate_fourth['is_scrubbing4'];
            //             $bookingMap->service_cost = $cate_fourth['service_cost4'];
            //             $bookingMap->status = $cate_fourth['status4'];
            //             $bookingMap->service_id	 = $serviceId;
            //             $bookingMap->booking_id =  $bookings->booking_id;
            //             $bookingMap->save();
            //         }
            //         if($serviceId == Config::get('values.sofa')) {

            //             if($cate_fifth['category_id5'] != '0') {
            //                 $bookingMap = new BookMapping();
            //                 $bookingMap->category_id = $cate_fifth['category_id5'];
            //                 $bookingMap->sub_category_id = $cate_fifth['sub_category_id5'];
            //                 $bookingMap->service_sub_furnish_id = $cate_fifth['service_sub_funish_id5'];
            //                 $bookingMap->name = $cate_fifth['name5'];
            //                 $bookingMap->is_scrubbing = $cate_fifth['is_scrubbing5'];
            //                 $bookingMap->service_cost = $cate_fifth['service_cost5'];
            //                 $bookingMap->status = $cate_fifth['status5'];
            //                 $bookingMap->service_id	 = $serviceId;
            //                 $bookingMap->booking_id =  $bookings->booking_id;
            //                 $bookingMap->save();
            //             }
            //             if($cate_sixth['category_id6'] != '0') {
            //                 $bookingMap = new BookMapping();
            //                 $bookingMap->category_id = $cate_sixth['category_id6'];
            //                 $bookingMap->sub_category_id = $cate_sixth['sub_category_id6'];
            //                 $bookingMap->service_sub_furnish_id = $cate_sixth['service_sub_funish_id6'];
            //                 $bookingMap->name = $cate_sixth['name6'];
            //                 $bookingMap->is_scrubbing = $cate_sixth['is_scrubbing6'];
            //                 $bookingMap->service_cost = $cate_sixth['service_cost6'];
            //                 $bookingMap->status = $cate_sixth['status6'];
            //                 $bookingMap->service_id	 = $serviceId;
            //                 $bookingMap->booking_id =  $bookings->booking_id;
            //                 $bookingMap->save();
            //             }
            //         }
            //     }
        }
        if ($payment_mode == 'card') {
            // $transaction_charg = ((($bookings->total_amount * 0.03) + 1) * 1.05);
            // $transaction_charge = number_format((float)$transaction_charg, 2, '.', '');
            $onlinePayment = new OnlinePayment();
            $onlinePayment->booking_id = $bookings->booking_id;
            $onlinePayment->reference_id = $bookings->reference_id;
            $onlinePayment->amount = $bookings->total_amount;
            // $onlinePayment->transaction_charge = $transaction_charge;
            $onlinePayment->customer_id = $bookings->customer_id;
            $onlinePayment->payment_status = 'initiated';
           if($request['is_device']=='mobile') {
                $onlinePayment->paid_from = 'M';
		} else if($request['is_device']=='web') {
                  $onlinePayment->paid_from = 'W';
		}
            $onlinePayment->payment_datetime = $dt->format('y-m-d H:i:s');
            $onlinePayment->user_agent = '';
            $onlinePayment->post_data = '';
            $onlinePayment->save();
            $bookings->pay_by = 'Card';
            $bookings->booking_status = 3; // status 0 when payment done
            $bookings->save();
            $transaction_charge = 0;
        } else {
            $transaction_charge = 10;
            $bookings->pay_by = 'Cash';
            $bookings->save();
        }

        // if ($postRequest) {
        //     // $this->load->helper('homemaid_helper');
        //     $data = array();
        //     $working_key = 'E0AA717A68B31B45396ACC639787A58F'; //Shared by CCAVENUES
        //     $data['access_code'] = 'AVNC02FJ87BJ11CNJB'; //Shared by CCAVENUES
        //     $merchant_data = '';

        //     foreach ($postRequest as $key => $value) {
        //         $merchant_data .= $key . '=' . $value . '&';
        //     }

        //     $data['encrypted_data'] = PaymentController::encrypt($merchant_data, $working_key); // Method for encrypting the data.  
        //     // $view = view('make_payment',$data);
        //     // $views = $view->render(); // Hello, World!
        //     // $this->load->view('make_payment', $data);
        // }
        //    }
        return response()->json(
            [
                'status' => 'success',
                'messages' => 'Booking saved successfully.',
                'data' => ['bookings' => $bookings, 'transaction_charge' => $transaction_charge, 'customer' => $customer, 'customer_address' => $customerAddress, 'payment_mode' => $payment_mode],
            ],
            200
        );
    }
    // public function encrypt($plainText,$key)
    // {  
    //     // return $plainText;
    //     $secretKey = PaymentController::hextobin(md5($key));
    // 	$initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
    //   	$openMode = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '','cbc', '');
    //   	$blockSize = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, 'cbc');
    // 	$plainPad = PaymentController::pkcs5_pad($plainText, $blockSize);
    //   	if (mcrypt_generic_init($openMode, $secretKey, $initVector) != -1) 
    // 	{
    // 	      $encryptedText = mcrypt_generic($openMode, $plainPad);
    //       	      mcrypt_generic_deinit($openMode);

    // 	} 
    // 	return bin2hex($encryptedText);
    // }

    // public function decrypt($encryptedText,$key)
    // {
    // 	$secretKey = PaymentController::hextobin(md5($key));
    // 	$initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
    // 	$encryptedText=PaymentController::hextobin($encryptedText);
    //   	$openMode = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '','cbc', '');
    // 	mcrypt_generic_init($openMode, $secretKey, $initVector);
    // 	$decryptedText = mdecrypt_generic($openMode, $encryptedText);
    // 	$decryptedText = rtrim($decryptedText, "\0");
    //  	mcrypt_generic_deinit($openMode);
    // 	return $decryptedText;

    // }
    // //*********** Padding Function *********************

    // public static function pkcs5_pad ($plainText, $blockSize)
    // {
    //     $pad = $blockSize - (strlen($plainText) % $blockSize);
    //     return $plainText . str_repeat(chr($pad), $pad);
    // }

    // //********** Hexadecimal to Binary function for php 4.0 version ********

    // public static function hextobin($hexString) 
    // { 
    //         $length = strlen($hexString); 
    //     	$binString="";   
    //     	$count=0; 
    //     	while($count<$length) 
    //     	{       
    //     	    $subString =substr($hexString,$count,2);           
    //             $packedString = pack("H*",$subString); 
    //     	    if ($count==0)
    //             {
    //                 $binString.=$packedString;
    //             } 
    //             else 
    //             {
    //                 $binString.=$packedString;
    //             } 
    //             $count+=2; 

    //     	} 
    //         return $binString; 
    // } 
    /**
     * function to calculate total price
     * Author:Karthika
     * Date:05/09/2020
     */
    public function calculateTotalPrice(Request $request)
    {
        $service = ServiceType::find($request['serviceId']);
        if (!$service) {
            return response()->json(
                [
                    'status' => 'Failed',
                    'messages' => 'Service type not found.',
                ]
            );
        }
        $no_hrs = $request['numberHours'];
        $no_maids = $request['numberMaids'];
        $cleaningStatus = $request['cleaningStatus'];
        // $extraServices = $request['extraServices'];
        $coupon = ucfirst($request['coupon']);
        $customerId = $request['customerId'];
        $bookDate = $request['bookedDate'];
        $monthDuration = $request['monthDuration'];
        $visitType = $request['visitType'];
        $interior = $request['interior'];
        $ironing = $request['ironing'];
        $fridge = $request['fridge'];
        $oven = $request['oven'];
        if ($bookDate != '') {

            $weekday = date('w', strtotime($bookDate));
            $startDate = new DateTime($bookDate);
            if ($visitType != 'OD') {
                $dateIntervel = new DateTime($bookDate);
                if ($monthDuration == '') {
                    $monthDuration = 1;
                }
                if ($monthDuration == 'Continue') {
                    $actual_ending_date = $startDate;
                    $ending_date = $startDate;
                } else {
                    $dateIntervel->add(new DateInterval('P' . $monthDuration . 'M'));
                    $actual_ending_date = $dateIntervel->format('Y-m-d');
                    $ending_date = $dateIntervel->format('Y-m-d');  // 2016-01-02
                    $endDate = new DateTime($ending_date);
                    $resultDays = array(
                        '0' => 0,
                        '1' => 0,
                        '2' => 0,
                        '3' => 0,
                        '4' => 0,
                        '5' => 0,
                        '6' => 0
                    );
                    // iterate over start to end date 
                    while ($startDate <= $endDate) {
                        // find the timestamp value of start date 
                        $timestamp = strtotime($startDate->format('d-m-Y'));
                        // find out the day for timestamp and increase particular day 
                        $weekDay = date('w', $timestamp);
                        $resultDays[$weekDay] = $resultDays[$weekDay] + 1;
                        // increase startDate by 1 
                        $startDate->modify('+1 day');
                    }
                    $number_of_weeks = $resultDays[$weekday];
                }
            } else {
                $actual_ending_date = $startDate;
                $endDate = $startDate;
            }


            $no_visits = 1;
            // if ($visitType == 'OD') {
            //     $no_visits = 1;
            // } elseif ($visitType == 'WE') {
            //     $no_visits = $number_of_weeks;
            // } elseif ($visitType == 'BW') {
            //     $no_visits = round($number_of_weeks/2);
            // }
        }
        $vat_percentage = Config::get('values.vat_amount');

        if ($customerId != "") {
            $cust_id = $customerId;
        } else {
            $cust_id = "";
        }
        // // $coupon = ucfirst($this->input->post('coupon_code'));
        // $booktype = $this->input->post('hiddenbooktype');
        // // $no_hrs = $this->input->post('select_hours');
        // $no_maids = $this->input->post('select_maids');
        // $no_visits = $this->input->post('no_of_visits');
        // $service_types = $this->input->post('service_types');


        //$interior_cost = $this->input->post('interiorcost');
        //$fridge_cost = $this->input->post('fridgecost');
        //$iron_cost = $this->input->post('ironingcost');
        //$oven_cost = $this->input->post('ovencost');
        // $clean_val = $this->input->post('hiddencleaningval');
        // $get_fee_details = $this->default_model->get_servicefee_details($service_types);
        //        $get_fee_details = $this->default_model->get_fee_details();
        $bookedDate = date("Y-m-d", strtotime($bookDate));
        if ($cleaningStatus == "N") {
            //$per_hour_rate = $get_fee_details[0]->price_n;//new
            $per_hour_rate = $service->service_rate;
            $cleaning_material_rate = 0;
            $price_to_show = (int)$service->service_rate;
        } else {
            //$per_hour_rate = $get_fee_details[0]->price_c;
            //$per_hour_rate = $get_fee_details[0]->price_n;//new
            $per_hour_rate = $service->service_rate;
            if ($service->material_incl == "Y") {
                $cleaning_material_rate = 0;
            } else {
                $cleaning_material_rate = Config::get('values.cleaning_amount');
            }
            $price_to_show = (int)$service->service_rate;
        }
        if ($no_visits != 0) {
            $a_service_rate = ((($no_hrs * $per_hour_rate) * $no_maids) * $no_visits);
            $cleaning_rates = ((($no_hrs * $cleaning_material_rate) * $no_maids) * $no_visits);
        } else {
            $a_service_rate = (($no_hrs * $per_hour_rate) * $no_maids);
            $cleaning_rates = (($no_hrs * $cleaning_material_rate) * $no_maids);
        }
        $service_rate = ($a_service_rate + $cleaning_rates);
        $vat_charge = ($service_rate * ($vat_percentage / 100));
        $gross_amount = ($service_rate + $vat_charge);
        $total_service_rate = 0;
        $discount = 0;



        if ($coupon == "") {
            $data = array('vat_charge' => $vat_charge, 'service_rate' => $service_rate, 'gross_amount' => $gross_amount, 'hour_rate' => $per_hour_rate, 'net_amount' => $total_service_rate, 'discount' => $discount, 'price_to_show' => $price_to_show, 'cleaningrate' => $cleaning_rates, 'serv_rate' => $a_service_rate);
            return response()->json([
                'status' => "success",
                'data' => $data,
                'message' => 'Amount details fetched!'
            ], 200);
        } else {
            $today = date('Y-m-d');


            $get_coupon_id = $this->default_model->get_coupon_id($coupon);
            $couponCode = CouponCode::where('coupon_name', $coupon)
                ->where('type', 'C')
                ->where('status', 1)
                ->where('expiry_date >=', $today)->first();
            if ($service_types != 1) {
                return response()->json([
                    'status' => "success",
                    'data' => array('vat_charge' => $vat_charge, 'service_rate' => $service_rate, 'gross_amount' => $gross_amount, 'hour_rate' => $per_hour_rate, 'net_amount' => $total_service_rate, 'discount' => $discount, 'price_to_show' => $price_to_show, 'cleaningrate' => $cleaning_rates, 'serv_rate' => $a_service_rate),
                    'message' => 'Coupon not valid for the selected service!'
                ], 200);
            }
            if ($no_hrs < $couponCode->min_hrs) {
                return response()->json([
                    'status' => "success",
                    'data' => array('vat_charge' => $vat_charge, 'service_rate' => $service_rate, 'gross_amount' => $gross_amount, 'hour_rate' => $per_hour_rate, 'net_amount' => $total_service_rate, 'discount' => $discount, 'price_to_show' => $price_to_show, 'cleaningrate' => $cleaning_rates, 'serv_rate' => $a_service_rate),
                    'message' => 'Coupon only valid for minimum ' . $get_coupon_id->min_hrs . 'hours booking!'
                ], 200);
            }
            $coupon_type = $couponCode->coupon_type;
            $expiry_date = $couponCode->expiry_date;
            $offer_type = $couponCode->offer_type;

            if ($expiry_date >= $bookedDate) {
                if ($offer_type == "P") {
                    if ($coupon_type == "FT") {
                        if ($cust_id != "") {
                            $checkbooking = Bookings::where('customer_id', $cust_id)->get();
                            if (count($checkbooking) != 0) {
                                $msg = 'Coupon valid for first booking only.';
                                return response()->json([
                                    'status' => "success",
                                    'data' => array('vat_charge' => $vat_charge, 'service_rate' => $service_rate, 'gross_amount' => $gross_amount, 'hour_rate' => $per_hour_rate, 'net_amount' => $total_service_rate, 'discount' => $discount, 'price_to_show' => $price_to_show, 'cleaningrate' => $cleaning_rates, 'serv_rate' => $a_service_rate),
                                    'message' => $msg
                                ], 200);
                            }
                        }
                    }

                    $v_week_day = $couponCode->valid_week_day;
                    $weekArray = explode(',', $v_week_day);

                    if (in_array($weekday, $weekArray)) {
                        if ($cleaningStatus == "N") {
                            $price_to_show = (int)$per_hour_rate;
                            $per_hour_rate = $per_hour_rate;
                            $cleaning_material_rate = 0;
                        } else {
                            $price_to_show = (int)$per_hour_rate;
                            $per_hour_rate = $per_hour_rate;
                            if ($service->material_incl == "Y") {
                                $cleaning_material_rate = 0;
                            } else {
                                $cleaning_material_rate = Config::get('values.cleaning_amount');
                            }
                        }

                        if ($no_visits != 0) {
                            $a_service_rate = ((($no_hrs * $per_hour_rate) * $no_maids) * $no_visits);
                            $cleaning_rates = ((($no_hrs * $cleaning_material_rate) * $no_maids) * $no_visits);
                        } else {
                            $a_service_rate = (($no_hrs * $per_hour_rate) * $no_maids);
                            $cleaning_rates = (($no_hrs * $cleaning_material_rate) * $no_maids);
                        }
                        $service_rate_new = ($a_service_rate_new + $cleaning_rates_new);

                        $perhrrate = $couponCode->percentage;
                        $aservicerate_new = ((($no_hrs * $perhrrate) * $no_maids) * $no_visits);
                        $serviceratenew = ($aservicerate_new + $cleaning_rates_new);

                        $discount = ($service_rate - $serviceratenew);
                        $getfee = ($service_rate - $discount);
                        $total_service_rate = $getfee;
                        $vat_charge = ($getfee * ($vat_percentage / 100));
                        $gross_amount = ($getfee + $vat_charge);
                        $msg = 'Coupon applied successfully.';
                        return response()->json([
                            'status' => "success",
                            'data' => array('vat_charge' => $vat_charge, 'service_rate' => $service_rate, 'gross_amount' => $gross_amount, 'hour_rate' => $per_hour_rate, 'net_amount' => $total_service_rate, 'discount' => $discount, 'price_to_show' => $price_to_show, 'cleaningrate' => $cleaning_rates_new, 'serv_rate' => $a_service_rate_new, 'coupon_id' => $couponCode->coupon_id),
                            'message' => $msg
                        ], 200);
                    } else {
                        $msg = 'Coupon not valid for selected day.';
                        return response()->json([
                            'status' => "success",
                            'data' => array('vat_charge' => $vat_charge, 'service_rate' => $service_rate, 'gross_amount' => $gross_amount, 'hour_rate' => $per_hour_rate, 'net_amount' => $total_service_rate, 'discount' => $discount, 'price_to_show' => $price_to_show, 'cleaningrate' => $cleaning_rates, 'serv_rate' => $a_service_rate),
                            'message' => $msg
                        ], 200);
                    }
                } else {
                    if ($coupon_type == "FT") {
                        if ($cust_id != "") {
                            $checkbooking = Bookings::where('customer_id', $cust_id)->get();
                            if (count($checkbooking) != 0) {
                                $msg = 'Coupon valid for first booking only.';
                                return response()->json([
                                    'status' => "success",
                                    'data' => array('vat_charge' => $vat_charge, 'service_rate' => $service_rate, 'gross_amount' => $gross_amount, 'hour_rate' => $per_hour_rate, 'net_amount' => $total_service_rate, 'discount' => $discount, 'price_to_show' => $price_to_show, 'cleaningrate' => $cleaning_rates, 'serv_rate' => $a_service_rate),
                                    'message' => $msg
                                ], 200);
                            }
                        }
                    }

                    $v_week_day = $couponCode->valid_week_day;
                    $weekArray = explode(',', $v_week_day);

                    if (in_array($weekday, $weekArray)) {
                        if ($cleaningStatus == "N") {
                            $per_hour_rate = $service->service_rate;
                            $cleaning_material_rate = 0;
                        } else {
                            $per_hour_rate = $service->service_rate;
                            if ($service->material_incl == "Y") {
                                $cleaning_material_rate = 0;
                            } else {
                                $cleaning_material_rate = 10;
                            }
                        }
                        if ($no_visits != 0) {
                            $a_service_rate = ((($no_hrs * $per_hour_rate) * $no_maids) * $no_visits);
                            $cleaning_rates = ((($no_hrs * $cleaning_material_rate) * $no_maids) * $no_visits);
                        } else {
                            $a_service_rate = (($no_hrs * $per_hour_rate) * $no_maids);
                            $cleaning_rates = (($no_hrs * $cleaning_material_rate) * $no_maids);
                        }


                        $service_rate_new = ($a_service_rate_new + $cleaning_rates_new);

                        $discount = $couponCode->percentage;

                        $getfee = ($service_rate - $discount);
                        $total_service_rate = $getfee;
                        $vat_charge = ($getfee * ($vat_percentage / 100));
                        $gross_amount = ($getfee + $vat_charge);
                        $msg = 'Coupon applied successfully.';
                        return response()->json([
                            'status' => "success",
                            'data' => array('vat_charge' => $vat_charge, 'service_rate' => $service_rate, 'gross_amount' => $gross_amount, 'hour_rate' => $per_hour_rate, 'net_amount' => $total_service_rate, 'discount' => $discount, 'price_to_show' => $per_hour_rate, 'cleaningrate' => $cleaning_rates_new, 'serv_rate' => $a_service_rate_new, 'coupon_id' => $couponCode->coupon_id),
                            'message' => $msg
                        ], 200);
                    } else {
                        $msg = 'Coupon not valid for selected day.';
                        return response()->json([
                            'status' => "success",
                            'data' => array('vat_charge' => $vat_charge, 'service_rate' => $service_rate, 'gross_amount' => $gross_amount, 'hour_rate' => $per_hour_rate, 'net_amount' => $total_service_rate, 'discount' => $discount, 'price_to_show' => $price_to_show, 'cleaningrate' => $cleaning_rates, 'serv_rate' => $a_service_rate),
                            'message' => $msg
                        ], 200);
                    }
                }
            } else {
                $msg = 'Coupon expired.';
                return response()->json([
                    'status' => "success",
                    'data' => array('vat_charge' => $vat_charge, 'service_rate' => $service_rate, 'gross_amount' => $gross_amount, 'hour_rate' => $per_hour_rate, 'net_amount' => $total_service_rate, 'discount' => $discount, 'price_to_show' => $price_to_show, 'cleaningrate' => $cleaning_rates, 'serv_rate' => $a_service_rate),
                    'message' => $msg
                ], 200);
            }
        }
    }
    public function calcAvailability($date, $s_hour, $week_day)
    {
        // dump($s_hour);
        $html = "";
        $times = array();
        $current_hour_index = 0;
        $time = '07:00 AM';
        $time_stamp = strtotime($time);
        $err = 0;
        $available_times = [];
        $oneDimensionalArray = [];
        // $available_times = BookingSlots::where('date', $date)->where('status', 1)->pluck('from_time')->toArray();
        $available_timess = BookingSlots::where('date',$date)->where('status',1)->select('from_time','to_time')->get(); 
        // dump($available_timess);die;
        foreach($available_timess as $at){
            $diff=ceil((strtotime($at->to_time) - strtotime($at->from_time))/(60*60));
            $f_time=$at->from_time;
            $t_time=$at->to_time;
            for($i=0;$i<$diff;$i++){
                if($f_time!=$t_time){
                    $available_times[]=$f_time;
                    $f_time=date('H:i:s', strtotime('+60mins', strtotime($f_time)));
                }
            }
        }
        // dump($available_times);die;
        // $html .= '<p>Available time <strong class="selecteddate">'.$request->input('bookedDate').'</strong><span class="hiddenfromtime_error val_errr" style="display:none;"></span></p>';
        for ($i = 0; $i < 11; $i++) {
            $oneDimensionalArray = $available_times;

            $time_stamp = strtotime('+60mins', strtotime($time));
            $timess = date('H:i:s', $time_stamp);
            $time = date('g:i A', $time_stamp);
            // dump($oneDimensionalArray);die;
            if (in_array($timess, $oneDimensionalArray)) {
            } else {
                if ($date == date('Y-m-d')) {
                    $t_shrt = date('H:i:s', strtotime($time));
                    $cur_shrt = date('H:i:s');
                    $hours = ((strtotime($t_shrt) - strtotime($cur_shrt)) / 3600);
                    // dump($hours);
                    if ($hours >= 2) {
                        // dump($hours);
                        $err = 0;
                        $to_time = date('H', strtotime($timess . '+' . $s_hour . ' hour'));
                        // if (((int) $to_time) <= 19 && ((int) $to_time) > 9) {
                        //     $html.= '<li>';
                        //     $html.= '<div class="tick-mark">&nbsp;</div>';
                        //     $html .= '<div class="tick-text" id="'.$timess.'">'.$time.'</div>';
                        //     $html .= '<div class="clear"></div></li>';
                        //     //$html .= '<li data-id="'.$timess.'">'.$time.'</li>';
                        // }
                        // dump($to_time);
                        if ($s_hour == 1) {
                            if (((int) $to_time) <= 19 && ((int) $to_time) > 7) {
                                $html .= '<li>';
                                $html .= '<div class="tick-mark"></div>';
                                $html .= '<div class="tick-text" id="' . $timess . '">' . $time . '</div>';
                                $html .= '<div class="clear"></div></li>';
                            }
                        } else {
                            if (((int) $to_time) <= 20 && ((int) $to_time) > 8) {
                                $html .= '<li>';
                                $html .= '<div class="tick-mark"></div>';
                                $html .= '<div class="tick-text" id="' . $timess . '">' . $time . '</div>';
                                $html .= '<div class="clear"></div></li>';
                            }
                        }
                    } else {
                        $err = 1;
                    }
                } else {
                    $to_time = date('H', strtotime($timess . '+' . $s_hour . ' hour'));
                    // dump($to_time);
                    // if (((int) $to_time) <= 19 && ((int) $to_time) > 9) {
                    //     $html.= '<li>';
                    //     $html.= '<div class="tick-mark"></div>';
                    //     $html.= '<div class="tick-text" id="'.$timess.'">'.$time.'</div>';
                    //     $html.= '<div class="clear"></div></li>';
                    //     $err = 0;
                    // } 
                    if ($s_hour == 1) {
                        if (((int) $to_time) <= 19 && ((int) $to_time) > 7) {
                            $html .= '<li>';
                            $html .= '<div class="tick-mark"></div>';
                            $html .= '<div class="tick-text" id="' . $timess . '">' . $time . '</div>';
                            $html .= '<div class="clear"></div></li>';
                            $err = 0;
                        }
                    } else {
                        if (((int) $to_time) <= 20 && ((int) $to_time) > 8) {
                            $html .= '<li>';
                            $html .= '<div class="tick-mark"></div>';
                            $html .= '<div class="tick-text" id="' . $timess . '">' . $time . '</div>';
                            $html .= '<div class="clear"></div></li>';
                            $err = 0;
                        }
                    }
                    // dump($err);

                }
            }
        }
        // dump($html);

        // if($err == 1)
        // {
        //     $html.='<div class="col-md-12 col-sm-12 comment-box no-left-right-padding">
        //                                     <p id="no-shift-message" class="black">Currently No Shift Available...</p>
        //                                     </div>';
        // }
        // dump($err);
        $html .= '<div class="clear"></div>';
        if ($err == 0) {
            return array('err' => $err, 'html' => $html, 'selectedDate' => $date);
        } else {
            // return array('err'=>$err,'html'=>$html,'selectedDate'=>$date);
            $tomorrow = new DateTime('tomorrow');
            $date = $tomorrow->format('Y-m-d');
            $week_day = date('w', strtotime($date));
            return PaymentController::calcAvailability($date, $s_hour, $week_day);
        }
    }
    public function timeAvailability(Request $request)
    {
        $bookDate = $request['bookedDate'];
        $date = date("Y-m-d", strtotime($bookDate));
        $week_day = date('w', strtotime($date));
        $s_hour = $request['numberHours'];
        $timeslots = PaymentController::calcAvailability($date, $s_hour, $week_day);
        return response()->json([
            'status' => "success",
            'data' => ['html' => $timeslots['html'], 'date' => $timeslots['selectedDate']],
            'message' => 'Time details fetched successfully'
        ], 200);
    }
    public function cashPaymentSuccess(Request $request)
    {
        $refId = $request['ref_no'];
        $bookings = Bookings::where('reference_id', $refId)->first();
        if ($bookings->pay_by == 'Card') {
            $bookings->total_amount = $bookings->total_amount + 5;
        }
        $bookings->pay_by = 'Cash';
        $bookings->booking_status = 0;
        $bookings->save();
        $customer = Customers::where('customer_id', $bookings->customer_id)->first();
        $customerAddress = CustomerAddress::where('customer_id', $bookings->customer_id)->where('default_address',1)->where('address_status',0)->first();
        $services = ServiceType::where('service_type_id', $bookings->service_type_id)->first();
        if ($customerAddress) {
            $area = Area::find($customerAddress->area_id);
            $areaName = $area->area_name;
        } else {
            $areaName = 'NA';
            $customerAddress = '';
        }
        $date = $bookings->service_start_date;
        $old_date_timestamp = strtotime($date);
        $dateFormat = date('l jS, F', $old_date_timestamp);
        $start = date("g:i a", strtotime($bookings->time_from));
        $end = date("g:i a", strtotime($bookings->time_to));
        // $msg = 'Hi admin, you have a new booking (Ref - ' . $refId . ') on ' . $dateFormat . ' with ' . $customer->customer_name . ' at ' . $start . ' - ' . $end . ' for ' . $services->service_type_name . '. Please call ' . $customer->mobile_number_1 . ' for any queries.';
        // LoginController::send_sms('582864783',$msg);

	if($bookings->booking_type == 'OD') {
            $val = 'One Day';
        } else if($bookings->booking_type == 'WE') {
            $val = 'Weekly';
        } else {
            $val = 'Bi-weekly';
        }
        $dt = new DateTime;
        $userActivity = new UserActivity();
        $userActivity->added_user = (int)1;
        $userActivity->booking_type = $val;
        $userActivity->shift = date("H:i", strtotime($bookings->time_from)).'-'.date("H:i", strtotime($bookings->time_to));
        $userActivity->action_type = 'Booking_add';
        $actionContent = 'A new booking for customer: '.$customer->customer_name.' at '.$start.' - '.$end.' is received from website.';
        $userActivity->action_content = $actionContent;
        $userActivity->addeddate = $dt->format('y-m-d H:i:s');
        $userActivity->date_time_added = $dt->format('y-m-d H:i:s');
        $userActivity->save();
        return response()->json(
            [
                'status' => 'success',
                'messages' => 'Booking saved successfully.',
                'data' => ['bookings' => $bookings, 'customer' => $customer, 'customer_address' => $customerAddress, 'services' => $services, 'serviceName' => $services->service_type_name, 'areaName' => $areaName],
            ],
            200
        );
    }
    public function paymentSuccess(Request $request)
    {
        $refId = $request['ref_no'];
        $trackId = $request['trackId'];
        $bookings = Bookings::where('reference_id', $refId)->first();
        $online = OnlinePayment::where('reference_id', $refId)->first();
        $status_msg = "refresh_success";
        if($online && $online->payment_status != "success") {
            $online->transaction_id = $trackId;
            $online->payment_status = "success";
            $online->save();
            $bookings->booking_status = 0;
            $bookings->save();
            $status_msg ="success";
        }
        $customer = Customers::where('customer_id', $bookings->customer_id)->first();
        $customerAddress = CustomerAddress::where('customer_id', $bookings->customer_id)->where('default_address',1)->where('address_status',0)->first();
        if ($customerAddress) {
            $area = Area::find($customerAddress->area_id);
            $areaName = $area->area_name;
        } else {
            $areaName = 'NA';
            $customerAddress = '';
        }
        $services = ServiceType::where('service_type_id', $bookings->service_type_id)->first();
        $date = $bookings->service_start_date;
        $old_date_timestamp = strtotime($date);
        $dateFormat = date('l jS, F', $old_date_timestamp);
        $start = date("g:i a", strtotime($bookings->time_from));
        $end = date("g:i a", strtotime($bookings->time_to));
        // $msg = 'Hi admin, you have a new booking (Ref - ' . $refId . ') on ' . $dateFormat . ' with ' . $customer->customer_name . ' at ' . $start . ' - ' . $end . ' for ' . $services->service_type_name . '. Please call ' . $customer->mobile_number_1 . ' for any queries.';
        // LoginController::send_sms('582864783',$msg);

	   if($bookings->booking_type == 'OD') {
            $val = 'One Day';
        } else if($bookings->booking_type == 'WE') {
            $val = 'Weekly';
        } else {
            $val = 'Bi-weekly';
        }
        $dt = new DateTime;
        $userActivity = new UserActivity();
        $userActivity->added_user = (int)1;
        $userActivity->booking_type = $val;
        $userActivity->shift = date("H:i", strtotime($bookings->time_from)).'-'.date("H:i", strtotime($bookings->time_to));
        $userActivity->action_type = 'Booking_add';
        $actionContent = 'A new booking for customer: '.$customer->customer_name.' at '.$start.' - '.$end.' is received from website.';
        $userActivity->action_content = $actionContent;
        $userActivity->addeddate = $dt->format('y-m-d H:i:s');
        $userActivity->date_time_added = $dt->format('y-m-d H:i:s');
        $userActivity->save();
        return response()->json(
            [
                'status' => $status_msg,
                'messages' => 'Booking saved successfully.',
                'data' => ['bookings' => $bookings, 'customer' => $customer, 'customer_address' => $customerAddress, 'services' => $services, 'serviceName' => $services->service_type_name, 'areaName' => $areaName, 'online' => $online],
            ],
            200
        );
    }
    public function paymentFailed(Request $request)
    {
        $refId = $request['ref_no'];
        $trackId = $request['trackId'];
        $online = OnlinePayment::where('reference_id', $refId)->first();
        $online->transaction_id = $trackId;
        $online->payment_status = 'failed';
        $online->save();
        $bookings = Bookings::where('reference_id', $refId)->first();
        $customer = Customers::where('customer_id', $bookings->customer_id)->first();
        $customerAddress = CustomerAddress::where('customer_id', $bookings->customer_id)->where('default_address',1)->where('address_status',0)->first();
        $services = ServiceType::where('service_type_id', $bookings->service_type_id)->first();
        if ($customerAddress) {
            $area = Area::find($customerAddress->area_id);
            $areaName = $area->area_name;
        } else {
            $areaName = 'NA';
            $customerAddress = '';
        }
        return response()->json(
            [
                'status' => 'success',
                'messages' => 'Booking failed.',
                'data' => ['bookings' => $bookings, 'customer' => $customer, 'customer_address' => $customerAddress, 'services' => $services, 'serviceName' => $services->service_type_name, 'areaName' => $areaName, 'online' => $online],
            ],
            200
        );
    }
    public function saveInvoicePay(Request $request)
    {

        $customer_id = $request['customerId'];
        $description = $request['description'];
        $inv_id = $request['inv_id'];

	$amount = $request['amount'];
        $data = array();
        if ($customer_id != "") {

            $data_post = serialize($_POST);

            // $this->load->library('user_agent');
            // if ($this->agent->is_browser())
            // {
            //     $agent = $this->agent->browser().' '.$this->agent->version();
            // }
            // elseif ($this->agent->is_robot())
            // {
            //     $agent = $this->agent->robot();
            // }
            // elseif ($this->agent->is_mobile())
            // {
            //     $agent = $this->agent->mobile();
            // }
            // else
            // {
            $agent = 'Unidentified User Agent';
            // }

            //$transaction_charg = ((($amount * 0.03) + 1) * 1.05);
            //$transaction_charge = number_format((float)$transaction_charg, 2, '.', '');
			$transaction_charge = 0;



            $onlinePayment = new OnlinePayment();
            $onlinePayment->description = $description;
            $onlinePayment->transaction_charge = $transaction_charge;
            $onlinePayment->amount = $amount;
            $onlinePayment->customer_id = $customer_id;
            $onlinePayment->paid_from = 'O';
            $onlinePayment->payment_datetime = date('Y-m-d H:i:s');
            $onlinePayment->user_agent = $agent;
            $onlinePayment->post_data = $data_post;
            $onlinePayment->ip_address = '';
            $onlinePayment->payment_status = 'initiated';
	        $onlinePayment->inv_id = $inv_id;
            $onlinePayment->save();

            $customer = Customers::find($customer_id);
            $customer_address_id = $customer->customer_address_id;
            $customerAddress = CustomerAddress::where('customer_id', $customer_id)->where('default_address',1)->where('address_status',0)->first();
            if ($customerAddress) {
                $address = $customerAddress->customer_address;
                $area = Area::find($customerAddress->area_id);
                $areaName = $area->area_name;
            } else {
                $address = '';
                $areaName = 'NA';
            }
            $customer_name = $customer->customer_name;
            $customer_mobile = $customer->mobile_number_1;
            $email_id = $customer->email_address;
            $data['customer_name'] = $customer_name;
            $data['customer_email'] = $email_id;
            $data['description'] = $description;
            $data['amount'] = $amount;
            $data['transaction_charge'] = $transaction_charge;
            $data['gross_amount'] = number_format((float)($amount + $transaction_charge), 2, '.', '');
            $data['phone_number'] = $customer_mobile;
            $data['address'] = $address;
            $data['area'] = $areaName;
            $data['pay_details'] = $onlinePayment;
            $data['reference_id'] = '';
            $data['inv_id'] = $inv_id;

            return response()->json(
                [
                    'status' => 'success',
                    'messages' => 'Payment details added.',
                    'data' => $data,
                ],
                200
            );
        } else {
            return response()->json(
                [
                    'status' => 'failed',
                    'messages' => 'Customer not found.',
                    'data' => null,
                ]
            );
        }
    }
    public function onlinePaymentSuccess(Request $request)
    {
        $payId = $request['payId'];
        $trackId = $request['trackId'];
        $paytype = $request['payment_type'];
        $refid = (1000 + $payId);
        $status_msg = "refresh_success";
        $online = OnlinePayment::where('payment_id', $payId)->first();
        if($online && $online->payment_status !='success') {
            if ($online->reference_id == '') {
                $online->reference_id = 'DHK-ON/'.date("Y").'/' . $refid;
            } elseif ($online->reference_id != '') {
                $bookings = Bookings::where('reference_id', $online->reference_id)->first();
                if($bookings) {
                    $bookings->booking_status = 0;
                    $bookings->save();
                }
            }
            $online->payment_status = "success";
            $online->transaction_id = $trackId;
            $online->payment_type = $paytype;
            $online->save();
            $customerPay = new CustomerPayments();
            $customerPay->customer_id = $online->customer_id;
            $customerPay->paid_amount = $online->amount;
            $customerPay->paid_at = 'O';
            if ($online->reference_id == '') {
                $customerPay->ps_no = 'DHK-ON/'.date("Y").'/' . $refid;
                $customerPay->receipt_no = 'DHK-ON/'.date("Y").'/' . $refid;
            } elseif ($online->reference_id != '') {
                $customerPay->ps_no = $online->reference_id;
                $customerPay->receipt_no = $online->reference_id;
            }
            $customerPay->paid_at_id = 1;
            $customerPay->payment_method = 1;
            $customerPay->day_service_id = 0;
            $customerPay->paid_datetime = date('Y-m-d H:i:s');
            $customerPay->save();
            $status_msg = "success";
			
			// try{
				// $post['params']['invoice_id'] = $online->inv_id;
				// $post['params']['amount'] = $online->amount;
				// $post['params']['comment'] = $online->description;
				// $post['params']['payment_date'] = date('Y-m-d', strtotime($online->payment_datetime));
				// $post['params']['transaction_id'] = $trackId;
				
				// $post_values=json_encode($post);
				
				
				// $ServiceURL="http://limshq.fortiddns.com:8088/post_payments";
				// $orderCreateResponse = $this->curlFunOdoo($post_values,$ServiceURL);
				// $responseBody_odoo=json_decode($orderCreateResponse,true);
			// } catch (\Exception $e) {
				
			// }
        }
            
        $customer = Customers::where('customer_id', $online->customer_id)->first();
        $customerAddress = CustomerAddress::where('customer_id', $online->customer_id)->where('default_address',1)->where('address_status',0)->first();
        if ($customerAddress) {
            $area = Area::find($customerAddress->area_id);
            $areaName = $area->area_name;
        } else {
            $areaName = 'NA';
            $customerAddress = '';
        }

        // Mail::send(new SuccessMail($customer->email_address,$customer,$bookings,$customerAddress,$areaName,$services,$online));
        // Mail::send(new SuccessMailAdmin($customer->email_address,$customer,$bookings,$customerAddress,$areaName,$services,$online));
        return response()->json(
            [
                'status' => $status_msg,
                'messages' => 'Booking saved successfully.',
                'data' => ['customer' => $customer, 'customer_address' => $customerAddress, 'serviceName' => 'Other Payments', 'areaName' => $areaName, 'paymentData' => $online],
            ],
            200
        );
    }
    public function onlinePaymentFailed(Request $request)
    {
        $payId = $request['payId'];
        $trackId = $request['trackId'];
        $payment_type = $request['payment_type'];
        $online = OnlinePayment::where('payment_id', $payId)->first();
        if ($online->reference_id == '') {
	    $reference_id =(1000 + $payId);
            $online->reference_id = 'DHK-ON/'.date("Y").'/'.$reference_id;
        }
        $online->transaction_id = $trackId;
        $online->payment_status = 'failed';
        $online->payment_type = $payment_type;
        $online->save();
        $customer = Customers::where('customer_id', $online->customer_id)->first();
        $customerAddress = CustomerAddress::where('customer_id', $online->customer_id)->where('default_address',1)->where('address_status',0)->first();
        if ($customerAddress) {
            $area = Area::find($customerAddress->area_id);
            $areaName = $area->area_name;
        } else {
            $areaName = 'NA';
            $customerAddress = '';
        }

        return response()->json(
            [
                'status' => 'success',
                'messages' => 'Booking failed.',
                'data' => ['customer' => $customer, 'customer_address' => $customerAddress, 'serviceName' => 'Other Payments', 'areaName' => $areaName, 'paymentData' => $online],
            ],
            200
        );
    }
    public function saveBookinglistPay(Request $request)
    {
        $data = array();
        $bookings = Bookings::where('booking_id', $request['bookingId'])->first();
        $amount = $bookings->total_amount;
         if($bookings->booked_from=='W' || $bookings->booked_from=='M') {
            $amount = $amount - 5;
        }
        // $transaction_charg = ((($amount * 0.03) + 1) * 1.05);
        // $transaction_charge = number_format((float)$transaction_charg, 2, '.', '');
        if ($bookings->pay_by == 'Cash') {
            $bookings->pay_by = 'Card';
            $bookings->total_amount = $amount; // status 0 when payment done
            $bookings->save();
            $agent = 'Unidentified User Agent';
            $onlinePayment = new OnlinePayment();
            $onlinePayment->description = '';
            $onlinePayment->booking_id = $bookings->booking_id;
            $onlinePayment->reference_id = $bookings->reference_id;
            // $onlinePayment->transaction_charge = $transaction_charge;
            $onlinePayment->amount = $amount;
            $onlinePayment->customer_id = $bookings->customer_id;
           if($request['is_device']=='mobile') {
		        $onlinePayment->paid_from = 'M';
            } else if($request['is_device']=='web') {
                $onlinePayment->paid_from = 'W';
            }
            $onlinePayment->payment_datetime = date('Y-m-d H:i:s');
            $onlinePayment->user_agent = $agent;
            $onlinePayment->post_data = '';
            $onlinePayment->ip_address = '';
            $onlinePayment->payment_status = 'initiated';
            $onlinePayment->save();
        }
        $onlinePayment = OnlinePayment::where('reference_id', $bookings->reference_id)->first();
        $customer = Customers::find($bookings->customer_id);
        $customer_address_id = $customer->customer_address_id;
        $customerAddress = CustomerAddress::where('customer_id', $customer->customer_id)->where('default_address',1)->where('address_status',0)->first();
        $customer_name = $customer->customer_name;
        $customer_mobile = $customer->mobile_number_1;
        $email_id = $customer->email_address;
        if ($customerAddress) {
            $address = $customerAddress->customer_address;
            $area = Area::find($customerAddress->area_id);
            $areaName = $area->area_name;
        } else {
            $address = 'NA';
            $areaName = 'NA';
        }


        $data['customer_name'] = $customer_name;
        $data['customer_email'] = $email_id;
        $data['amount'] = $amount;
        // $data['transaction_charge']=$transaction_charge;
        $data['gross_amount'] = number_format((float)$amount, 2, '.', '');
        $data['phone_number'] = $customer_mobile;
        $data['address'] = $address;
        $data['area'] = $areaName;
        $data['reference_id'] = $bookings->reference_id;
        $data['payment_id'] = $onlinePayment->payment_id;
        return response()->json(
            [
                'status' => 'success',
                'messages' => 'Payment details added.',
                'data' => $data,
            ],
            200
        );
    }
    public function cancelBooking(Request $request)
    {
        $cancelBooking = Bookings::find($request['bookingId']);
        $cancelBooking->is_cancelled = 'yes';
        $cancelBooking->booking_status = 2;
        $cancelBooking->cancel_date = date('Y-m-d H:i:s');
        $cancelBooking->save();
        // $BookingCancel = new BookingCancel();
        // $BookingCancel->booking_id = $request['bookingId'];
        // $BookingCancel->service_date = $cancelBooking->service_start_date;
        // $BookingCancel->deleted_by = $request['customerId'];
        // $BookingCancel->added_datetime =  date('Y-m-d H:i:s');
        // $BookingCancel->save();
        $customer = Customers::find($cancelBooking->customer_id);
        $customer_address_id = $customer->customer_address_id;
        $customerAddress = CustomerAddress::where('customer_id', $customer->customer_id)->where('default_address',1)->where('address_status',0)->first();
        if ($customerAddress) {
            $address = $customerAddress->customer_address;
            $area = Area::find($customerAddress->area_id);
            $areaName = $area->area_name;
        } else {
            $address = 'NA';
            $areaName = 'NA';
        }
        $customer_name = $customer->customer_name;
        $customer_mobile = $customer->mobile_number_1;
        $email_id = $customer->email_address;

        $data = array();
        $data['customer_name'] = $customer_name;
        $data['customer_email'] = $email_id;
        $data['phone_number'] = $customer_mobile;
        $data['address'] = $address;
        $data['area'] = $areaName;
        $data['reference_id'] = $cancelBooking->reference_id;
        $data['time_from'] = $cancelBooking->time_from;
        $data['service_start_date'] = $cancelBooking->service_start_date;
        $data['time_to'] = $cancelBooking->time_to;
        $data['pay_by'] = $cancelBooking->pay_by;
        return response()->json(
            [
                'status' => 'success',
                'messages' => 'Booking cancelled successfully.',
                'data' => $data,
            ],
            200
        );
    }
    public function cancelBookingNew(Request $request)
    {
        $cancelBooking = Bookings::find($request['bookingId']);
        $cancelBooking->is_cancelled = 'yes';  
        $cancelBooking->cancel_reason = $request['cancelReason'];  
        $cancelBooking->cancel_comments = $request['cancelcomments'];  
        $cancelBooking->booking_status = 2;
        $cancelBooking->cancel_date = date('Y-m-d H:i:s');
        $cancelBooking->save();
        // $BookingCancel = new BookingCancel();
        // $BookingCancel->booking_id = $request['bookingId'];
        // $BookingCancel->service_date = $cancelBooking->service_start_date;
        // $BookingCancel->deleted_by = $request['customerId'];
        // $BookingCancel->added_datetime =  date('Y-m-d H:i:s');
        // $BookingCancel->save();
        $customer = Customers::find($cancelBooking->customer_id);
        $customer_address_id = $customer->customer_address_id;
        $customerAddress = CustomerAddress::where('customer_id',$customer->customer_id)->where('default_address',1)->where('address_status',0)->first();
        if($customerAddress) {
            $address = $customerAddress->customer_address;
            $area = Area::find($customerAddress->area_id);
            $areaName= $area->area_name; 
        } else {
            $address ='NA';
            $areaName = 'NA';
        }
        $customer_name = $customer->customer_name;
        $customer_mobile = $customer->mobile_number_1;
        $email_id = $customer->email_address; 
        $data=array();   
        $data['customer_name']=$customer_name;
        $data['customer_email']=$email_id;
        $data['phone_number']=$customer_mobile;
        $data['address']=$address;
        $data['area']=$areaName;
        $data['reference_id'] = $cancelBooking->reference_id;
        $data['time_from']=$cancelBooking->time_from;
        $data['service_start_date']=$cancelBooking->service_start_date;
        $data['time_to'] = $cancelBooking->time_to;
        $data['pay_by']=$cancelBooking->pay_by;
        return response()->json(
            [
                'status' => 'success',
                'messages' => 'Booking cancelled successfully.',
                'data' =>$data,
            ],
            200
        ); 
    }
	
	public static function curlFunOdoo($curl_post,$url)
    {
        $curl_header = array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($curl_post)
		);
		$ch2 = curl_init();
		curl_setopt($ch2, CURLOPT_URL, $url);
		curl_setopt($ch2, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($ch2, CURLOPT_POST, 1);
		curl_setopt($ch2, CURLOPT_POSTFIELDS, $curl_post);
		curl_setopt($ch2, CURLOPT_HTTPHEADER, $curl_header);
		curl_setopt($ch2, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch2, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, 0);
		$user_datas = curl_exec($ch2);
		
		if (curl_errno($ch2)) {
			echo $error_msg = curl_error($ch2);
			exit();
		}
		
		return $user_datas;
    }
}
