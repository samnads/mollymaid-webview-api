<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ServiceType;
use App\ExtraServices;
use App\Area;
use App\Bookings;
use App\ServiceCategory;
use App\CustomerAddress;
use App\BookingServices;
use App\ServiceCategoryCost;
use App\DayServices;
use App\Customers;
use App\OnlinePayment;
use CouponCode;
use Exception;
use App\ConfigCleaningSupplies;
use App\ServiceCleaningSupplies;
use DateTime;
use DateInterval;
use Illuminate\Support\Facades\Config;
use DB;
use Response;


use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;


class ServiceController extends Controller
{
    /**
     * Author:Karthika
     * Date:19/08/2020
     * Function to get all services
     */
    public function getServices(Request $request)
    {
        $customerId = $request['customerId'];
        $service = 'House Cleaning';
        $serviceId = '1';
        $defaultService = ServiceType::where('service_type_id', $serviceId)->first();

        $extraServices = ExtraServices::where('status', '0')->get();
        // $area = Area::select('area_id', 'area_name')->where('area_name', '!=', '')->get();
        $area = Area::select('area_id', 'area_name')->where('area_status', '1')->where('web_status',1)->get();
        if ($customerId != '') {
            $customer = Customers::where('customer_id', $customerId)->first();
            $addressArray = [];
            $customerAddress = CustomerAddress::where('customer_id', $customer->customer_id)->where('address_status',0)->where('default_address',1)->first();
            if ($customerAddress != '') {
                $areaCustomer = Area::find($customerAddress->area_id);
                $areaName = $areaCustomer->area_name;
                $addressArray = ['addressName' => $customerAddress['customer_address'], 'AreaName' => $areaName];
            }
			$cust_mob = $customer->mobile_number_1;
        } else {
            $addressArray = [];
			$cust_mob = "";
        }
        $data = [
            'defaultService' => $defaultService, 'cust_phone' => $cust_mob, 'extraservices' => $extraServices, 'current_service' => $service, 'area' => $area,
            'address' => $addressArray,
        ];
        //$data['custom_cleaning_supplies'] = ConfigCleaningSupplies::where('type', "Custom")->get();
        //$data['plan_based_cleaning_supplies'] = ConfigCleaningSupplies::where('type', "PlanBased")->get();
        return response()->json([
            'status' => "success",
            'data' => $data,
            'message' => 'All services listed successfully!'
        ], 200, array(), JSON_PRETTY_PRINT);
    }
    public function loadServiceById(Request $request)
    {
        $service_type_id = $request->service_type_id;
        $customerId = $request['customerId'];
        $service = 'House Cleaning';
        $defaultService = ServiceType::where('service_type_id', $request->service_type_id)->first();
        //$extraServices = ExtraServices::where('status', '0')->get();
        // $area = Area::select('area_id', 'area_name')->where('area_name', '!=', '')->get();
        $area = Area::select('area_id', 'area_name')->where('area_status', '1')->where('web_status',1)->orderBy('area_name', 'ASC')->get();
        if ($customerId != '') {
            if ($customer = Customers::where('customer_id', $customerId)->first()) {
                $addressArray = [];
                $customerAddress = CustomerAddress::where('customer_id', $customer->customer_id)->where('address_status', 0)->where('default_address', 1)->first();
                if ($customerAddress != '') {
                    $areaCustomer = Area::find($customerAddress->area_id);
                    $areaName = $areaCustomer->area_name;
                    $addressArray = ['addressName' => $customerAddress['customer_address'], 'AreaName' => $areaName];
                }
                $cust_mob = $customer->mobile_number_1;} else {
                $addressArray = [];
                $cust_mob = "";
            }
        } else {
            $addressArray = [];
            $cust_mob = "";
        }
        /*********************************************************** */
        $data = [
             'cust_phone' => $cust_mob, 'extraservices' => @$extraServices, 'current_service' => $service, 'area' => $area,
            'address' => $addressArray,
        ];
        /*********************************************************** */
        if($request['customerId'] && $request['token']){
            if(!$data['customer'] = Customers::where('customer_id', $request['customerId'])->where('oauth_token', $request['token'])->first())
            {
                $addressArray = [];
                $cust_mob = "";
            }
        }
        else if($request['customerId']){
            if(!$data['customer'] = Customers::where('customer_id', $request['customerId'])->first())
            {
                $addressArray = [];
                $cust_mob = "";
            }
        }
        else{
            $addressArray = [];
			$cust_mob = "";
        }
        /*********************************************************** */
        $data['address'] = $addressArray;
        $data['cust_phone'] = $cust_mob;
        /*********************************************************** */
        $data['services'] = ServiceType::where('web_status',1)
        //->orderBy('web_order_id', 'ASC')
        ->get();
        $data['service'] = ServiceType::where('service_type_id', $request->service_type_id)->first();
        $data['service_expect_points'] = DB::table('service_what_to_expect_points')->where('service_type_id', $request->service_type_id)->orderBy('order_id', 'ASC')->get();
        $data['service_categories'] = DB::table('servicecategory')->where('service_type_id', $request->service_type_id)->get();
        $data['service_sub_categories'] = DB::table('servicesubcategory as ssc')->select('ssc.*')->leftjoin('servicecategory as sc', 'ssc.category_id', '=', 'sc.id')->where('sc.service_type_id', $request->service_type_id)->get();
        $data['service_category_costs'] = DB::table('servicecategorycosts as scc')
                                            ->select('scc.*','sc.service_category_name','ssc.sub_category_name','service_furnish_name')
                                            ->leftjoin('servicesubfurnishcategory as ssfc', 'scc.service_sub_funish_id', '=', 'ssfc.id')
                                            ->leftjoin('servicesubcategory as ssc', 'scc.sub_category_id', '=', 'ssc.id')
                                            ->leftjoin('servicecategory as sc', 'scc.category_id', '=', 'sc.id')
                                            ->where('scc.service_id', $request->service_type_id)
                                            ->where('scc.status', 1)
                                            ->orderBy('service_cost', 'ASC')
                                            ->get();
        //$data['service_extra_services'] = DB::table('extra_services as es')->select('es.*')->where('service_type_id', $request->service_type_id)->where('status',0)->get();
        //$data['custom_cleaning_supplies'] = ServiceCleaningSupplies::leftjoin('config_cleaning_supplies as ccs', 'service_cleaning_supplies.cleaning_supply_id', '=', 'ccs.id')->where('service_cleaning_supplies.service_type_id', $request->service_type_id)->where('ccs.type', 'Custom')->get();
        //$data['plan_based_cleaning_supplies'] = ServiceCleaningSupplies::leftjoin('config_cleaning_supplies as ccs', 'service_cleaning_supplies.cleaning_supply_id', '=', 'ccs.id')->where('service_cleaning_supplies.service_type_id', $request->service_type_id)->where('ccs.type','PlanBased')->get();
        /*$data['crew_in_options'] = DB::table('crew_in_options as cio')->select('cio.*')->where(function ($query) use ($service_type_id){
                                    $query->where('cio.service_type_id', '=', $service_type_id)
                                        ->orWhere('cio.service_type_id', '=', null);
                                })
                                ->orderBy('cio.sort_order', 'ASC')
                                ->orderBy('cio.text', 'ASC')
                                ->where('cio.deleted_at', null)->get();*/
        //$data['holiday_dates'] = array_column(DB::table('holidays')->where([['deleted_at', '=', null], ['date', '>=', date('Y-m-d')]])->get()->toArray(),'date');
        $data['weekends'] = array_column(DB::table('week_days')->where([['weekend', '=', 1]])->get()->toArray(),'week_day_id');
        $data['request'] = $request->all();
        return response()->json([
            'status' => "success",
            'data' => $data,
            'message' => 'Services fetched successfully!'
        ], 200,[], JSON_PRETTY_PRINT);
    }
    /**
     * function to get all details of disinfection services
     */
    function getDisinfection(Request $request)
    {
        $customerId = $request['customerId'];
        $service = ServiceType::where('service_type_name', 'Disinfection Only')->first();
        $allServices = ServiceCategoryCost::select('service_id', 'category_id', 'sub_category_id', 'service_sub_funish_id', 'is_scrubbing', 'name', 'service_cost', 'status')
            ->where('service_id', $service->service_type_id)->get()->toArray();
        $area = Area::select('area_id', 'area_name')->where('area_status', 1)->where('web_status',1)->get();
        if ($customerId != '') {
            $customer = Customers::where('customer_id', $customerId)->first();
            $addressArray = [];
            $customerAddress = CustomerAddress::where('customer_id', $customer->customer_id)->where('default_address',1)->where('address_status',0)->first();
            if ($customerAddress != '') {
                $areaCustomer = Area::find($customerAddress->area_id);
                $areaName = $areaCustomer->area_name;
                $addressArray = ['addressName' => $customerAddress['customer_address'], 'AreaName' => $areaName];
            }
			$cust_mob = $customer->mobile_number_1;
        } else {
            $addressArray = [];
			$cust_mob = "";
        }
        $data = ['service' => $service, 'area' => $area, 'allServices' => $allServices, 'address' => $addressArray,'cust_phone' => $cust_mob];

        return response()->json([
            'status' => "success",
            'data' => $data,
            'message' => 'Disinfection service fetched successfully!'
        ], 200);
    }
    /**
     * function to get all details of disinfection services
     */
    function getDeepCleaning(Request $request)
    {
        $customerId = $request['customerId'];
        $service = ServiceType::where('service_type_name', 'Deep / Move In Move Out Cleaning')->first();
        $allServices = ServiceCategoryCost::select('service_id', 'category_id', 'sub_category_id', 'service_sub_funish_id', 'is_scrubbing', 'name', 'service_cost', 'status')
            ->where('service_id', $service->service_type_id)->get()->toArray();
        $area = Area::select('area_id', 'area_name')->where('area_status', 1)->where('web_status',1)->get();
        if ($customerId != '') {
            $customer = Customers::where('customer_id', $customerId)->first();
            $addressArray = [];
            $customerAddress = CustomerAddress::where('customer_id', $customer->customer_id)->where('default_address',1)->where('address_status',0)->first();
            if ($customerAddress != '') {
                $areaCustomer = Area::find($customerAddress->area_id);
                $areaName = $areaCustomer->area_name;
                $addressArray = ['addressName' => $customerAddress['customer_address'], 'AreaName' => $areaName];
            }
			$cust_mob = $customer->mobile_number_1;
        } else {
            $addressArray = [];
			$cust_mob = "";
        }
        $data = ['service' => $service, 'area' => $area, 'allServices' => $allServices, 'address' => $addressArray,'cust_phone' => $cust_mob];
        return response()->json([
            'status' => "success",
            'data' => $data,
            'message' => 'Deep cleaning service fetched successfully!'
        ], 200);
    }
    /**
     * function to get all details of sofa services
     */
    function getSofaCleaning(Request $request)
    {
        $customerId = $request['customerId'];
        $service = ServiceType::where('service_type_name', 'Sofa Cleaning Services')->first();
        $area = Area::select('area_id', 'area_name')->where('area_status', 1)->where('web_status',1)->get();
        $allServices = ServiceCategoryCost::select('service_id', 'category_id', 'sub_category_id', 'service_sub_funish_id', 'is_scrubbing', 'name', 'service_cost', 'status')
            ->where('service_id', $service->service_type_id)->get()->toArray();
        if ($customerId != '') {
            $customer = Customers::where('customer_id', $customerId)->first();
            $addressArray = [];
            $customerAddress = CustomerAddress::where('customer_id', $customer->customer_id)->where('default_address',1)->where('address_status',0)->first();
            if ($customerAddress != '') {
                $areaCustomer = Area::find($customerAddress->area_id);
                $areaName = $areaCustomer->area_name;
                $addressArray = ['addressName' => $customerAddress['customer_address'], 'AreaName' => $areaName];
            }
			$cust_mob = $customer->mobile_number_1;
        } else {
            $addressArray = [];
			$cust_mob = "";
        }
        $data = ['service' => $service, 'area' => $area, 'address' => $addressArray, 'allServices' => $allServices,'cust_phone' => $cust_mob];
        return response()->json([
            'status' => "success",
            'data' => $data,
            'message' => 'Sofa cleaning service fetched successfully!'
        ], 200);
    }
    /**
     * function to get all details of disinfection services
     */
    function getCarpetCleaning(Request $request)
    {
        $customerId = $request['customerId'];
        $service = ServiceType::where('service_type_name', 'Carpet Cleaning Services')->first();
        $area = Area::select('area_id', 'area_name')->where('area_status', 1)->where('web_status',1)->get();
        $allServices = ServiceCategoryCost::select('service_id', 'category_id', 'sub_category_id', 'service_sub_funish_id', 'is_scrubbing', 'name', 'service_cost', 'status')
            ->where('service_id', $service->service_type_id)->get()->toArray();
        if ($customerId != '') {
            $customer = Customers::where('customer_id', $customerId)->first();
            $addressArray = [];
            $customerAddress = CustomerAddress::where('customer_id', $customer->customer_id)->where('default_address',1)->where('address_status',0)->first();
            if ($customerAddress != '') {
                $areaCustomer = Area::find($customerAddress->area_id);
                $areaName = $areaCustomer->area_name;
                $addressArray = ['addressName' => $customerAddress['customer_address'], 'AreaName' => $areaName];
            }
			$cust_mob = $customer->mobile_number_1;
        } else {
            $addressArray = [];
			$cust_mob = "";
        }
        $data = ['service' => $service, 'area' => $area, 'address' => $addressArray, 'allServices' => $allServices,'cust_phone' => $cust_mob];
        return response()->json([
            'status' => "success",
            'data' => $data,
            'message' => 'Carpet cleaning service fetched successfully!'
        ], 200);
    }
    /**
     * function to get all details of disinfection services
     */
    function getMattressCleaning(Request $request)
    {
        $customerId = $request['customerId'];
        $service = ServiceType::where('service_type_name', 'Mattress Cleaning Services')->first();
        $allServices = ServiceCategoryCost::select('service_id', 'category_id', 'sub_category_id', 'service_sub_funish_id', 'is_scrubbing', 'name', 'service_cost', 'status')
            ->where('service_id', $service->service_type_id)->get()->toArray();
        $area = Area::select('area_id', 'area_name')->where('area_status', 1)->where('web_status',1)->get();
        if ($customerId != '') {
            $customer = Customers::where('customer_id', $customerId)->first();
            $addressArray = [];
            $customerAddress = CustomerAddress::where('customer_id', $customer->customer_id)->where('default_address',1)->where('address_status',0)->first();
            if ($customerAddress != '') {
                $areaCustomer = Area::find($customerAddress->area_id);
                $areaName = $areaCustomer->area_name;
                $addressArray = ['addressName' => $customerAddress['customer_address'], 'AreaName' => $areaName];
            }
			$cust_mob = $customer->mobile_number_1;
        } else {
            $addressArray = [];
			$cust_mob = "";
        }
        $data = ['service' => $service, 'area' => $area, 'address' => $addressArray, 'allServices' => $allServices,'cust_phone' => $cust_mob];
        return response()->json([
            'status' => "success",
            'data' => $data,
            'message' => 'Mattress cleaning service fetched successfully!'
        ], 200);
    }
 /**
     * function to get all details of disinfection services
     */
    function getFloorScrubbing(Request $request)
    {
        $customerId = $request['customerId'];
        $service = ServiceType::where('service_type_name', 'Floor Scrubbing')->first();
        $allServices = ServiceCategoryCost::select('service_id', 'category_id', 'sub_category_id', 'service_sub_funish_id', 'is_scrubbing', 'name', 'service_cost', 'status')
            ->where('service_id', $service->service_type_id)->get()->toArray();
        $area = Area::select('area_id', 'area_name')->where('area_status', 1)->where('web_status',1)->get();
        if ($customerId != '') {
            $customer = Customers::where('customer_id', $customerId)->first();
            $addressArray = [];
            $customerAddress = CustomerAddress::where('customer_id', $customer->customer_id)->where('default_address',1)->where('address_status',0)->first();
            if ($customerAddress != '') {
                $areaCustomer = Area::find($customerAddress->area_id);
                $areaName = $areaCustomer->area_name;
                $addressArray = ['addressName' => $customerAddress['customer_address'], 'AreaName' => $areaName];
            }
			$cust_mob = $customer->mobile_number_1;
        } else {
            $addressArray = [];
			$cust_mob = "";
        }
        $data = ['service' => $service, 'area' => $area, 'address' => $addressArray, 'allServices' => $allServices,'cust_phone' => $cust_mob];
        return response()->json([
            'status' => "success",
            'data' => $data,
            'message' => 'Floor scrubbing service fetched successfully!'
        ], 200);
    }

    /**
     * Author:Karthika
     * Date:20/08/2020
     * Function to save booking step 1
     */
    public function saveBookingOne(Request $request)
    {
        // return $request->all();
        // DB::beginTransaction();
        // try { 
        $services = ServiceType::where('service_type_id', $request['serviceId'])->first();
        // find($request->input('serviceId'));
        if ($services) {
            $service = $services->service_type_name;
            if ($service == 'Cleaning Services') {
                // $validData = Validator::make($request->all(), [
                //     'serviceId' => 'required',
                //     'noOfHours' => 'required',
                //     'cleaningMaterialStatus' => 'required',
                //     // 'noOfVisit' => 'required',
                //     'noOfMaids' => 'required',
                //     'extraServices'=>'required',
                //     'cleaning_material_fee'=>'required'
                // ]);
                // if ($validData->fails()) {
                //     return response()->json(
                //         [
                //             'status' => "error",
                //             'data' => null,
                //             'messages' => $validData->errors()
                //         ],
                //         400
                //     );
                // }
                $booking_id = $this->saveCleaning($request->all());
            }
            if ($service == 'Disinfection Services') {
                $validData = Validator::make($request->all(), [
                    'serviceId' => 'required',
                    'service_for' => 'required',
                ]);
                if ($request->input('service_for') == 'home') {
                    $validData1 = Validator::make($request->all(), [
                        'room_count' => 'required',
                        'building_type' => 'required',
                    ]);
                    if ($validData1->fails()) {
                        return response()->json(
                            [
                                'status' => "error",
                                'data' => null,
                                'messages' => $validData->errors()
                            ],
                            400
                        );
                    }
                } else  if ($request->input('service_for') == 'office') {
                    $validData2 = Validator::make($request->all(), [
                        'officeSqureFeet' => 'required',
                    ]);
                    if ($validData2->fails()) {
                        return response()->json(
                            [
                                'status' => "error",
                                'data' => null,
                                'messages' => $validData->errors()
                            ],
                            400
                        );
                    }
                }
                if ($validData->fails()) {
                    return response()->json(
                        [
                            'status' => "error",
                            'data' => null,
                            'messages' => $validData->errors()
                        ],
                        400
                    );
                }
                $booking_id =  $this->saveDisinfetction($request->all());
            }
            if ($service == 'Deep Cleaning Services') {
                $validData = Validator::make($request->all(), [
                    'serviceId' => 'required',
                    'service_for' => 'required',
                ]);
                if ($request->input('service_for') == 'home') {
                    $validData1 = Validator::make($request->all(), [
                        'room_count' => 'required',
                        'building_type' => 'required',
                        'floor_scrubbing' => 'required',
                        'furnished_home' => 'required'
                    ]);
                    if ($validData1->fails()) {
                        return response()->json(
                            [
                                'status' => "error",
                                'data' => null,
                                'messages' => $validData->errors()
                            ],
                            400
                        );
                    }
                } else  if ($request->input('service_for') == 'office') {
                    $validData2 = Validator::make($request->all(), [
                        'officeSqureFeet' => 'required',
                        'floor_scrubbing' => 'required',
                        'furnished_office' => 'required'
                    ]);
                    if ($validData2->fails()) {
                        return response()->json(
                            [
                                'status' => "error",
                                'data' => null,
                                'messages' => $validData->errors()
                            ],
                            400
                        );
                    }
                }
                if ($validData->fails()) {
                    return response()->json(
                        [
                            'status' => "error",
                            'data' => null,
                            'messages' => $validData->errors()
                        ],
                        400
                    );
                }
                $booking_id =  $this->saveDeepCleaning($request->all());
            }
            if ($service == 'Carpet Cleaning Services') {
                $validData = Validator::make($request->all(), [
                    'serviceId' => 'required',
                    'carpet_size' => 'required',
                    'no_carpets' => 'required',
                ]);
                if ($validData->fails()) {
                    return response()->json(
                        [
                            'status' => "error",
                            'data' => null,
                            'messages' => $validData->errors()
                        ],
                        400
                    );
                }
                $booking_id = $this->saveCarpetCleaning($request->all());
            }
            if ($service == 'Sofa Cleaning Services') {
                $validData = Validator::make($request->all(), [
                    'serviceId' => 'required',
                    'sofa_type' => 'required',
                ]);
                if ($request->input('sofa_type') == 'normal') {
                    $validData1 = Validator::make($request->all(), [
                        'no_of_seats' => 'required',
                    ]);
                    if ($validData1->fails()) {
                        return response()->json(
                            [
                                'status' => "error",
                                'data' => null,
                                'messages' => $validData->errors()
                            ],
                            400
                        );
                    }
                } else  if ($request->input('service_for') == 'Lshape') {
                    $validData2 = Validator::make($request->all(), [
                        'no_of_seats' => 'required',
                        'seat_type' => 'required'
                    ]);
                    if ($validData2->fails()) {
                        return response()->json(
                            [
                                'status' => "error",
                                'data' => null,
                                'messages' => $validData->errors()
                            ],
                            400
                        );
                    }
                }
                if ($validData->fails()) {
                    return response()->json(
                        [
                            'status' => "error",
                            'data' => null,
                            'messages' => $validData->errors()
                        ],
                        400
                    );
                }
                $booking_id =  $this->saveSofaCleaning($request->all());
            }
            if ($service == 'Matress Services') {
                $validData = Validator::make($request->all(), [
                    'serviceId' => 'required',
                    'mattress_type' => 'required',
                    'noOfMattresses' => 'noOfMattresses'
                ]);
                if ($validData->fails()) {
                    return response()->json(
                        [
                            'status' => "error",
                            'data' => null,
                            'messages' => $validData->errors()
                        ],
                        400
                    );
                }
                $booking_id = $this->saveMattressCleaning($request->all());
            }

            // DB::commit();
            return response()->json(
                [
                    'status' => 'success',
                    'messages' => 'Booking time details saved successfully.',
                    'data' => [
                        'id' =>  $booking_id
                    ]
                ],
                200
            );
        } else {
            return response()->json(
                [
                    'status' => 'Failed',
                    'messages' => 'Service not found.',
                ]
            );
        }
        // } catch (\Exception $e) {
        //     dd($e->getMessage());
        //     DB::rollBack();
        //     return response()->json([
        //         'status' => "Failed",
        //         'data' => [],
        //         'messages' => array('Failed' => array('Exception error occured'))
        //     ]);
        // }
    }
    /**
     * Author:Karthika
     * Date:20/08/2020
     * Function to save some details in second step in the booking form
     */
    public function saveBookingTwo(Request $request)
    {
        // $validData = Validator::make($request->all(), [
        //     'bookingId' => 'required',

        // ]);
        // if ($validData->fails()) {
        //     return response()->json(
        //         [
        //             'status' => "error",
        //             'data' => null,
        //             'messages' => $validData->errors()
        //         ],
        //         400
        //     );
        // }
        // DB::beginTransaction();
        // try { 
        $bookings = Bookings::find($request['bookingId']);
        if (!$bookings) {
            return response()->json(
                [
                    'status' => 'Failed',
                    'messages' => 'Booking not found.',
                ]
            );
        }
        $dateIntervel = new DateTime($request['cleaning_date']);
        $dateIntervel->add(new DateInterval('P' . $request['monthDurations'] . 'M'));
        $actual_ending_date = $dateIntervel->format('Y-m-d');
        $ending_date = $dateIntervel->format('Y-m-d');  // 2016-01-02
        $endDate = new DateTime($ending_date);

        $bookings->booking_type = $request['how_often'];
        $bookings->booking_category = '';
        $bookings->service_end = 0;
        $bookings->service_end_date = $ending_date;
        $bookings->service_actual_end_date = $actual_ending_date;
        $bookings->pending_amount = 0;
        $bookings->discount = $request['discount'];
        $bookings->total_amount =  $request['total_amount'];
        $bookings->price_per_hr = $request['hour_rate'];
        $bookings->service_charge = $request['service_charge'];
        $bookings->vat_charge = $request['vat_charge'];
        $bookings->net_cleaning_fee = $request['net_cleaning_fee'];
        $bookings->net_service_charge = $request['net_service_charge'];
        $bookings->net_discount = $request['net_discount'];

        $bookings->net_vat_charge = $request['net_vat_charge'];
        $bookings->total_net_amount = $request['total_net_amount'];
        $bookings->cleaning_material_fee = $request['cleaning_material_fee'];

        $bookings->month_durations = $request['monthDurations']; //new field
        $bookings->service_start_date = $request['cleaning_date']; //
        $bookings->service_week_day = date('w', strtotime($request['cleaning_date']));

        // $bookings->time_from = $request['time_from'];//
        // $bookings->time_to = $request['time_to'];//
        $bookings->save();
        // DB::commit();
        return response()->json(
            [
                'status' => 'success',
                'messages' => 'Booking time details saved successfully.',
                'data' => [
                    'id' =>  $request->input('bookingId')
                ]
            ],
            200
        );
        // } catch (\Exception $e) {
        //     DB::rollBack();
        //     return response()->json([
        //         'status' => "Failed",
        //         'data' => [],
        //         'messages' => array('Failed' => array('Exception error occured'))
        //     ]);
        // }
    }
    /**
     * api to save cleaning services
     * Author:Karthika
     * Date:21/08/2020
     */
    private static function saveCleaning($request)
    {
        // return $request['bookingId'];

        if (isset($request['bookingId']) && $request['bookingId'] != '') {
            $bookings = Bookings::find($request['bookingId']);
            if ($bookings->customer_id == 0) {
                $bookings->customer_id = 0; //
                $bookings->customer_address_id = 0; //
            }
        } else {
            $bookings = new Bookings();
            $bookings->customer_id = 0; //
            $bookings->customer_address_id = 0; //
        }
        if ($request['extraServices'] != 0) {
            $extraService = ExtraServices::find($request['extraServices']);
            $extraServiceName = $extraService->service;
            if ($extraServiceName == 'Fridge Cleaning') {
                $bookings->fridge_cleaning = 1;
            }
            if ($extraServiceName == 'Ironing') {
                $bookings->ironing_services = 1;
            }
            if ($extraServiceName == 'Oven Cleaning') {
                $bookings->oven_cleaning = 1;
            }
            if ($extraServiceName == 'Interior Windows') {
                $bookings->interior_window_clean = 1;
            }
        }
        $bookings->service_type_id = $request['serviceId'];
        $bookings->no_of_maids = $request['noOfMaids'];
        $bookings->no_of_hrs = $request['noOfHours'];
        // $bookings->number_of_visits = $request->input('noOfVisit');
        $bookings->cleaning_material = $request['cleaningMaterialStatus'];
        $bookings->cleaning_material_fee = $request['cleaning_material_fee'];



        $dt = new DateTime;
        $bookings->reference_id = '';

        $bookings->maid_id = 1; //
        $bookings->service_start_date = '2020-07-19'; //
        $bookings->service_week_day = '2';
        $bookings->time_from = '08:00:00';
        $bookings->time_to = '10:00:00';
        $bookings->booking_type = '';
        $bookings->booking_category = '';
        $bookings->service_end = 0;
        $bookings->service_end_date = '2020-07-19';
        $bookings->service_actual_end_date = '2020-07-19';
        $bookings->pending_amount = 100;
        $bookings->discount = 0;
        $bookings->total_amount = 100;
        $bookings->is_locked = 0;
        $bookings->booked_by = 1;

        $bookings->booked_datetime = $dt->format('y-m-d H:i:s');
        $bookings->booking_status = 1;
        $bookings->save();
        return $bookings->booking_id;
    }
    /**
     * api to save disinfection cleaning services
     * Author:Karthika
     * Date:21/08/2020
     */
    private static function saveDisinfetction($request)
    {
        $bookings = new Bookings();
        $bookings->service_type_id = $request->input('serviceId');
        $bookings->service_for = $request->input('service_for'); //
        if ($request->input('service_for') == 'home') {

            $bookings->room_count = $request->input('room_count');
            $bookings->building_type = $request->input('building_type');
        }
        if ($request->input('service_for') == 'office') {
            $bookings->officeSqureFeet = $request->input('officeSqureFeet');
        }
        $dt = new DateTime;
        $bookings->booked_datetime = $dt->format('y-m-d H:i:s');
        $bookings->booking_status = 1;
        $bookings->save();
        return $bookings->booking_id;
    }
    /**
     * api to save deep cleaning services
     * Author:Karthika
     * Date:21/08/2020
     */
    private static function saveDeepCleaning($request)
    {
        $bookings = new Bookings();
        $bookings->service_type_id = $request->input('serviceId');
        $bookings->service_for = $request->input('service_for'); //
        if ($request->input('service_for') == 'home') {
            $bookings->room_count = $request->input('room_count');
            $bookings->building_type = $request->input('building_type');
            $bookings->floor_scrubbing = $request->input('floor_scrubbing');
            $bookings->furnished_home = $request->input('furnished_home');
        }
        if ($request->input('service_for') == 'office') {
            $bookings->floor_scrubbing = $request->input('floor_scrubbing');
            $bookings->officeSqureFeet = $request->input('officeSqureFeet');
            $bookings->furnished_office = $request->input('furnished_office');
        }
        $dt = new DateTime;
        $bookings->booked_datetime = $dt->format('y-m-d H:i:s');
        $bookings->booking_status = 1;
        $bookings->save();
        return $bookings->booking_id;
    }
    /**
     * api to save carpet cleaning services
     * Author:Karthika
     * Date:21/08/2020
     */
    private static function saveCarpetCleaning($request)
    {
        $bookings = new Bookings();
        $bookings->service_type_id = $request->input('serviceId');
        // $bookings->carpet_size = $request->input('carpet_size'); //
        // $bookings->no_carpets = $request->input('no_carpets'); //
        $carpetSize =  $request->input('carpet_size'); //
        $no_carpets =  $request->input('no_carpets'); //
        $dt = new DateTime;
        $bookings->booked_datetime = $dt->format('y-m-d H:i:s');
        $bookings->booking_status = 1;
        $bookings->save();
        foreach ($carpetSize as $key => $size) {
            $bookingServices = new BookingServices();
            $bookingServices->service_id =  $request->input('serviceId');
            $bookingServices->booking_id =  $bookings->booking_id;
            $bookingServices->carpet_size =  $size;
            $bookingServices->no_carpets =  $no_carpets[$key];
            $bookingServices->save();
        }
        return $bookings->booking_id;
    }

    /**
     * api to save sofa cleaning services
     * Author:Karthika
     * Date:21/08/2020
     */
    private static function saveSofaCleaning($request)
    {
        $bookings = new Bookings();
        $bookings->service_type_id = $request->input('serviceId');
        // $bookings->sofa_type = $request->input('sofa_type'); //
        $sofa_type = $request->input('sofa_type');
        $ThreeSeatNormal = $request->input('ThreeSeatNormal');
        $FourSeatNormal = $request->input('FourSeatNormal');

        $ThreeSeatLshape = $request->input('ThreeSeatLshape');
        $FourSeatLshape = $request->input('FourSeatLshape');

        $NrmlThreeSeatCount = $request->input('NrmlThreeSeatCount');
        $NrmlFourSeatCount = $request->input('NrmlFourSeatCount');

        $LshapeThreeSeatCount = $request->input('LshapeThreeSeatCount');
        $LshapeNrmlFourSeatCount = $request->input('LshapeNrmlFourSeatCount');

        // if ($request->input('sofa_type') == 'normal') {
        //     $bookings->no_of_seats = $request->input('no_of_seats');
        // }
        // if ($request->input('sofa_type') == 'Lshape') {
        //     $bookings->no_of_seats = $request->input('no_of_seats');
        //     $bookings->seat_type = $request->input('seat_type');
        // } 
        $dt = new DateTime;
        $bookings->booked_datetime = $dt->format('y-m-d H:i:s');
        $bookings->booking_status = 1;
        $bookings->save();
        foreach ($sofa_type as $key => $type) {
            $bookingServices = new BookingServices();
            $bookingServices->service_id =  $request->input('serviceId');
            $bookingServices->booking_id =  $bookings->booking_id;
            if ($type == 'normal') {
                $bookingServices->ThreeSeatNormal = $ThreeSeatNormal;
                $bookingServices->NrmlThreeSeatCount = $NrmlThreeSeatCount;
                $bookingServices->FourSeatNormal = $FourSeatNormal;
                $bookingServices->NrmlFourSeatCount = $NrmlFourSeatCount;
            }
            if ($type == 'Lshape') {
                $bookingServices->FourSeatLshape = $FourSeatLshape;
                $bookingServices->LshapeThreeSeatCount = $LshapeThreeSeatCount;
                $bookingServices->ThreeSeatLshape = $ThreeSeatLshape;
                $bookingServices->LshapeNrmlFourSeatCount = $LshapeNrmlFourSeatCount;
            }
            $bookingServices->save();
        }
        return $bookings->booking_id;
    }
    /**
     * api to save mattress cleaning services
     * Author:Karthika
     * Date:21/08/2020
     */
    private static function saveMattressCleaning($request)
    {
        $bookings = new Bookings();
        $bookings->service_type_id = $request->input('serviceId');
        $mattress_type = $request->input('mattress_type'); //
        $noOfMattresses = $request->input('noOfMattresses'); //

        // $bookings->mattress_type = $request->input('mattress_type'); //
        // $bookings->noOfMattresses = $request->input('noOfMattresses'); //
        $dt = new DateTime;
        $bookings->booked_datetime = $dt->format('y-m-d H:i:s');
        $bookings->booking_status = 1;
        $bookings->save();
        foreach ($mattress_type as $key => $size) {
            $bookingServices = new BookingServices();
            $bookingServices->service_id =  $request->input('serviceId');
            $bookingServices->booking_id =  $bookings->booking_id;
            $bookingServices->mattress_type =  $size;
            $bookingServices->noOfMattresses =  $noOfMattresses[$key];
            $bookingServices->save();
        }
        return $bookings->booking_id;
    }
    /**
     * API for check coupon
     * Author:Karthika
     * Date:21/08/2020
     */
    public function checkCoupon(Request $request)
    {
        $validData = Validator::make($request->all(), [
            'serviceId' => 'required',
            'coupon' => 'required',
        ]);
        if ($validData->fails()) {
            return response()->json(
                [
                    'status' => "error",
                    'data' => null,
                    'messages' => $validData->errors()
                ],
                400
            );
        }
        $coupon = CouponCode::where('coupon_name', $request->input('coupon'))->where('status', 1)->first();
        if (!$coupon) {
            return response()->json(
                [
                    'status' => "Failed",
                    'data' => null,
                    'messages' => 'Coupon not found.'
                ]
            );
        }
        if ($coupon) {
            $hoursSelected = $request->input('noOfHours'); //
            $dateBooked = $request->input('dateBooked'); //Selected date from calendar
            $couponType = $request->input('couponType'); // 
            $customerId = $request->input('customerId');
            $bookingId = $request->input('bookingId');
            $customerCoupons = CustomerCoupons::where('customer_id', $customerId)->where('coupon_id', $coupon->coupon_id)->where('reference_id', $bookingId)->first();
            if ($couponType == 'FT') { //fisrt time coupon usage
                if ($customerCoupons) {
                    return response()->json(
                        [
                            'status' => "Failed",
                            'data' => null,
                            'messages' => 'First time use coupon already used.'
                        ]
                    );
                }
            }
            $datetime = DateTime::createFromFormat('Y-m-d', $dateBooked);
            $weekCount = $datetime->format('w');
            $couponWeeks = $coupon->valid_week_day;
            $arrayWeeks = explode(",", $couponWeeks);
            if ($hoursSelected >= $coupon->min_hrs && $dateBooked <= $coupon->expiry_date && (!in_array($weekCount, $arrayWeeks))) { // minimum hours calculation,Expiry date calculation,day wise calculation
                return response()->json(
                    [
                        'status' => "Success",
                        'data' => null,
                        'messages' => 'Valid coupon code.'
                    ]
                );
            } else {
                return response()->json(
                    [
                        'status' => "Failed",
                        'data' => null,
                        'messages' => 'Less booking hours selected'
                    ]
                );
            }
        }
    }
    /**
     * function to get all data in second step
     * Author:Karthika
     * Date:03/09/2020
     */
    public function getBookingTwo(Request $request)
    {
        $customerBooking = Bookings::find($request['bookingId']);
        return response()->json(
            [
                'status' => 'success',
                'messages' => 'Customer address fetched successfully.',
                'data' => $customerBooking
            ],
            200
        );
    }
    /**
     * function to get all data in second step
     * Author:Karthika
     * Date:03/09/2020
     */
    public function getBookingOne(Request $request)
    {
        $customerBooking = Bookings::find($request['bookingId']);
        return response()->json(
            [
                'status' => 'success',
                'messages' => 'Customer address fetched successfully.',
                'data' => $customerBooking
            ],
            200
        );
    }
    public function viewBookings(Request $request)
    {
        $bookings = Bookings::select(
        'booking_id',
        'no_of_maids',
        'reference_id',
        'service_type_id',
        'service_start_date',
        'time_from',
        'booking_type',
        'service_actual_end_date',
        'service_end',
        'booked_from',
        'pay_by',
        'is_cancelled',
        'booking_status',
        'total_amount'
        )
        ->where(function ($query) {
            $query->whereColumn('booking_common_id', 'booking_id')->orWhere('booking_common_id', '=', null);
        })
        ->where('booking_status','!=',3)
        ->where('customer_id', $request['customerId'])
        //->where('booked_from', 'W')
        ->where('is_cancelled', '!=', 'yes')
        ->orderBy('booking_id', 'DESC')
        ->get();
        $bookingArray = [];
        foreach ($bookings as $book) {
            $onlinePayment =  OnlinePayment::where('reference_id', $book['reference_id'])->first();
            if (($onlinePayment && $onlinePayment->payment_status != 'success') || $book['pay_by'] == 'Cash') {
                $payButton = 'active';
            } else {
                $payButton = 'inactive';
            }

            if ($book->service_end == 0 || $book->service_actual_end_date >= date('Y-m-d')) {
                $service = ServiceType::find($book['service_type_id']);
                $bookingType = $book['booking_type'];
                if ($bookingType == 'OD') {
                    $type = 'One Day Booking';
                } elseif ($bookingType == 'WE') {
                    $type = 'Weekly Booking';
                } elseif ($bookingType == 'BW') {
                    $type = 'Bi-Weekly Booking';
                }
                if ($book['reference_id'] == '') {
                    $referenceId = $book['booking_id'];
                } else {
                    $referenceId = $book['reference_id'];
                }
                $start = date("g:i a", strtotime($book['time_from']));
                $date = date('j M, Y', strtotime($book['service_start_date']));
                $no_of_maids = $book['no_of_maids'] > 0 ? $book['no_of_maids'] : 1;
                $bookingArray[] = ['booking_id' => $book['booking_id'], 'no_of_maids' => $no_of_maids,'reference_id' => $referenceId, 'service' => $service['service_type_name'], 'service_start_date' => $date, 'total_amount'=>$book['total_amount']*$no_of_maids,'time_from' => $start, 'type' => $type, 'onlinePayment' => $onlinePayment, 'payButton' => $payButton];
            }
        }
        $pastBookings = DayServices::select('booking_id', 'day_service_id', 'start_time', 'service_status', 'service_date')->where('customer_id', $request['customerId'])->get();
        $pastBookingArray = [];
        foreach ($pastBookings as $pbook) {
            if ($pbook->service_status == 2) {
                // dump($pbook);
                $pbookings = Bookings::where('booking_id', $pbook['booking_id'])->first();
                $service = ServiceType::find($pbookings['service_type_id']);
                if ($service) {
                    $serviceName = $service['service_type_name'];
                } else {
                    $serviceName = '';
                }
                $pbookingType = $pbookings['booking_type'];
                if ($pbookingType == 'OD') {
                    $ptype = 'One Day Booking';
                } elseif ($pbookingType == 'WE') {
                    $ptype = 'Weekly Booking';
                } elseif ($pbookingType == 'BW') {
                    $ptype = 'Bi-Weekly Booking';
                }
                $start = date("g:i a", strtotime($pbook['start_time']));
                $date = date('j M, Y', strtotime($pbook['service_date']));
                $pastBookingArray[] = ['booking_id' => $pbook['booking_id'], 'reference_id' => $pbookings['reference_id'], 'service' => $serviceName, 'service_date' => $date, 'time_from' => $start, 'type' => $ptype];
            }
        }
        // dd($pastBookings);
        $data['upcoming'] = $bookingArray;
        $data['past'] = $pastBookingArray;
        /********************************************************************************** */
        $cancelled_bookings = Bookings::
        select(
                'bookings.*',
                'st.service_type_name',
                'bt.booking_type_name',
                'op.amount as online_payment_amount',
                DB::raw('LOWER(op.payment_status) as online_payment_payment_status'),
                DB::raw('IFNULL(op.transaction_charge,0) as online_payment_transaction_charge'),
            )
        ->leftJoin('service_types as st', 'bookings.service_type_id', 'st.service_type_id')
        ->leftJoin('booking_types as bt', 'bookings.booking_type', 'bt.booking_type')
        ->leftJoin('online_payments as op', 'bookings.reference_id', 'op.reference_id')
        ->where(function ($query) {
            $query->whereColumn('bookings.booking_common_id', 'bookings.booking_id')
            ->orWhere('bookings.booking_common_id', '=', null);
        })
        ->where('bookings.customer_id',$request['customerId'])
        ->where('bookings.booking_status',2)
        ->orderBy('bookings.booking_id', 'DESC')
        ->get();
        foreach($cancelled_bookings as $key => $booking){
                if($booking->pay_by == 'Cash'){
                    $transaction_charge = 0;
                    $cancelled_bookings[$key]->total_amount = ($booking->total_amount * $booking->no_of_maids) + $transaction_charge;
                }
                else{
                    $transaction_charge = $booking->online_payment_transaction_charge;
                    $cancelled_bookings[$key]->total_amount = ($booking->total_amount * $booking->no_of_maids) + $transaction_charge;
                }
        }
        $data['cancelled'] = $cancelled_bookings;
        /********************************************************************************** */
        return response()->json(
            [
                'status' => 'success',
                'messages' => 'Booking details fetched successfully.',
                'data' => $data,
            ],
            200
        );
    }
	
	public function booking_detail(Request $request)
    {
        $bookings = Bookings::select('booking_id', 'reference_id', 'service_type_id','booking_status','total_amount','service_charge','vat_charge')->where('reference_id', $request['ref_id'])->first();
        if($bookings)
		{
			$service = ServiceType::find($bookings->service_type_id);
			$bookings->service_type_name = $service['service_type_name'];
			if($request['payMode'] == "applePay" || $request['payMode'] == "gPay")
			{
				// $transaction_charg = $bookings->total_amount * 0.03;
				$transaction_charg = 0;
				$transaction_charge = number_format((float)$transaction_charg, 2, '.', '');
				$bookings->total_amount = ($bookings->total_amount + $transaction_charge);
			}
				
			return response()->json(
				[
					'status' => 'success',
					'messages' => 'Booking details fetched successfully.',
					'data' => $bookings,
				],
				200
			);
		} else {
			return response()->json(
				[
					'status' => 'error',
					'messages' => 'No booking found.',
				],
				200
			);
		}
        
    }

    // customer app booking history code begins here
    function booking_history(Request $request)
    {
        if(!isset($request->user_id))
        return response()->json(['status'=>'error','message'=>'unsatisfied parameters']);
        $date=date('Y-m-d');
        $service_week_day = date('w',strtotime($date));
        $deletes = $this->get_booking_deletes_by_date($date);
        $sql="SELECT bookings.booking_type,
        bookings.booking_id,bookings.reference_id,bookings.booking_status,customer_addresses.customer_address,service_types.service_type_name,bookings.time_from,bookings.time_to,bookings.total_amount,
        bookings.service_week_day,bookings.service_start_date,'no' deleted,'no' day_service_id
        FROM bookings 
        INNER JOIN customers on customers.customer_id=bookings.customer_id
        INNER JOIN customer_addresses on bookings.customer_address_id=customer_addresses.customer_address_id
        INNER JOIN areas on customer_addresses.area_id=areas.area_id
        INNER JOIN service_types on service_types.service_type_id=bookings.service_type_id
        WHERE bookings.customer_id={$request->user_id} 
        AND date(bookings.service_start_date)>='{$date}'
        AND  bookings.booking_status!=3
        /*AND bookings.booking_id not in(SELECT booking_id from booking_deletes WHERE date(service_date)>='{$date}')*/
        AND date(bookings.service_actual_end_date)>='{$date}' AND (bookings.service_end = 1 OR bookings.service_end = 0) AND bookings.booking_type='OD'";
        $past="select * from(SELECT 
        bookings.booking_type,bookings.reference_id,day_services.service_date dd,day_services.service_status,day_services.day_service_id,bookings.booking_id,bookings.booking_status,customer_addresses.customer_address,service_types.service_type_name,bookings.time_from,bookings.time_to,bookings.total_amount,
              bookings.service_week_day,bookings.service_start_date,'no' deleted
              FROM bookings 
              INNER JOIN customers on customers.customer_id=bookings.customer_id
              INNER JOIN customer_addresses on bookings.customer_address_id=customer_addresses.customer_address_id
              INNER JOIN areas on customer_addresses.area_id=areas.area_id
              INNER JOIN service_types on service_types.service_type_id=bookings.service_type_id
              
              INNER JOIN day_services ON day_services.booking_id=bookings.booking_id
              WHERE bookings.customer_id={$request->user_id} 
              AND  bookings.booking_status!=3
              AND date(bookings.service_start_date)<'{$date}'
              AND (bookings.service_end = 1 OR bookings.service_end = 0) UNION ALL
              SELECT 
        bookings.booking_type,bookings.reference_id,booking_deletes.service_date dd,'no' service_status,'no' day_service_id,bookings.booking_id,bookings.booking_status,customer_addresses.customer_address,service_types.service_type_name,bookings.time_from,bookings.time_to,bookings.total_amount,
              bookings.service_week_day,bookings.service_start_date,1 deleted
              FROM bookings 
              INNER JOIN customers on customers.customer_id=bookings.customer_id
              INNER JOIN customer_addresses on bookings.customer_address_id=customer_addresses.customer_address_id
              INNER JOIN areas on customer_addresses.area_id=areas.area_id
              INNER JOIN service_types on service_types.service_type_id=bookings.service_type_id
              INNER JOIN booking_deletes ON booking_deletes.booking_id=bookings.booking_id
              
              WHERE bookings.customer_id={$request->user_id}
              AND  bookings.booking_status!=3
              AND date(bookings.service_start_date)<'{$date}'
              AND (bookings.service_end = 1 OR bookings.service_end = 0))as t ORDER BY date(dd) ASC";
        $upcoming2="SELECT bookings.booking_type,
        bookings.booking_id,bookings.reference_id,bookings.booking_status,customer_addresses.customer_address,service_types.service_type_name,bookings.time_from,bookings.time_to,bookings.total_amount,
        bookings.service_week_day,bookings.service_start_date,'no' deleted,'no' day_service_id
        FROM bookings
        INNER JOIN customers on customers.customer_id=bookings.customer_id
        INNER JOIN customer_addresses on bookings.customer_address_id=customer_addresses.customer_address_id
        INNER JOIN areas on customer_addresses.area_id=areas.area_id
        INNER JOIN service_types on service_types.service_type_id=bookings.service_type_id
        WHERE bookings.customer_id={$request->user_id}
        AND  bookings.booking_status!=3
        AND bookings.booking_type!='OD'";
        $upcomming=DB::select($sql);
        $upcoming2=DB::select($upcoming2);
        $past=DB::select($past);
        foreach($past as $k=>$booking){
            if($booking->booking_status==0)
         $status='pending';
         elseif($booking->booking_status==1)
         $status='working';
         elseif($booking->booking_status==2)
         $status='Cancelled';
         if($booking->deleted==1)
          $status='cancelled';
          if($booking->service_status=='1')
          $status='onprogress';
          if($booking->service_status=='2')
          $status='finished';
          $past[$k]->booking_status=$status;
        }
        foreach($upcomming as $k=>$p){
        $upcomming[$k]->booking_status=$this->getupcomingod($p->booking_id);
        }
        foreach($upcoming2 as $k=>$p){
            $upcoming2[$k]->service_start_date=$this->getupcomingnotod($p->booking_id);
            $upcoming2[$k]->booking_status=$this->getupcomingnotodstatus($p->booking_id,$upcoming2[$k]->service_start_date);
        }
        $upcomming=array_merge($upcomming,$upcoming2);
        foreach($upcomming as $k=>$p){
            if($p->service_start_date=='unset')
            unset($upcomming[$k]);
        }
        foreach($upcomming as $k=>$p){
            if($p->booking_status=='finished'){
                $upcomming[$k]->dd= $upcomming[$k]->service_start_date;
                $past[]=$upcomming[$k];
                unset($upcomming[$k]);
            }
        }
        usort($upcomming,function($a,$b){
            $t1 = strtotime($a->service_start_date);
            $t2 = strtotime($b->service_start_date);
            return $t1 - $t2;
        });
        return response()->json(['status'=>'success','upcomming'=>$upcomming,'past'=>$past]);
    }
	function get_booking_deletes_by_date($service_date, $service_end_date = NULL)
	{
        $res=DB::table('booking_deletes')->select('booking_id');
                if($service_end_date != NULL)
                {
                    $res->whereBetween('service_date',[$service_date,$service_end_date]);
                }
                else
                {
                    $res->where('service_date',$service_date);
                }
		return $res->get();
	}
	function getupcomingod($bid)
    {
        $status='pending';
        $booking=DB::table('bookings')->where('booking_id',$bid)->where('booking_status','!=',3)->first();
        if(isset($booking) && $booking) {
            if($booking && $booking->booking_status == 2)
		{
			$status='cancelled';
			return $status;
		}
        if($booking && $booking->booking_status == 1)
		{
			$status='confirmed';
			return $status;
		}
        $dayservice=DB::table('day_services')->where('booking_id',$bid)
        ->whereRaw("date(service_date)='{$booking->service_start_date}'")
        ->first();
        if($dayservice){
            if($dayservice->service_status=='0')
            $status='started';
            elseif($dayservice->service_status=='1')
            $status='started';
            elseif($dayservice->service_status=='2')
            $status='finished';
            elseif($dayservice->service_status=='3')
            $status='pending';
        }
        $booking_delete=DB::table('booking_deletes')
        ->where('booking_id',$bid)
        ->whereRaw("date(service_date)='{$booking->service_start_date}'")->first();
        if($booking_delete)
        $status='cancelled';
        return $status;
        }
        return $status;
    }
	function getupcomingnotod($bid){
        $date='unset';
        $booking=DB::table('bookings')->where('booking_id',$bid)->where('booking_status','!=',3)->first();
        if(isset($booking) && $booking) {
        $date=$booking->service_start_date;
        if($booking && $booking->service_end=='1' && date('Y-m-d',strtotime($booking->service_end_date))<date('Y-m-d'))
        $date='unset';
        else{
            if($booking && $booking->booking_type=='WE'){
                $day='+'.ceil(date_diff(date_create($booking->service_start_date),date_create(date('Y-m-d')))->format("%R%a")/7)*7.;
                
            }else{
                $day='+'.ceil(date_diff(date_create($booking->service_start_date),date_create(date('Y-m-d')))->format("%R%a")/14)*14.;
            }
            $day.=' day';
            $date=date('Y-m-d',strtotime($day,strtotime($date)));
        }
        }
        return $date;
    }
	function getupcomingnotodstatus($bid,$bdate){
        $status='pending';
        if($bdate=='unset')
        return $bdate;
        else{
			$booking=DB::table('bookings')->where('booking_id',$bid)->where('booking_status','!=',3)->first();
			if(isset($booking) && $booking->booking_status == 2)
			{
				$status='cancelled';
				return $status;
			}
            $booking_delete=DB::table('booking_deletes')
        ->where('booking_id',$bid)
        ->whereRaw("date(service_date)='{$bdate}'")->first();
        if($booking_delete)
        $status='cancelled';
        }
        return $status;
    }
    // customer app booking history code ends here
    //customer app booking details page code starts here
    function jobdetails(Request $request)
    {
        if(!isset($request->data))
        return['status'=>'error','message'=>'Unsatisfied Parameters'];
        $data=json_decode(base64_decode($request->data),1);
        if(!$data || count($data)!=4)
        return['status'=>'error','message'=>'Parameters mismatch'];
        $booking=Bookings::Find($data['id']);
        if(!$booking)
        return['status'=>'error','message'=>'No Such Bookings'];
		
		$payButton = 'active';
		if($data['id'] != "")
		{
			$onlinePayment =  OnlinePayment::where('booking_id',$data['id'])->first();
			if (!$onlinePayment)
			{
				$payButton = 'active';
			} else {
				if($onlinePayment->payment_status !='success') {
					$payButton = 'active';
				} else {
					$payButton = 'inactive';
				}
			}	
		}
		
		if($data['dayid'] != 'no')
		{
			$dayservice=DB::table('day_services')->where('day_service_id',$data['dayid'])->first();
			if ($dayservice)
			{
				if($dayservice->payment_status ==1) {
					$payButton = 'inactive';
				}
			}
		}

        $datas['id']=$data['id'];
		if($booking->booking_status == 2)
		{
			$datas['status']='cancelled';
		} else if($booking->booking_status == 1) {
            $datas['status']='confirmed';
        }  else {
			$datas['status']=$this->getbookingstatus($data['id'],$data['date']);
		}
        $datas['paystatus']=$payButton;
        $datas['dayserviceid']=$data['dayid'];
        $datas['servicedate']=$data['date'];
        $datas['material']=$booking->cleaning_material;
        $datas['date']=$data['date'];
        $datas['durartion']=$booking->no_of_hrs!=''?$booking->no_of_hrs:abs(strtotime($booking->time_from) - strtotime($booking->time_to))/3600;
        $datas['frequency']=$booking->booking_type;
        $datas['maids']=$booking->no_of_maids;
        $datas['time']=$booking->time_from;
        $address=DB::table('customer_addresses')->where('customer_address_id',$booking->customer_address_id)->first();
        $add=$address->customer_address;
        $add.=$address->building!=''?', '.$address->building:'';
        $datas['address']=$add;
        $datas['price']=$booking->service_charge;
        $datas['vat']=$booking->vat_charge;
        $datas['discount']=$booking->discount;
        $datas['schedule']='0';
        $datas['total']=$booking->total_amount;
        return['status'=>'success','data'=>$datas];
    }
    function getbookingstatus($id,$date)
    {
        $delete=DB::table('booking_deletes')->where('booking_id',$id)->where('service_date',$date)->first();
        if($delete) return 'cancelled';
        $dayservice=DB::table('day_services')->where('booking_id',$id)->where('service_date',$date)->first();
        if($dayservice && $dayservice->service_status=='1') return 'started';
        elseif($dayservice && $dayservice->service_status=='2') return 'finished';
        else return 'pending';
    }
    //customer app booking details page code ends here
    public function getServicesAreas()
    {
        $area = Area::select('area_id','area_name','a_latitude','a_longitude')->where('area_status',1)->where('web_status',1)->get();
        $areaArray = [];
		foreach($area as $val) {
			if($val['a_latitude'] != "")
			{
				$latitude = $val['a_latitude'];
			} else {
				$latitude = "";
			}
			
			if($val['a_longitude'] != "")
			{
				$longitude = $val['a_longitude'];
			} else {
				$longitude = "";
			}
			$areaArray[] = ['area_id'=>$val['area_id'],'area_name'=>$val['area_name'],'a_latitude'=>$latitude,'a_longitude'=>$longitude];
		}
        return response()->json([
            'status' => "success",
            'data' => $areaArray,
            'message' =>'All areas listed successfully!'
            ], 200, array(), JSON_PRETTY_PRINT);
    }
    public function getCleaningServices()
    {
        $defaultService = ServiceType::select(
            'service_type_id',
            'service_type_name',
            'service_type_name',
            'detail','cleaning_icon',
            'url_slug',
            DB::raw('CONCAT("'.Config::get('url.webview_base').'",url_slug,"?mobile") as webview_url'),
            'web_order_id',
            )
            ->where('web_status',1)
            ->orderBy('web_order_id', 'ASC')
            ->get();
        return response()->json([
            'status' => "success",
            'data' => $defaultService,
			'appUrl' => env('SERVICE_URL'),
            'message' =>'All services listed successfully!'
            ], 200, array(), JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES);
    }
    public function notifications(Request $request)
	{
        $input['user_id'] = $request->user_id;
        $input['token'] = $request->token;
        /******************************************************************************* */
        $response['data'] = DB::table('customer_notifications as cn')
            ->select(
                'cn.id',
                'cn.title',
                'b.reference_id as booking_ref_id',
                'cn.content as message',
                //DB::raw('(CASE WHEN cn.read_at IS NOT NULL THEN true ELSE false END) as read_status'),
                'cn.created_at as time',
                'c.customer_name',
            )
            ->leftJoin('bookings as b', 'cn.booking_id', 'b.booking_id')
            ->leftJoin('customers as c', 'cn.customer_id', 'c.customer_id')
            ->where(function ($query) {
                $query->whereColumn('b.booking_common_id', 'b.booking_id')
                ->orWhere('b.booking_common_id', '=', null);
            })
            ->where([['cn.customer_id', '=', $input['user_id']], ['cn.cleared_at', '=', null], ['cn.deleted_at', '=', null]])
            ->orderBy('cn.id', 'DESC')
            ->get();
        /******************************************************************************* */
        // templating starts
        foreach ($response['data'] as $key => $notification) {
            //$response['data'][$key]->read_status = $notification->read_status ? true : false;
            //$response['data'][$key]->time_elapsed = time_elapsed_string($notification->created_at);
            //unset($response['data'][$key]->created_at);
            /****************************************************************/
            $template_data_content = array(
                '{{customer_name}}' => $notification->customer_name,
                '{{company_name}}' => "DHK",
                '{{booking_ref_id}}' => "<b>".$notification->booking_ref_id."</b>",
            );
            /****************************************************************/
            $template_data_title = array(
                '{{customer_name}}' => $notification->customer_name,
                '{{company_name}}' => "DHK",
                '{{booking_ref_id}}' => $notification->booking_ref_id,
            );
            /*************** message templating *****************************/
            foreach ($template_data_content as $temp => $value) {
                $response['data'][$key]->message = str_replace($temp, $value, $response['data'][$key]->message);
            }
            /*************** title templating *******************************/
            foreach ($template_data_title as $temp => $value) {
                $response['data'][$key]->title = str_replace($temp, $value, $response['data'][$key]->title);
            }
            /****************************************************************/
            unset($response['data'][$key]->booking_ref_id);
            unset($response['data'][$key]->customer_name);
        }
        /******************************************************************************* */
        $response['status'] = "success";
        $response['message'] = "Notifications fetched successfully!";
        return Response::json($response, 200, [],JSON_PRETTY_PRINT);
		/*$data[0]['title'] = "Test title";
		$data[0]['message']   = "Please contact us with your request and we will endeavor to meet your requirements.";
		$data[0]['time']   = "2021-05-12 13:20:20";

		$data[1]['title'] = "Test title new";
		$data[1]['message']   = "Please contact us with your request and we will endeavor to meet your requirements.";
		$data[1]['time']   = "2021-05-12 13:20:20";
        
		return response()->json([
            'status' => "success",
            'data' => $data,
            'message' =>'All notifications listed successfully!'
            ], 200, array(), JSON_PRETTY_PRINT);
        */
	}
	
	public function appleProcessPay(Request $request)
	{
		$paymentToken = $request['paymentToken'];
        // $secret_key="pk_sbox_rmpjrc4nra5yliwneajdk7s4vyt";
        $secret_key="pk_xqqeetnolgaiplojquua3f6xdei";
		// $ServiceURL="https://api.sandbox.checkout.com/tokens";
		$ServiceURL="https://api.checkout.com/tokens";
		$Headers  = array("Authorization: ".$secret_key, "Content-Type: application/json", "Accept: application/json");
		$post_fields = '{"type":"applepay","token_data":'.$paymentToken.'}';
		
		//$dataa = array('country'=>'AE','currency'=>'AED','phone'=>$phone,'order_value'=>$ordervalue);
		$orderCreateResponse = $this->curlFun($ServiceURL, $Headers, $post_fields);
		$result=json_decode($orderCreateResponse,true);
		if($result != null) {
			$data=array();
			$data['source']= array("type" => "token","token" => $result['token']);
			$data['customer']= array("email" => $request['email'],"name" => $request['name']);
			$data['3ds']= array("enabled" => true);
			$data['amount']= (int)($request['amount'] * 100);
			$data['currency']= 'AED';
			// $data['processing_channel_id']= 'pc_7v5wzdvocmiuzea7yliehsvf4m';
			$data['processing_channel_id']= 'pc_w43m4vmnjruevpchn2nif3hssu';
			$data['reference']= $request['referenceId'];
			//$data['success_url']= Config::get('values.checkout_success');
            //$data['failure_url']= Config::get('values.checkout_fail');
            $urlVal = 'applepay-payment-failed';
			
			$post_datas=json_encode($data);
			// $secret_key_new="sk_sbox_ganchuqapm4vxup7b34t7b2ydeg";
			$secret_key_new="sk_6rnjwxr2r2nbtsvjb7oa2htv2uf";
			// $ServiceURL_new="https://api.sandbox.checkout.com/payments";
			$ServiceURL_new="https://api.checkout.com/payments";
			$Headers_new  = array("Authorization: Bearer ".$secret_key_new, "Content-Type: application/json", "Accept: application/json");
			$orderCreateResponse_new = $this->curlFun($ServiceURL_new, $Headers_new, $post_datas);
			$result_new=json_decode($orderCreateResponse_new,true);
			if($result_new['approved'] == true)
			{
				if($request['type'] == "package")
				{
					return response()->json(
						[
							'status' => 'success',
							'messages' => 'Payment done successfully.',
							'redirect_url' => Config::get('values.web_url').'applepay-package-success/'.$request['referenceId'].'/'.$result_new['id'].'?mobile=active',
						],
						200
					);
				} else {
					return response()->json(
						[
							'status' => 'success',
							'messages' => 'Payment done successfully.',
							'redirect_url' => Config::get('values.web_url').'applepay-payment-success/'.$request['referenceId'].'/'.$result_new['id'].'?mobile=active',
						],
						200
					);
				}
			} else {
				if($request['type'] == "package")
				{
					return response()->json(
						[
							'status' => 'failed',
							'messages' => 'Payment failed.',
							'redirect_url' => Config::get('values.web_url').'applepay-package-failed/'.$request['referenceId'].'?mobile=active',
						]
					);
				} else {
					return response()->json(
						[
							'status' => 'failed',
							'messages' => 'Payment failed.',
							'redirect_url' => Config::get('values.web_url').'applepay-payment-failed/'.$request['referenceId'].'?mobile=active',
						]
					);
				}
			}
		} else {
			if($request['type'] == "package")
			{
				return response()->json(
					[
						'status' => 'failed',
						'messages' => 'Payment failed.',
						'redirect_url' => Config::get('values.web_url').'applepay-package-failed/'.$request['referenceId'].'?mobile=active',
					]
				);
			} else {
				return response()->json(
					[
						'status' => 'failed',
						'messages' => 'Payment failed.',
						'redirect_url' => Config::get('values.web_url').'applepay-payment-failed/'.$request['referenceId'].'?mobile=active',
					]
				);
			}
		}
	}
	
	public function gpayProcessPay(Request $request)
	{
		$paymentToken = $request['paymentToken'];
        // $secret_key="pk_sbox_rmpjrc4nra5yliwneajdk7s4vyt";
        $secret_key="pk_xqqeetnolgaiplojquua3f6xdei";
		// $ServiceURL="https://api.sandbox.checkout.com/tokens";
		$ServiceURL="https://api.checkout.com/tokens";
		$Headers  = array("Authorization: ".$secret_key, "Content-Type: application/json", "Accept: application/json");
		$post_fields = '{"type":"googlepay","token_data":'.$paymentToken.'}';
		
		//$dataa = array('country'=>'AE','currency'=>'AED','phone'=>$phone,'order_value'=>$ordervalue);
		$orderCreateResponse = $this->curlFun($ServiceURL, $Headers, $post_fields);
		$result=json_decode($orderCreateResponse,true);
		if($result != null) {
			$data=array();
			$data['source']= array("type" => "token","token" => $result['token']);
			$data['customer']= array("email" => $request['email'],"name" => $request['name']);
			$data['3ds']= array("enabled" => true);
			$data['amount']= (int)($request['amount'] * 100);
			$data['currency']= 'AED';
			// $data['processing_channel_id']= 'pc_7v5wzdvocmiuzea7yliehsvf4m';
			$data['processing_channel_id']= 'pc_w43m4vmnjruevpchn2nif3hssu';
			$data['reference']= $request['referenceId'];
			if($request['type'] == "package")
			{
				$data['success_url']= Config::get('values.web_url').'googlepay-package-success?mobile=active';
				$data['failure_url']= Config::get('values.web_url').'googlepay-package-failed/'.$request['referenceId'].'?mobile=active';
			} else {
				$data['success_url']= Config::get('values.web_url').'payment-success?mobile=active';
				$data['failure_url']= Config::get('values.web_url').'googlepay-payment-failed/'.$request['referenceId'].'?mobile=active';
			}
			
			$post_datas=json_encode($data);
			// $secret_key_new="sk_sbox_ganchuqapm4vxup7b34t7b2ydeg";
			$secret_key_new="sk_6rnjwxr2r2nbtsvjb7oa2htv2uf";
			// $ServiceURL_new="https://api.sandbox.checkout.com/payments";
			$ServiceURL_new="https://api.checkout.com/payments";
			$Headers_new  = array("Authorization: Bearer ".$secret_key_new, "Content-Type: application/json", "Accept: application/json");
			$orderCreateResponse_new = $this->curlFun($ServiceURL_new, $Headers_new, $post_datas);
			$result_new=json_decode($orderCreateResponse_new,true);
			if(!isset($result_new['error_type'])) {
				$status=$result_new['status'];
				if ($status == 'Pending') { 
					$redirectUrl = $result_new['_links']['redirect']['href'];
					//return redirect($redirectUrl);
					return response()->json(
						[
							'status' => 'success',
							'messages' => 'Payment done successfully.',
							'redirect_url' =>$redirectUrl,
						],
						200
					);
				} else {
					if($request['type'] == "package")
					{
						return response()->json(
							[
								'status' => 'failed',
								'messages' => 'Payment failed.',
								'redirect_url' => Config::get('values.web_url').'googlepay-package-failed/'.$request['referenceId'].'?mobile=active',
							]
						);
					} else {
						return response()->json(
							[
								'status' => 'failed',
								'messages' => 'Payment failed.',
								'redirect_url' => Config::get('values.web_url').'googlepay-payment-failed/'.$request['referenceId'].'?mobile=active',
							]
						);
					}
				}
			} else{
				if($request['type'] == "package")
				{
					return response()->json(
						[
							'status' => 'failed',
							'messages' => 'Payment failed.',
							'redirect_url' => Config::get('values.web_url').'googlepay-package-failed/'.$request['referenceId'].'?mobile=active',
						]
					);
				} else {
					return response()->json(
						[
							'status' => 'failed',
							'messages' => 'Payment failed.',
							'redirect_url' => Config::get('values.web_url').'googlepay-payment-failed/'.$request['referenceId'].'?mobile=active',
						]
					);
				}
			}
		} else {
			if($request['type'] == "package")
			{
				return response()->json(
					[
						'status' => 'failed',
						'messages' => 'Payment failed.',
						'redirect_url' => Config::get('values.web_url').'googlepay-package-failed/'.$request['referenceId'].'?mobile=active',
					]
				);
			} else {
				return response()->json(
					[
						'status' => 'failed',
						'messages' => 'Payment failed.',
						'redirect_url' => Config::get('values.web_url').'googlepay-payment-failed/'.$request['referenceId'].'?mobile=active',
					]
				);
			}
		}
	}
	
	public static function curlFun($url,$Headers,$post_datas)
    {
        $ch = curl_init();
        $certificate_location = "/usr/local/openssl-0.9.8/certs/cacert.pem";
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, $certificate_location);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $certificate_location);
        
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$Headers);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_datas);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($ch);
        if($response === FALSE){
            die(curl_error($ch));
			// echo 'no';
			// echo 'Request Error:' . curl_error($ch);
        }
        curl_close($ch);
        return $response;
    }
    
}

