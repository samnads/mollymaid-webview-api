<?php

namespace App\Http\Controllers;

use App\Bookings;
use App\Customers;
use App\CustomerAddress;
use Carbon\Carbon;
use Config;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Mail;
use Response;
use App\Models\Settings;
use App\Models\BookingDeleteRemarks;
use stdClass;

class AdminApiMailController extends Controller
{
    public function registration_success_to_customer($customer_id)
    {
        /**
         * 
         * send registration confirmation mail to customer
         * 
         */
        try {
            //$settings = Settings::first();
            $customer = Customers::where('customer_id', '=', $customer_id)->first();
            if ($customer->email_address != '') {
                Mail::send(
                    'emails.registration-success-to-customer',
                    [
                        'settings' => @$settings,
                        'greeting' => 'Dear ' . $customer->customer_name,
                        'text_body' => 'Thank you for registration, you\'ve successfully registered on ' . Config::get('values.company_name') . '.',
                        'test' => isset($_GET['test'])
                    ],
                    function ($m) use ($customer) {
                        $m->from(Config::get('mail.mail_from_address'), Config::get('mail.mail_from_name'));
                        $m->to($customer->email_address, $customer->customer_name);
                        $m->bcc('samnad.s@azinova.info', 'Samnad S - Developer');
                        $m->subject('Registration Confirmation');
                    }
                );
            }
        } catch (\Exception $e) {
            throw new \ErrorException($e->getMessage());
        }
    }
    public function login_otp_to_customer($customer_id,$otp)
    {
        /**
         * 
         * send login otp mail to customer
         * 
         */
        try {
            //$settings = Settings::first();
            $customer = Customers::where('customer_id', '=', $customer_id)->first();
            if ($customer->email_address != '') {
                Mail::send(
                    'emails.login-otp-to-customer',
                    [
                        'settings' => @$settings,
                        'greeting' => 'Dear ' . $customer->customer_name,
                        'text_body' => 'Your OTP for login to ' . Config::get('values.company_name') . ' is '.$otp.'.',
                        'test' => isset($_GET['test'])
                    ],
                    function ($m) use ($customer) {
                        $m->from(Config::get('mail.mail_from_address'), Config::get('mail.mail_from_name'));
                        $m->to($customer->email_address, $customer->customer_name);
                        $m->bcc('samnad.s@azinova.info', 'Samnad S - Developer');
                        $m->subject(Config::get('values.company_name').' Login OTP');
                    }
                );
            }
        } catch (\Exception $e) {
            throw new \ErrorException($e->getMessage());
        }
    }
    public function password_recovery_to_customer($customer_id)
    {
        /**
         * 
         * send password recovery mail to customer
         * 
         */
        try {
            //$settings = Settings::first();
            $customer = Customers::where('customer_id', '=', $customer_id)->first();
            if ($customer->email_address != '') {
                Mail::send(
                    'emails.password-recovery-to-customer',
                    [
                        'settings' => @$settings,
                        'greeting' => 'Dear ' . $customer->customer_name,
                        'text_body' => 'We have received a request for password recovery, your current password is <b>' . $customer->customer_password . '</b>.',
                        'test' => isset($_GET['test'])
                    ],
                    function ($m) use ($customer) {
                        $m->from(Config::get('mail.mail_from_address'), Config::get('mail.mail_from_name'));
                        $m->to($customer->email_address, $customer->customer_name);
                        $m->bcc('samnad.s@azinova.info', 'Samnad S - Developer');
                        $m->subject(Config::get('values.company_name') . ' - Password Recovery');
                    }
                );
            }
        } catch (\Exception $e) {
            throw new \ErrorException($e->getMessage());
        }
    }
    public function booking_confirmation_to_customer($booking_id)
    {
        /**
         * 
         * send password recovery mail to customer
         * 
         */
        try {
            //$settings = Settings::first();
            $settings = null;
            $booking = Bookings::find($booking_id);
            $bookings = DB::table('bookings as b')
                ->select(
                    'b.*',
                    DB::raw('(CASE WHEN (b.reference_id IS NULL OR b.reference_id = "") THEN b.booking_id ELSE b.reference_id END) as reference_id'),
                    DB::raw('st.service_type_name as service_type_name'),
                    DB::raw('(CASE WHEN b.cleaning_material = "Y" THEN "Yes" ELSE "No" END) as cleaning_material_opted'),
                    'f.name as frequency',
                    'ca.customer_address as address',
                    DB::raw('(CASE WHEN b.payment_type_id IS NULL THEN b.pay_by ELSE pt.payment_mode END) as payment_method'),
                    //'pt.charge as payment_type_charge',
                    DB::raw('DAYNAME(b.service_start_date) as service_week_day_name'),
                    'op.transaction_id as op_transaction_id',
                    'op.amount as op_amount',
                    'op.transaction_charge as op_transaction_charge',
                )
                ->leftJoin('service_types as st', 'b.service_type_id', 'st.service_type_id')
                ->leftJoin('customers as c', 'b.customer_id', 'c.customer_id')
                ->leftJoin('frequencies as f', 'b.booking_type', 'f.code')
                ->leftJoin('payment_modes as pt', 'b.payment_type_id', 'pt.payment_mode_id')
                ->leftJoin('customer_addresses as ca', 'b.customer_address_id', 'ca.customer_address_id')
                ->leftJoin('online_payments as op', function ($join) {
                    $join->on('b.booking_id', '=', 'op.booking_id');
                    $join->whereColumn('op.reference_id', 'b.reference_id');
                    $join->where('op.payment_status', 'success');
                })
                ->where(function ($query) use ($booking_id) {
                    $query->where('b.booking_id', $booking_id);
                })
                ->orderBy('b.booking_id', 'ASC')
                ->get();
            //dd($bookings);
            $booking = $bookings[0];
            $customer = Customers::where('customer_id', '=', $booking->customer_id)->first();
            if ($customer->email_address != '') {
                Mail::send(
                    'emails.booking-confirmation-to-customer',
                    [
                        'settings' => @$settings,
                        'greeting' => 'Dear ' . @$customer->customer_name,
                        'text_body' => 'Your booking with Ref. No. <b>'. $booking->reference_id.'</b> is received, we\'ll contact you soon for confirmation',
                        'bookings' => $bookings,
                        'booking' => $booking,
                        'test' => isset($_GET['test'])
                    ],
                    function ($m) use ($customer, $booking) {
                        $m->from(Config::get('mail.mail_from_address'), Config::get('mail.mail_from_name'));
                        $m->to($customer->email_address, $customer->customer_name);
                        $m->bcc('samnad.s@azinova.info', 'Samnad S - Developer');
                        $m->subject(Config::get('values.company_name') . ' - Booking Confirmation '. $booking->reference_id);
                    }
                );
            }
        } catch (\Exception $e) {
            throw new \ErrorException($e->getMessage());
        }
    }
    public function schedule_cancel_to_customer($booking_id, $service_date)
    {
        /**
         * 
         * send schedule cancellation mail to customer
         * 
         */
        try {
            //$settings = Settings::first();
            $settings = null;
            $booking = Bookings::find($booking_id);
            $bookings = DB::table('bookings as b')
                ->select(
                    'b.*',
                    DB::raw('(CASE WHEN (b.reference_id IS NULL OR b.reference_id = "") THEN b.booking_id ELSE b.reference_id END) as reference_id'),
                    DB::raw('st.service_type_name as service_type_name'),
                    DB::raw('(CASE WHEN b.cleaning_material = "Y" THEN "Yes" ELSE "No" END) as cleaning_material_opted'),
                    'f.name as frequency',
                    'ca.customer_address as address',
                    DB::raw('(CASE WHEN b.payment_type_id IS NULL THEN b.pay_by ELSE pt.payment_mode END) as payment_method'),
                    //'pt.charge as payment_type_charge',
                    DB::raw('DAYNAME(b.service_start_date) as service_week_day_name'),
                    'op.transaction_id as op_transaction_id',
                    'op.amount as op_amount',
                    'op.transaction_charge as op_transaction_charge',
                )
                ->leftJoin('service_types as st', 'b.service_type_id', 'st.service_type_id')
                ->leftJoin('customers as c', 'b.customer_id', 'c.customer_id')
                ->leftJoin('frequencies as f', 'b.booking_type', 'f.code')
                ->leftJoin('payment_modes as pt', 'b.payment_type_id', 'pt.payment_mode_id')
                ->leftJoin('customer_addresses as ca', 'b.customer_address_id', 'ca.customer_address_id')
                ->leftJoin('online_payments as op', function ($join) {
                    $join->on('b.booking_id', '=', 'op.booking_id');
                    $join->whereColumn('op.reference_id', 'b.reference_id');
                    $join->where('op.payment_status', 'success');
                })
                ->where(function ($query) use ($booking_id) {
                    $query->where('b.booking_id', $booking_id);
                })
                ->orderBy('b.booking_id', 'ASC')
                ->get();
            //dd($bookings);
            $booking = $bookings[0];
            $customer = Customers::where('customer_id', '=', $booking->customer_id)->first();
            if ($customer->email_address != '') {
                Mail::send(
                    'emails.booking-cancel-to-customer',
                    [
                        'settings' => @$settings,
                        'greeting' => 'Dear ' . @$customer->customer_name,
                        'text_body' => 'Your booking with Reference No. <b>' . $booking->reference_id . '</b> <!--on service date <b>' . Carbon::createFromFormat('Y-m-d', @$service_date)->format('d M Y') . '</b>--> has been cancelled successfully.',
                        'bookings' => $bookings,
                        'booking' => $booking,
                        'test' => isset($_GET['test'])
                    ],
                    function ($m) use ($customer, $booking) {
                        $m->from(Config::get('mail.mail_from_address'), Config::get('mail.mail_from_name'));
                        $m->to($customer->email_address, $customer->customer_name);
                        $m->bcc('samnad.s@azinova.info', 'Samnad S - Developer');
                        $m->subject(Config::get('values.company_name') . ' - Booking Cancellation ' . $booking->reference_id);
                    }
                );
            }
        } catch (\Exception $e) {
            //throw new \ErrorException($e->getMessage());
        }
    }
    public function schedule_cancel_to_admin($booking_id, $service_date)
    {
        /**
         * 
         * send schedule cancellation mail to admin
         * 
         */
        try {
            //$settings = Settings::first();
            $settings = null;
            $booking = Bookings::find($booking_id);
            $bookings = DB::table('bookings as b')
                ->select(
                    'b.*',
                    DB::raw('(CASE WHEN (b.reference_id IS NULL OR b.reference_id = "") THEN b.booking_id ELSE b.reference_id END) as reference_id'),
                    DB::raw('st.service_type_name as service_type_name'),
                    DB::raw('(CASE WHEN b.cleaning_material = "Y" THEN "Yes" ELSE "No" END) as cleaning_material_opted'),
                    'f.name as frequency',
                    'ca.customer_address as address',
                    DB::raw('(CASE WHEN b.payment_type_id IS NULL THEN b.pay_by ELSE pt.payment_mode END) as payment_method'),
                    //'pt.charge as payment_type_charge',
                    DB::raw('DAYNAME(b.service_start_date) as service_week_day_name'),
                    'op.transaction_id as op_transaction_id',
                    'op.amount as op_amount',
                    'op.transaction_charge as op_transaction_charge',
                )
                ->leftJoin('service_types as st', 'b.service_type_id', 'st.service_type_id')
                ->leftJoin('customers as c', 'b.customer_id', 'c.customer_id')
                ->leftJoin('frequencies as f', 'b.booking_type', 'f.code')
                ->leftJoin('payment_modes as pt', 'b.payment_type_id', 'pt.payment_mode_id')
                ->leftJoin('customer_addresses as ca', 'b.customer_address_id', 'ca.customer_address_id')
                ->leftJoin('online_payments as op', function ($join) {
                    $join->on('b.booking_id', '=', 'op.booking_id');
                    $join->whereColumn('op.reference_id', 'b.reference_id');
                    $join->where('op.payment_status', 'success');
                })
                ->where(function ($query) use ($booking_id) {
                    $query->where('b.booking_id', $booking_id);
                })
                ->orderBy('b.booking_id', 'ASC')
                ->get();
            //dd($bookings);
            $booking = $bookings[0];
            $customer = Customers::where('customer_id', '=', $booking->customer_id)->first();
            if ($customer->email_address != '') {
                Mail::send(
                    'emails.booking-cancel-to-customer',
                    [
                        'settings' => @$settings,
                        'greeting' => 'Dear Admin',
                        'text_body' => 'Booking with Reference No. <b>' . $booking->reference_id . '</b> <!--on service date <b>' . Carbon::createFromFormat('Y-m-d', @$service_date)->format('d M Y') . '</b>--> has been cancelled by customer '.$customer->customer_name.'.',
                        'bookings' => $bookings,
                        'booking' => $booking,
                        'test' => isset($_GET['test'])
                    ],
                    function ($m) use ($customer, $booking) {
                        $m->from(Config::get('mail.mail_from_address'), Config::get('mail.mail_from_name'));
                        $m->to($customer->email_address, $customer->customer_name);
                        $m->bcc('samnad.s@azinova.info', 'Samnad S - Developer');
                        $m->subject(Config::get('values.company_name') . ' - Booking Cancellation ' . $booking->reference_id);
                    }
                );
            }
        } catch (\Exception $e) {
            //throw new \ErrorException($e->getMessage());
        }
    }
}
