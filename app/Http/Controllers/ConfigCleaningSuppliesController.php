<?php

namespace App\Http\Controllers;
use App\ConfigCleaningSupplies;
use Response;

use Illuminate\Http\Request;

class ConfigCleaningSuppliesController extends Controller
{
    public function get_cleaning_supplies_by_type($type)
    {
        $data['cleaning_supplies'] = ConfigCleaningSupplies::where('type', "Custom")->get();
        return Response::json($data, 200, ['Content-type' => 'application/json; charset=utf-8'], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
    }
}
