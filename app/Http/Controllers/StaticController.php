<?php
namespace App\Http\Controllers;

use DB;

class StaticController extends Controller
{
    /**
     * Author: Samnad S
     * Date: 26/09/2023
     * Function to faqs
     */
    public function faqs()
    {
        $faqs = DB::table('faqs as f')->select('f.question', 'f.answer','f.sort_order_id')->where('f.deleted_at', null)->orderBy('f.sort_order_id', 'ASC')->get();
        return response()->json([
            'data' => $faqs,
            'status' => "success",
            'message' => 'All FAQ listed successfully!',
        ], 200, array(), JSON_PRETTY_PRINT);
    }
}
