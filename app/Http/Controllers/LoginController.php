<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customers;
use App\CustomerAddress;
use App\Bookings;
use App\Mail\SendOTPMail;
use App\ServiceType;
use Illuminate\Support\Facades\Mail;
use App\Area;
use Exception;
use DB;
use DateTime;
use App\OfferBanners;
use App\Mail\ForgotPassword;
use File;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Config;

class LoginController extends Controller
{
    /**
     * Updated By : Samnad S
     * Last Updated At : 25/01/2024
     * Api for signup
     */
    public function signUp(Request $request)
    {
        try {
            $email = $request['email'];
            $country_code = $request['countryCode'];
            $phoneNumber = $country_code . $request['phoneNumber'];
            $phone_num_stripped = substr($phoneNumber, -9);
            /***************************************************** */
            // check email exist and verified
            $customer = Customers::where('email_address', $email)->where('customer_status', 1)->first();
            if ($customer) {
                return response()->json([
                    'status' => 'failed',
                    'data' => null,
                    'messages' => 'Email already exist!',
                ]);
            }
            /***************************************************** */
            // check mobile exist and verified
            $customer = Customers::where("mobile_number_1", "like", "%" . $phone_num_stripped)->where('mobile_status', '=', 1)->where('customer_status', 1)->first();
            if ($customer) {
                return response()->json([
                    'status' => 'failed',
                    'data' => null,
                    'messages' => 'Phone number already exist!',
                ]);
            }
            /***************************************************** */
            /*$customer = Customers::where('email_address', $email)
                ->where("mobile_number_1", "like", "%" . $phone_num_stripped)
                ->where('customer_status', 1)
                ->where(function ($query) {
                    $query->where('mobile_status', '=', 0);
                    $query->orWhere('email_verified_at', '=', null);
                })
                ->first();*/
            $customer = Customers::where("mobile_number_1", "like", "%" . $phone_num_stripped)
                ->where('mobile_status', '=', 0)
                ->first();
            /***************************************************** */
            if (!$customer) {
                // new registration
                $customer = new Customers();
                $customer->customer_name = ucfirst(ucwords($request['fullName']));
                $customer->customer_nick_name = $request['fullName'];
                $customer->default_address_id = null;
                $customer->default_booking_address = null;
                $customer->location_id = null;
                $customer->customer_type_id = 1;
                $customer->customer_company_id = null;
                $customer->customer_dealer_id = null;
                $customer->refer_source_id = null;
                $customer->refer_source_note = null;
                $customer->payment_mode_id = 1;
                $customer->payment_frequency_id = 1;
                $customer->gender_id = null;
                $customer->odoo_customer_id = null;
                $customer->customer_username = null;
                $customer->customer_password = trim($request['password']);
                $customer->booking_note = null;
                $customer->driver_note = null;
                $customer->key_given = 'N';
                $customer->customer_added_datetime = date('Y-m-d H:i:s');
                $customer->customer_last_modified_datetime = date('Y-m-d H:i:s');
                $customer->created_at = date('Y-m-d H:i:s');
                $customer->updated_at = date('Y-m-d H:i:s');
                $customer->contact_person = ucfirst(ucwords($request['fullName']));
                $customer->mobile_number_1 = $phoneNumber;
                $customer->email_address = $email;
                $customer->customer_type = 'HO';
                $customer->website_url = null;
                $customer->balance = 0;
                $customer->signed = null;
                $customer->customer_notes = null;
                $customer->customer_source_id = null;
                $customer->odoo_customer_newid = null;
                $customer->save();
                $customer->customer_code = 'MM-' . sprintf("%05d", $customer->customer_id) . '-' . date('m-Y');
            }
            $otp = "1234";
            $customer->mobile_verification_code = $otp;
            $customer->oauth_token = bin2hex(random_bytes(20));
            $customer->save();
            /***************************************************** */
            //$msg = 'Hi ' . ucfirst(ucwords($request['fullName'])) . ', Your OTP for verification is ' . $customer->mobile_verification_code;
            //LoginController::send_sms_from_external_server($phoneNumber, $msg);
            //Mail::send(new SendOTPMail($customer->email_address, $customer->mobile_verification_code, $customer->customer_name));
            $mail = new AdminApiMailController;
            $mail->login_otp_to_customer($customer->customer_id, $customer->mobile_verification_code);
            /***************************************************** */
            return response()->json(
                [
                    'status' => 'success',
                    'messages' => 'Customer registerd successfully.',
                    'data' => [
                        'id' => $customer->customer_id,
                        'name' => $customer->customer_name,
                        'emailId' => $customer->email_address,
                        'phone' => $customer->mobile_number_1,
                        'token' => $customer->oauth_token,
                        'mobile' => $customer->mobile_number_1,
                        'mobile_verified' => $customer->mobile_status,
                    ],
                ],
                200
            );
            /***************************************************** */
        } catch (Exception $e) {
            return response()->json(
                [
                    'status' => 'failed',
                    'messages' => $e->getMessage(),
                    'data' => null,
                ],
                200
            );
        }
    }

    function send_sms_from_external_server($mobile_number, $message)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://www.azinovatechnologies.com/sms/dhk-sms-api.php?mobile_number=' . $mobile_number . '&message=' . urlencode($message));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $res = curl_exec($ch);
        curl_close($ch);
    }

    public static function send_sms($mobile_number, $message)
    {
        $num = substr($mobile_number, -9);

        //$sms_url = 'https://api.smsglobal.com/http-api.php?action=sendsms&user=jhh1a51c&password=LXoAEzMF&&from=TestJC&to=971' . urlencode($num) . '&text=' . urlencode($message) . '';
        // $sms_url = 'http://sms.azinova.in/WebServiceSMS.aspx?User=crystalblu&passwd=crystal@123&mobilenumber=971'.$num.'&message='.urlencode($message).'&sid=BULKSMS&mtype=N';

        $sms_url = 'http://sms.azinova.in/WebServiceSMS.aspx?User=elitemaids&passwd=emaid@123&mobilenumber=971' . $num . '&message=' . urlencode($message) . '&sid=EliteMaids&mtype=N';

        $sms_url = str_replace(" ", '%20', $sms_url);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $sms_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $res = curl_exec($ch);
        curl_close($ch);
    }
    public static function test_sms()
    {
        $mobile_number = '582863990';
        $message = 'SMS TEST AZINOVA';
        $num = substr($mobile_number, -9);

        //$sms_url = 'https://api.smsglobal.com/http-api.php?action=sendsms&user=jhh1a51c&password=LXoAEzMF&&from=TestJC&to=971' . urlencode($num) . '&text=' . urlencode($message) . '';
        // $sms_url = 'http://sms.azinova.in/WebServiceSMS.aspx?User=crystalblu&passwd=crystal@123&mobilenumber=971'.$num.'&message='.urlencode($message).'&sid=BULKSMS&mtype=N';

        $sms_url = 'http://sms.azinova.in/WebServiceSMS.aspx?User=elitemaids&passwd=emaid@123&mobilenumber=971' . $num . '&message=' . urlencode($message) . '&sid=EliteMaids&mtype=N';

        $sms_url = str_replace(" ", '%20', $sms_url);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $sms_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $res = curl_exec($ch);
        curl_close($ch);
        echo "<pre>";
        print_r($res);
        die;
    }


    /**
     * Author:Karthika
     * Date:19/08/2020
     * API for save customer address details
     */
    public function saveAddress(Request $request)
    {
        DB::beginTransaction();
        try {
            // $customerBooking = Bookings::find($request['bookingId']);
            $customerAddress = new CustomerAddress();
            $customerAddress->customer_id = $request['customerId'];
            $customerAddress->area_id = $request['area'];
            $customerAddress->customer_address = ucfirst(ucwords($request['address']));
            $customerAddress->latitude = $request['latitude'] ?: '0';
            $customerAddress->longitude = $request['longitude'] ?: '0';
            $customerAddress->address_category = $request['address_type'];
            $customerAddress->save();
            $default_address = CustomerAddress::where('customer_id', $request['customerId'])->where('address_status', 0)->where('default_address', 1)->first();
            if (!$default_address) {
                $customerAddress->default_address = (int) 1;
                $customerAddress->save();
            }
            DB::commit();
            $addressArray = [];
            $area = Area::find($customerAddress->area_id);
            $areaName = $area->area_name;
            // dd($areaName);
            $addressArray = ['addressName' => $customerAddress['customer_address'], 'Area' => $areaName];
            return response()->json(
                [
                    'status' => 'success',
                    'messages' => 'Customer address saved successfully.',
                    'data' => [
                        'id' => $request['customerId'],
                        'address' => $addressArray
                    ]
                ],
                200
            );
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                'status' => "Failed",
                'data' => [],
                'messages' => $e->getMessage()
            ]);
        }
    }

    /**
     * Author:Karthika
     * Date:19/08/2020
     * API for sign in
     */
    public function signIn(Request $request)
    {
        // $validData = Validator::make($request->all(), [
        //     'email' => 'required|email',
        //     'password' => 'required',
        //     'bookingId' => 'required'
        // ]);
        // if ($validData->fails()) {
        //     return response()->json(
        //         [
        //             'status' => "error",
        //             'data' => null,
        //             'messages' => $validData->errors()
        //         ],
        //         400
        //     );
        // }


        // return $request['emailId'];
        // dd($request['emailId']);
        // $customer = Customers::where('email_address', $request['emailId'])->
        // where('customer_status', 1)->where('mobile_status',1)->first();



        $customer = Customers::where('email_address', $request['emailId'])->where('customer_status', 1)->first();
        if ($customer) {
            if ($customer->customer_password == '' || (trim($request['password']) != $customer->customer_password)) {
                return response()->json([
                    'status' => 'failed',
                    'data' => null,
                    'messages' => 'Invalid login.'
                ]);
            }
            if ($customer->mobile_status == 0) {
                //$rand_no = rand(1000, 9999);
                $rand_no = '1234';
                $msg = 'Hi ' . ucfirst(ucwords($customer->customer_nick_name)) . ', Your OTP for verification is ' . $rand_no;
                LoginController::send_sms_from_external_server($customer->mobile_number_1, $msg);
                $customer->mobile_verification_code = $rand_no;
                $customer->save();
                try {
                    //Mail::send(new SendOTPMail($customer->email_address,$rand_no,$customer->customer_name));
                    $mail = new AdminApiMailController;
                    $mail->login_otp_to_customer($customer->customer_id, $customer->mobile_verification_code);
                } catch (Exception $e) {
                    return response()->json([
                        'status' => 'failed',
                        'data' => [
                            'id' => $customer->customer_id,
                            'mobile_verified' => $customer->mobile_status,
                            'mobile' => $customer->mobile_number_1,
                        ],
                        'messages' => 'Verify Your mobile..'
                    ]);
                }
                return response()->json([
                    'status' => 'failed',
                    'data' => [
                        'id' => $customer->customer_id,
                        'mobile_verified' => $customer->mobile_status,
                        'mobile' => $customer->mobile_number_1,
                    ],
                    'messages' => 'Verify Your mobile..'
                ]);
            }
            $addressArray = [];
            $customerAddress = CustomerAddress::where('customer_id', $customer->customer_id)->where('address_status', 0)->where('default_address', 1)->first();
            if ($customerAddress != '') {
                $area = Area::find($customerAddress->area_id);
                $areaName = $area->area_name;
                // dd($areaName);
                $addressArray = ['addressName' => $customerAddress['customer_address'], 'Area' => $areaName];
            }
            $customer->oauth_token = bin2hex(random_bytes(20));
            $customer->save();
            return response()->json(
                [
                    'status' => 'success',
                    'messages' => 'Customer signed in successfully.',
                    'data' => [
                        'id' => $customer->customer_id,
                        'address' => $addressArray,
                        'name' => $customer->customer_name,
                        'emailId' => $customer->email_address,
                        'phone' => $customer->mobile_number_1,
                        'token' => $customer->oauth_token,
                        'customer_photo_url' => $customer->customer_photo_file ? Config::get('url.customer_image_prefix') . $customer->customer_photo_file : Config::get('url.customer_image_prefix') . 'default.png',
                        'deleteHide' => 'no',
                    ]
                ],
                200
            );
        } else {
            return response()->json([
                'status' => "failed",
                'data' => new \stdClass,
                'messages' => 'Email Id not found'
            ]);
        }
    }

    public function getUserData(Request $request)
    {
        $customer = Customers::where('customer_id', $request['customerId'])->
            where('customer_status', 1)->first();
        if ($customer) {
            return response()->json(
                [
                    'status' => 'success',
                    'messages' => 'Customer signed in successfully.',
                    'data' => [
                        'name' => $customer->customer_name,
                        'emailId' => $customer->email_address,
                        'phone' => $customer->mobile_number_1,
                        'customerId' => $customer->customer_id
                    ]
                ],
                200
            );
        } else {
            return response()->json([
                'status' => "failed",
                'data' => [],
                'messages' => 'Customer Id not found'
            ]);
        }
    }

    public function getCustomerOdooData(Request $request)
    {
        $customer = Customers::where('odoo_package_customer_id', $request['customerId'])->
            where('customer_status', 1)->first();
        if ($customer) {
            return response()->json(
                [
                    'status' => 'success',
                    'messages' => 'Customer signed in successfully.',
                    'data' => [
                        'name' => $customer->customer_name,
                        'emailId' => $customer->email_address,
                        'phone' => $customer->mobile_number_1,
                        'customerId' => $customer->customer_id
                    ]
                ],
                200
            );
        } else {
            return response()->json([
                'status' => "failed",
                'data' => [],
                'messages' => 'Customer Id not found'
            ]);
        }
    }

    /**
     * 
     * 
     * Author:Karthika
     * Date:20/08/2020
     * API for forgot password
     * 
     * 
     */
    public function forgotPassword(Request $request)
    {
        try {
            $customer = Customers::where('email_address', $request->email)->where('customer_status', 1)->first();
            if ($customer) {
                $password = $customer->customer_password;
                $name = $customer->customer_name;
                //Mail::send(new ForgotPassword($email,$password,$name));
                $mail = new AdminApiMailController;
                $mail->password_recovery_to_customer($customer->customer_id);
                return response()->json(
                    [
                        'status' => 'success',
                        'data' => ['password' => $password, 'name' => $name],
                        'messages' => 'Password recovery mail send successfully.'
                    ],
                    200
                );
            } else {
                return response()->json(
                    [
                        'status' => 'Failed',
                        'messages' => 'Customer not exist with this email address.'
                    ],
                    200
                );
            }
        } catch (Exception $e) {
            return response()->json([
                'status' => "Failed",
                'data' => [],
                'messages' => array('Failed' => array($e->getmessage()))
            ]);
        }
    }
    /**
     * Function to check api for otp validation
     * Author:Karthika
     * Date:24/08/2020
     */
    public function checkOTP(Request $request)
    {
        // $validData = Validator::make($request->all(), [
        //     'otp' => 'required|min:6|max:6|number',
        //     'customer_id' => 'required'
        // ]);
        // if ($validData->fails()) {
        //     return response()->json(
        //         [
        //             'status' => "error",
        //             'data' => null,
        //             'messages' => $validData->errors()
        //         ],
        //         400
        //     );
        // } 
        try {
            $customer = Customers::find($request['customerId']);
            if ($customer) {
                //$customer->mobile_verification_code=1234;
                $first = $request['otp1'];
                $second = $request['otp2'];
                $third = $request['otp3'];
                $fourth = $request['otp4'];
                $combinedOTP = $first . $second . $third . $fourth;
                if ($combinedOTP == $customer->mobile_verification_code) {
                    $customer->mobile_status = 1; //mobile number verification status
                    $customer->email_verified_at = date('Y-m-d H:i:s'); // indicates email is verified
                    $customer->save();
                    $mail = new AdminApiMailController;
                    $mail->registration_success_to_customer($customer->customer_id);
                    return response()->json(
                        [
                            'status' => "success",
                            // 'data' => $request['customerId'],
                            'data' => [
                                'id' => $customer->customer_id,
                                'name' => $customer->customer_name,
                                'emailId' => $customer->email_address,
                                'phone' => $customer->mobile_number_1,
                                'token' => $customer->oauth_token,
                                'password' => $customer->customer_password,
                            ],
                            'messages' => 'OTP Verified successfully.'
                        ]
                    );
                } else {
                    return response()->json(
                        [
                            'status' => "Failed",
                            'data' => null,
                            'messages' => 'Invalid OTP'
                        ]
                    );
                }
            } else {
                return response()->json(
                    [
                        'status' => "Failed",
                        'data' => null,
                        'messages' => 'Customer not found'
                    ]
                );
            }
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                'status' => "Failed",
                'data' => [],
                'messages' => array('Failed' => array($e->getMessage()))
            ]);
        }
    }

    /**
     * API to resend otp
     * Author:Karthika
     * Date:24/08/2020
     */
    public function resendOtp(Request $request)
    {
        // $validData = Validator::make($request->all(), [
        //     'customer_id' => 'required'
        // ]);
        // if ($validData->fails()) {
        //     return response()->json(
        //         [
        //             'status' => "error",
        //             'data' => null,
        //             'messages' => $validData->errors()
        //         ],
        //         400
        //     );
        // } 
        try {
            $customer = Customers::find($request['customerId']);
            if ($customer) {
                $rand_no = rand(1000, 9999); //send sms to the curresponding number
                //$rand_no = '1234'; //send sms to the curresponding number
                $customer->mobile_verification_code = $rand_no; //mobile number verification status


                $customer->save();
                $msg = 'Hi ' . ucfirst(ucwords($customer->customer_name)) . ', Your OTP for verification is ' . $customer->mobile_verification_code;
                $this->send_sms_from_external_server($customer->mobile_number_1, $msg);
                try {
                    Mail::send(new SendOTPMail($customer->email_address, $rand_no, $customer->customer_name));
                } catch (Exception $e) {
                    return response()->json(
                        [
                            'status' => "success",
                            'data' => null,
                            'messages' => 'OTP resend successfully.'
                        ]
                    );
                }
                return response()->json(
                    [
                        'status' => "success",
                        'data' => null,
                        'messages' => 'OTP resend successfully.'
                    ]
                );
            } else {
                return response()->json(
                    [
                        'status' => "Failed",
                        'data' => null,
                        'messages' => 'Customer not found'
                    ]
                );
            }
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                'status' => "Failed",
                'data' => [],
                'messages' => array('Failed' => array('Exception error occured'))
            ]);
        }
    }
    /**
     * function to get address details of user
     * Author:Karthika
     * Date:03/09/2020
     */
    public function getAddress(Request $request)
    {
        $customerBooking = Bookings::find($request['bookingId']);
        $area = Area::select('area_id', 'area_name')->where('area_name', '!=', '')->where('web_status', 1)->get();
        $customerAddressId = $customerBooking->customer_address_id;
        $customerAddress = CustomerAddress::find($customerAddressId);
        return response()->json(
            [
                'status' => 'success',
                'messages' => 'Customer address fetched successfully.',
                'data' => $customerAddress,
                'area' => $area
            ],
            200
        );
    }
    public function updateCustomerAvatar(Request $request)
    {
        $customer = Customers::where('customer_id', $request['customerId'])->where('customer_status', 1)->first();
        if ($customer) {
            // image upload function here
            $image = $request['customer_image_base64'];
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = "customer-avatar-" . $request['customerId'] . "-" . time() . ".png";
            //dd(base64_decode($image));
            $path = Config::get('url.customer_image_upload_path') . $imageName;
            //dd(File::put($path, base64_decode($image)));
            File::put($path, base64_decode($image));
            // resize
            $image = Image::make($path)->resize(250, 250);
            $image->save($path);
            /******************* */
            $customer->customer_photo_file = File::exists($path) ? $imageName : null;
            $customer->save();
            $data['customer_photo_url'] = $customer->customer_photo_file ? Config::get('url.customer_image_prefix') . $customer->customer_photo_file : Config::get('url.customer_image_prefix') . 'default.png';
            return response()->json(
                [
                    'status' => 'success',
                    'messages' => 'Avatar updated successfully.',
                    'data' => $data,
                ],
                200
            );
        } else {
            return response()->json([
                'status' => "failed",
                'data' => [],
                'messages' => 'Customer not found',
            ]);
        }

    }
    public function editUserData(Request $request)
    {
        $customerEmailExists = Customers::where('customer_id', '!=', $request['customerId'])->
            where('customer_status', 1)->where('email_address', $request['email'])->first();

        $customerMobileExists = Customers::where('customer_id', '!=', $request['customerId'])->
            where('customer_status', 1)->where('mobile_number_1', $request['phone'])->first();
        if (!$customerEmailExists && !$customerMobileExists) {
            $customer = Customers::where('customer_id', $request['customerId'])->
                where('customer_status', 1)->first();
            if ($customer) {
                $customer->customer_name = ucfirst(ucwords($request['fullName']));
                $customer->mobile_number_1 = $request['phone'];
                $customer->email_address = $request['email'];
                $customer->save();
                return response()->json(
                    [
                        'status' => 'success',
                        'messages' => 'Details updated successfully.',
                        'data' => []
                    ],
                    200
                );
            } else {
                return response()->json([
                    'status' => "failed",
                    'data' => [],
                    'messages' => 'Customer Id not found'
                ]);
            }
        } else {
            return response()->json([
                'status' => "failed",
                'data' => [],
                'messages' => 'Mobile number or Emaild already exists.'
            ]);
        }
    }
    public function getUserAddress(Request $request)
    {
        $customerAddress = CustomerAddress::where('customer_id', $request['customerId'])->where('address_status', 0)->get();
        $addressArray = [];
        if ($customerAddress != '') {
            foreach ($customerAddress as $address) {
                $area = Area::find($address->area_id);
                $areaName = $area->area_name;
                $addressArray[] = [
                    'id' => $address['customer_address_id'],
                    'addressName' => $address['customer_address'],
                    'Area' => $areaName,
                    'AddressType' => $address->address_category,
                    'default' => $address->default_address
                ];
            }
        }
        return response()->json(
            [
                'status' => 'success',
                'messages' => 'Customer address fetched successfully.',
                'data' => $addressArray,
            ],
            200
        );
    }
    public function editAddressData(Request $request)
    {
        try {
            $customerAddress = CustomerAddress::where('customer_address_id', $request['addressId'])->first();
            if ($customerAddress) {
                $area = Area::select('area_id', 'area_name')->where('area_status', 1)->where('web_status', 1)->orderBy('area_name', 'ASC')->get();
                return response()->json(
                    [
                        'status' => 'success',
                        'messages' => 'Customer address fetched successfully.',
                        'data' => ['customerAddress' => $customerAddress, 'area' => $area, 'customerId' => $customerAddress['customer_id']],
                    ],
                    200
                );
            } else {
                return response()->json(
                    [
                        'status' => 'failed',
                        'messages' => 'Customer address not found.',
                        'data' => [],
                    ]
                );
            }
        } catch (Exception $e) {
            return response()->json([
                'status' => "Failed",
                'data' => [],
                'messages' => 'Exception error occured'
            ], 500);
        }
    }
    public function addAddressData(Request $request)
    {
        $area = Area::select('area_id', 'area_name')->where('area_status', 1)->where('web_status', 1)->orderBy('area_name', 'ASC')->get();
        return response()->json(
            [
                'status' => 'success',
                'messages' => 'Customer address fetched successfully.',
                'data' => ['area' => $area, 'customerId' => $request['customerId']],
            ],
            200
        );
    }
    public function saveAddressData(Request $request)
    {
        // DB::beginTransaction();
        // try {
        if ($request['address_id'] != '') {
            $customerAddress = CustomerAddress::where('customer_address_id', $request['address_id'])->first();
            $messages = 'Address details edited successfully.';
        } else {
            $customerAddress = new CustomerAddress();
            $messages = 'Address details saved successfully.';
            $customerAddress->default_address = (int) 0;
        }
        $customerAddress->customer_id = $request['customerId'];
        $customerAddress->area_id = $request['area'];
        $customerAddress->customer_address = ucfirst(ucwords($request['address']));
        $customerAddress->latitude = $request['latitude'];
        $customerAddress->longitude = $request['longitude'];
        $customerAddress->address_category = $request['address_type'];
        $customerAddress->save();
        $default_address = CustomerAddress::where('customer_id', $request['customerId'])->where('address_status', 0)->where('default_address', 1)->first();
        if (!$default_address) {
            $customerAddress->default_address = (int) 1;
            $customerAddress->save();
        }
        // DB::commit();
        return response()->json(
            [
                'status' => 'success',
                'messages' => $messages,
            ],
            200
        );
        // }
        //  catch (Exception $e) {
        //     dd($e);
        //     DB::rollBack();
        //     return response()->json([
        //         'status' => "Failed",
        //         'data' => [],
        //         'messages' => 'Exception error occured'
        //     ]);
        // } 
    }
    public function deleteAddress(Request $request)
    {
        $customerAddress = CustomerAddress::where('customer_address_id', $request['addressId'])->first();
        $customerAddress->address_status = (int) 1;
        $customerAddress->save();
        return response()->json(
            [
                'status' => 'success',
                'messages' => 'Address deleted successfully.',
            ],
            200
        );
    }
    public function setDefault(Request $request)
    {
        $customerAddress = CustomerAddress::where('customer_address_id', $request['addressId'])->first();
        $customerAddress->default_address = (int) 1;
        $customerAddress->save();
        $anotherAddress = CustomerAddress::where('address_status', 0)->where('customer_id', $customerAddress->customer_id)->where('customer_address_id', '!=', $customerAddress->customer_address_id)->get();
        if ($anotherAddress != '') {
            foreach ($anotherAddress as $address) {
                $address->default_address = (int) 0;
                $address->save();
            }
        }
        return response()->json(
            [
                'status' => 'success',
                'messages' => 'Address deleted successfully.',
                'data' => $customerAddress->customer_id,
            ],
            200
        );
    }
    public function updatePassword(Request $request)
    {
        $customer = Customers::where('customer_id', $request['customerId'])->first();
        if ($customer && $customer->customer_password == $request['old_password']) {
            $customer->customer_password = $request['new_password'];
            $customer->save();
            return response()->json(
                [
                    'status' => 'success',
                    'messages' => 'Password changed successfully.',
                    'data' => [],
                ],
                200
            );
        } else {
            return response()->json(
                [
                    'status' => 'failed',
                    'messages' => 'Incorrect old password.',
                    'data' => [],
                ]
            );
        }
    }
    function token_auth(Request $request)
    {
        if ($request->has('user_id')) {
            $customer = Customers::Find($request->user_id);
            if (!$customer)
                return ['status' => 'error', 'message' => 'User Not Found'];
            if ($request->has('token') && $customer->oauth_token !== $request->token)
                return ['status' => 'error', 'message' => 'Invalid token'];
            else
                return ['status' => 'success', 'token' => $customer->oauth_token, 'message' => 'User ' . $customer->customer_name . ' verified Successfully'];
        } else
            return ['status' => 'error', 'message' => 'Unsatisfied Parameters'];

    }
    function user_id(Request $request)
    {
        if (isset($request->id)) {
            $customer = Customers::where('customer_id', $request->id)->first();
            if ($customer)
                return response()->json(['status' => 'success', 'data' => true, 'customer' => $customer, 'message' => 'User Verified Successfully']);
            else
                return response()->json(['status' => 'error', 'data' => false, 'message' => 'Invalid User']);
        } else
            return response()->json(['status' => 'error', 'data' => false, 'message' => 'Unsatisfied Parameters']);
    }
    function getOffers()
    {
        $res = DB::table('offers_sb')
            ->where('offer_status', 0)
            ->orderBy('id_offers_sb', 'DESC');
        return response()->json([
            'status' => 'success',
            'data' => $res ? $res->get() : [],
            'url' => 'https://booking.emaid.info:3444/dhk-admin-demo/offer_img/uploads/'
        ], 200, [], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        //return ['status'=>'success','data'=>$res?$res->get():[],'url'=> 'https://booking.emaid.info:3444/dhk-admin-demo/offer_img/uploads/'];
    }
    public function getOfferBanners(Request $request)
    {
        $offers = OfferBanners::select('id', 'banner_image')->where('status', 1)->get();
        $bannerOffers = [];
        foreach ($offers as $offerval) {
            $bannerOffers[] = ['id' => $offerval['id'], 'banner_image' => $offerval['banner_image']];
        }
        $data = ['offers' => $bannerOffers];
        return response()->json([
            'status' => "success",
            'data' => $data,
            'appUrl' => env('APP_URL') . '/uploads/offers/',
            'message' => 'All offers listed successfully!'
        ], 200);
    }
    function getuserdetails(Request $request)
    {
        // return $request->all();
        if (!isset($request->user_id) && !isset($request->token))
            return ['status' => 'error', 'message' => 'Unsatisfied Parameters'];
        if (isset($request->token)) {
            $customer = Customers::where('oauth_token', $request->token)->first();
            if (!$customer)
                return ['status' => 'error', 'message' => 'Invalid Token'];
        } elseif (isset($request->user_id)) {
            $customer = Customers::Find($request->user_id);
            if (!$customer)
                return ['status' => 'error', 'message' => 'User Not Found'];
        }
        $addressArray = [];
        $customerAddress = CustomerAddress::where('customer_id', $customer->customer_id)->where('default_address', 1)->where('address_status', 0)->first();
        if ($customerAddress != '') {
            $area = Area::find($customerAddress->area_id);
            $areaName = $area->area_name;
            // dd($areaName);
            $addressArray = ['addressName' => $customerAddress['customer_address'], 'Area' => $areaName];
        }
        return [
            'status' => 'success',
            'data' => [
                'id' => $customer->customer_id,
                'address' => $addressArray,
                'name' => $customer->customer_name,
                'emailId' => $customer->email_address,
                'phone' => $customer->mobile_number_1,
                'otp' => $customer->mobile_verification_code,
                'token' => $customer->oauth_token,
            ]
        ];
    }
    function forgotpasswordmobile(Request $request)
    {
        if (!isset($request->emilorphone))
            return ['status' => 'error', 'message' => 'Unsatisfied Parametrers'];
        $customer = Customers::where('mobile_number_1', $request->emilorphone)
            ->orWhere('email_address', $request->emilorphone)->first();
        if (!$customer)
            return ['status' => 'error', 'message' => 'User not Found'];
        else {
            $customer = Customers::Find($customer->customer_id);
            $customer->mobile_verification_code = rand(1000, 9999);
            //$customer->mobile_verification_code='1234';
            $customer->oauth_token = bin2hex(random_bytes(16));
            $customer->oauth_validty = date('y-m-d');
            $customer->save();
            $msg = 'Hi ' . ucfirst(ucwords($customer->customer_name)) . ', Your OTP for verification is ' . $customer->mobile_verification_code;
            $this->send_sms_from_external_server($customer->mobile_number_1, $msg);
            try {
                Mail::send(new SendOTPMail($customer->email_address, $customer->mobile_verification_code, $customer->customer_name));
            } catch (Exception $e) {
                $data = [
                    'id' => $customer->customer_id,
                    'name' => $customer->customer_name,
                    'emailId' => $customer->email_address,
                    'phone' => $customer->mobile_number_1,
                    'otp' => $customer->mobile_verification_code,
                    'token' => $customer->oauth_token,
                ];
                return ['status' => 'success', 'data' => $data, 'message' => 'OTP Sent Successfully'];

            }
            $data = [
                'id' => $customer->customer_id,
                'name' => $customer->customer_name,
                'emailId' => $customer->email_address,
                'phone' => $customer->mobile_number_1,
                'otp' => $customer->mobile_verification_code,
                'token' => $customer->oauth_token,
            ];
            return ['status' => 'success', 'data' => $data, 'message' => 'OTP Sent Successfully'];
        }
    }
    function resetpassword(Request $request)
    {
        if (!(isset($request->user_id) && isset($request->password)))
            return ['status' => 'error', 'message' => 'Unsatisfied Parametrers'];
        if (isset($request->token)) {
            $customer = Customers::where('oauth_token', $request->token)->first();
            if (!$customer)
                return ['status' => 'error', 'message' => 'Invalid Token'];
        }
        $customer = Customers::Find($request->user_id);
        if (!$customer)
            return ['status' => 'error', 'message' => 'User not Found'];
        else {
            if ($customer->customer_password === $request->password)
                return ['status' => 'error', 'message' => 'New password is same as Old password'];
            $customer->customer_password = $request->password;
            $customer->save();
            $data = [
                'id' => $customer->customer_id,
                'name' => $customer->customer_name,
                'emailId' => $customer->email_address,
                'phone' => $customer->mobile_number_1,
                'token' => $customer->oauth_token,
            ];
            return ['status' => 'success', 'data' => $data, 'message' => 'Password Reset Successfully'];
        }
    }

    public function deleteAccount(Request $request)
    {
        $customer = Customers::where('customer_id', $request['user_id'])->first();
        if ($customer) {
            $customer->customer_status = 0;
            $customer->save();
            return response()->json(
                [
                    'status' => 'success',
                    'messages' => 'Account deleted successfully.',
                ],
                200
            );
        } else {
            return response()->json([
                'status' => "failed",
                'data' => new \stdClass,
                'messages' => 'Customer not found'
            ]);
        }
    }
    public function logout(Request $request)
    {
        // clear the token
        /************************************************************* */
        $input['customer_id'] = $request->user_id; // 660 for test purpose
        /************************************************************* */
        if ($customer = Customers::where([['customer_id', '=', $input['customer_id']]])->first()) {
            $customer->oauth_token = null;
            $customer->mobile_verification_code = null;
            $customer->save();
            $response['status'] = 'success';
            $response['message'] = 'Signed out successfully !';
        } else {
            $response['status'] = 'failed';
            $response['message'] = 'User not found !';
        }
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);

    }
    public function myAccount(Request $request)
    {
        /*********************************************************** */
        $data['customer'] = null;
        if ($request['customerId'] && $request['token']) {
            $data['customer'] = Customers::where('customer_id', $request['customerId'])->where('oauth_token', $request['token'])->first();
            if ($data['customer']) {
                $data['customer']->customer_photo_url = $data['customer']->customer_photo_file ? Config::get('url.customer_image_prefix') . $data['customer']->customer_photo_file : Config::get('url.customer_image_prefix') . 'default.png';
            }
        }
        /*********************************************************** */
        $data['request'] = $request->all();
        return response()->json([
            'status' => "success",
            'data' => $data,
            'message' => 'Customer data feched successfully!'
        ], 200, array(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
    }
}
