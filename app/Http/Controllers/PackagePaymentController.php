<?php
namespace App\Http\Controllers;

use App\Area;
use App\CustomerAddress;
use App\Customers;
use App\ServiceType;
use App\SubscriptionPackage;
use App\SubscriptionPackagePayment;
use App\SubscriptionPackageSubscription;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class PackagePaymentController extends Controller
{
    public function getPaymentDetailsByRefId($reference_id)
    {
        try {
            $data['payment'] = SubscriptionPackagePayment::where('reference_id', $reference_id)->where('status', 1)->first();
            $data['subscription'] = SubscriptionPackageSubscription::where('id', $data['payment']->package_subscription_id)->first();
            $data['package'] = SubscriptionPackage::where('package_id', $data['subscription']['package_id'])->first();
            $data['customer'] = Customers::where('customer_id', $data['subscription']['customer_id'])->first();
            $data['customer_address'] = CustomerAddress::where('customer_id', $data['subscription']['customer_id'])->first();
            $data['area'] = Area::find($data['customer_address']['area_id']);
            $data['service'] = ServiceType::where('service_type_id', $data['package']['service_type_id'])->first();
            return response()->json($data, 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 200);
        }
    }
    public function makePaymentForPackage(Request $request)
    {
        /**
         * function to save package payment details
         * Author:  Samnad. S
         * Created Date:    20/05/2023
         * Updated Date:    20/05/2023
         */
        header('Access-Control-Allow-Origin: *'); // CORS policy fix
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS'); // CORS policy fix
        try {
            DB::beginTransaction();
            /***************************** */
            $booking = new SubscriptionPackageSubscription();
            $booking->package_id = $request['package_id'];
            $booking->customer_id = $request['customer_id'];
            $booking->customer_address_id = CustomerAddress::where('customer_id', $request['customer_id'])->where('default_address', 1)->where('address_status', 0)->first()['customer_address_id'];
            $booking->service_date = Carbon::createFromFormat('d-m-Y', $request['service_date'])->format('Y-m-d');
            $booking->addedFrom = $request['platform'];
            $booking->price = $request['amount'];
            $booking->notes = $request['notes'];
            $booking->addedDateTime = date('Y-m-d H:i:s');
            $booking->status = 1;
            $booking->save();
            $booking->reference_id = $this->generateSubReference($booking->id);
            $booking->save();
            /***************************** */
            $payment = new SubscriptionPackagePayment();
            $payment->package_subscription_id = $booking->id;
            $payment->amount = $request['amount'];
            $payment->transaction_charge = $request['transaction_charge'] ?: 0;
            $payment->total_amount = $request['total_amount'];
            $payment->payment_mode = $request['payment_mode'];
            $payment->payment_datetime = date('Y-m-d H:i:s');
            $payment->post_data = $request;
            if ($request['payment_mode'] == 'cash') {
                $payment->payment_status = 'Pending';
            } else {
                $payment->payment_status = 'Initiated';
            }
            $payment->save();
            $payment->reference_id = $booking->reference_id;
            $payment->save();
            /***************************** */
            $data['package'] = SubscriptionPackage::where('package_id', $request['package_id'])->first();
            $data['booking'] = $booking;
            $data['customer'] = Customers::where('customer_id', $data['booking']['customer_id'])->first();
            $data['customer_address'] = CustomerAddress::where('customer_address_id', $booking->customer_address_id)->first();
            $data['address'] = CustomerAddress::where('customer_id', $data['booking']['customer_id'])->first();
            $data['area'] = Area::find($data['address']['area_id']);
            $data['payment'] = $payment;
            DB::commit();
            return response()->json(['status' => true, 'message' => 'Package booking saved successfully !', 'data' => $data], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => false, 'message' => $e->getMessage().$e->getLine(), 'data' => []], 200);
        }
    }
    public function makePayment(Request $request)
    {
        /**
         * function to save package subscription details
         * Author:  Samnad. S
         * Date:    21/01/2023
         */
        try {
            DB::beginTransaction();
            /***************************** */
            $booking = new SubscriptionPackageSubscription();
            $booking->package_id = $request['package_id'];
            $booking->customer_id = $request['customer_id'];
            // make sure customer address is not hijacked
            if (is_numeric($request['customer_address_id']) && CustomerAddress::where('customer_id', $request['customer_id'])->where('customer_address_id', $request['customer_address_id'])->where('address_status', 0)->first()) {
                $booking->customer_address_id = $request['customer_address_id'];
            } else {
                // set default address
                $booking->customer_address_id = CustomerAddress::where('customer_id', $request['customer_id'])->where('default_address', 1)->where('address_status', 0)->first()['customer_address_id'];
            }
            $booking->service_date = Carbon::createFromFormat('d-m-Y', $request['service_date'])->format('Y-m-d');
            $booking->notes = $request['notes'];
            $booking->addedFrom = 'M';
            $booking->addedDateTime = date('Y-m-d H:i:s');
            $booking->status = 1;
            $booking->save();
            /***************************** */
            $booking->reference_id = $this->generateSubReference($booking->id);
            $booking->save();
            /***************************** */
            $payment = new SubscriptionPackagePayment();
            $payment->package_subscription_id = $booking->id;
            $payment->amount = $request['net_charge'];
            $payment->transaction_charge = $request['transaction_charge'];
            $payment->payment_mode = $request['payment_mode'];
            $payment->payment_datetime = date('Y-m-d H:i:s');
            $payment->post_data = $request;
            if ($request['payment_mode'] == 'cash') {
                $payment->payment_status = 'Pending';
            } else {
                $payment->payment_status = 'Initiated';
            }

            $payment->save();
            // $payment->reference_id = $this->generatePayReference($payment->id);
            $payment->reference_id = $booking->reference_id;
            $payment->save();
            /***************************** */
            // actiivity log goes here
            /***************************** */
            $data['package'] = SubscriptionPackage::where('package_id', $request['package_id'])->first();
            $data['booking'] = $booking;
            $data['customer'] = Customers::where('customer_id', $data['booking']['customer_id'])->first();
            $data['address'] = CustomerAddress::where('customer_id', $data['booking']['customer_id'])->first();
            $data['area'] = Area::find($data['address']['area_id']);
            $data['payment'] = $payment;
            DB::commit();
            return response()->json(['status' => 'success', 'message' => 'Package booking saved successfully !', 'data' => $data], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 'error', 'message' => $e->getMessage(), 'data' => []], 200);
        }
    }
    public function changePaymentStatus(Request $request)
    {
        /**
         * function to update online payment data
         * Author:  Samnad. S
         * Date:    25/01/2023
         */
        try {
            DB::beginTransaction();
            // get current payment data for the subscription
            // $current = SubscriptionPackagePayment::where(['package_subscription_id' => $request->order_id, 'status' => 1])->first();
            $current = SubscriptionPackagePayment::where(['reference_id' => $request->order_id, 'status' => 1])->first();
            /***************************** */
            if ($current) {
                $current->package_subscription_id = $current['package_subscription_id'];
                $current->transaction_id = $request->transaction_id;
                $current->amount = $current['amount'];
                $current->payment_mode = $current['payment_mode'];
                $current->payment_datetime = date('Y-m-d H:i:s');
                $current->post_data = $request->post_data ?: $request;
                $current->payment_status = $request->status;
                $current->save();
            } else {
                // create a new one - because it have new transaction id
                $payment = new SubscriptionPackagePayment();
                $payment->package_subscription_id = $current['package_subscription_id'];
                $payment->transaction_id = $request->transaction_id;
                $payment->amount = $current['amount'];
                $payment->payment_mode = $current['payment_mode'];
                $payment->payment_datetime = date('Y-m-d H:i:s');
                $payment->post_data = $request->post_data ?: $request;
                $payment->payment_status = $request->status;
                $payment->save();
                $payment->reference_id = $this->generatePayReference($payment->id);
                $payment->save();
            }

            /***************************** */
            // delete all previous
            //SubscriptionPackagePayment::where('package_subscription_id', $request->order_id)->where('id', '!=', $payment->id)->update(['status' => 0]);
            /***************************** */
            $data['booking'] = SubscriptionPackageSubscription::where('id', $current['package_subscription_id'])->first();
            $data['package'] = SubscriptionPackage::where('package_id', $data['booking']['package_id'])->first();
            $data['payment'] = SubscriptionPackagePayment::where('package_subscription_id', $data['booking']['id'])->where('status', 1)->first();
            $data['customer'] = Customers::where('customer_id', $data['booking']['customer_id'])->first();
            $data['address'] = CustomerAddress::where('customer_id', $data['booking']['customer_id'])->first();
            $data['area'] = Area::find($data['address']['area_id']);
            /***************************** */
            /*$payment = SubscriptionPackagePayment::where(['id'=>$request['payment_id'],'package_subscription_id'=>$request['order_id']])->first();
            $payment->payment_status = $request->status;
            if($request->transaction_id){
            $payment->transaction_id = $request->transaction_id;
            }
            $payment->save();*/
            DB::commit();
            return response()->json(['status' => 'success', 'message' => 'Payment Data Updated !', 'data' => $data], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 200);
        }
    }
    public function checkPaymentStatus(Request $request)
    {
        /**
         * function to check already paid or not
         * Author:  Samnad. S
         * Date:    25/01/2023
         */
        $payment = SubscriptionPackagePayment::where(['reference_id' => $request['order_id'], 'payment_status' => 'success', 'status' => 1])->first();
        return response()->json(['status' => 'success', 'message' => 'Status Checked !', 'data' => $payment], 200);
    }
    public function checkPaymentStatusByStatus($payment_status, $reference_id)
    {
        /**
         * function to get payment single row by ref id
         * Author:  Samnad. S
         * Date:    20/05/2023
         */
        $payment = SubscriptionPackagePayment::where(['reference_id' => $reference_id, 'payment_status' => $payment_status, 'status' => 1])->first();
        return response()->json(['status' => 'success', 'message' => 'Data Retrieved !', 'data' => $payment], 200);
    }
    public function cashPaymentSuccess(Request $request)
    {
        /**
         * function to return data of a successful cash payment
         * Author:  Samnad. S
         * Date:    21/01/2023
         */
        try {
            $data['booking'] = SubscriptionPackageSubscription::where('id', $request['order_id'])->first();
            $data['package'] = SubscriptionPackage::where('package_id', $data['booking']['package_id'])->first();
            $data['payment'] = SubscriptionPackagePayment::where('package_subscription_id', $data['booking']['id'])->first();
            $data['customer'] = Customers::where('customer_id', $data['booking']['customer_id'])->first();
            $data['address'] = CustomerAddress::where('customer_id', $data['booking']['customer_id'])->first();
            $data['area'] = Area::find($data['address']['area_id']);
            $data['service'] = ServiceType::where('service_type_id', $data['package']['service_type_id'])->first();
            /***************************** */
            $dateFormat = date('l jS, F', strtotime($data['booking']['service_date']));
            $message = 'Hi admin, you have a new package booking (Ref - ' . $request['reference_id'] . ') on ' . $dateFormat . ' with ' . $data['customer']->customer_name . ' for ' . $data['service']->service_type_name . '. Please call ' . $data['customer']->mobile_number_1 . ' for any queries.';
            //LoginController::send_sms('582864783', $message);
            /***************************** */
            return response()->json(['status' => 'success', 'message' => 'Package booking saved successfully !', 'data' => $data], 200);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 200);
        }
    }
    public function onlinePaymentFailed(Request $request)
    {
        try {
            $payment = SubscriptionPackagePayment::where('reference_id', $request['order_id'])->where('status', 1)->first();
            if ($payment->reference_id == null) {
                //DHK-PS-ON/2023/1001
                //$payment->reference_id = 'DHK-PS-ON/'.date('Y').'/'.($request['order_id']+1000);
            }
            //$payment->transaction_id = $request['transaction_id'];
            //$payment->payment_status = 'Failed';
            //$payment->save();
            $data['payment'] = $payment;
            $data['booking'] = SubscriptionPackageSubscription::where('id', $request['order_id'])->first();
            $data['package'] = SubscriptionPackage::where('package_id', $data['booking']['package_id'])->first();
            $data['customer'] = Customers::where('customer_id', $data['booking']['customer_id'])->first();
            $data['address'] = CustomerAddress::where('customer_id', $data['booking']['customer_id'])->first();
            $data['area'] = Area::find($data['address']['area_id']);
            return response()->json(['status' => 'success', 'messages' => 'Booking data reteival success !.', 'data' => $data], 200);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 200);
        }
    }
    public function onlinePaymentSuccess(Request $request)
    {
        $payment = SubscriptionPackagePayment::where('package_subscription_id', $request['order_id'])->first();
        if ($payment->reference_id == null) {
            //DHK-PS-ON/2023/1001
            //$payment->reference_id = 'DHK-PS-ON/'.date('Y').'/'.($request['order_id']+1000);
        }
        $payment->transaction_id = $request['transaction_id'];
        //$payment->payment_status = 'Success';
        $payment->save();
        $data['payment'] = $payment;
        $data['booking'] = SubscriptionPackageSubscription::where('id', $payment['package_subscription_id'])->first();
        $data['customer'] = Customers::where('customer_id', $data['booking']['customer_id'])->first();
        $data['address'] = CustomerAddress::where('customer_id', $data['booking']['customer_id'])->first();
        $data['area'] = Area::find($data['address']['area_id']);
        return response()->json(
            [
                'status' => 'success',
                'messages' => 'Booking success.',
                'data' => $data,
            ],
            200
        );
    }
    public static function generatePayReference($id)
    {
        return 'DHK-PS-ON-' . date('Y') . '-' . ($id + 1000);
    }
    public static function generateSubReference($id)
    {
        return "DHK-PS-" . date("Y") . "-" . sprintf('%05d', $id);
    }

    public function updateTamaraPackagePayment(Request $request)
    {
        $reference_id = $request['reference_id'];
        $order_id = $request['order_id'];
        $checkout_id = $request['checkout_id'];

        $online = SubscriptionPackagePayment::where('reference_id', $reference_id)->first();
        if ($online && $online->payment_status != "success") {
            $online->transaction_id = $order_id;
            $online->checkout_id = $checkout_id;
            $online->save();
            return response()->json(
                [
                    'status' => 'success',
                    'messages' => 'Detail saved successfully.',
                ],
                200
            );
        } else {
            return response()->json(
                [
                    'status' => 'error',
                    'messages' => 'Something went wrong. Try again.',
                ],
                200
            );
        }
    }

    public function paymentTamaraSuccess(Request $request)
    {
        $trackId = $request['trackId'];
        $online = SubscriptionPackagePayment::where('transaction_id', $trackId)->first();
        $status_msg = "refresh_success";
        // $status_msg = "success";
        $bookings = SubscriptionPackageSubscription::where('reference_id', $online->reference_id)->first();
        if ($online && $online->payment_status != "success") {
            $online->transaction_id = $trackId;
            $online->payment_status = "success";
            $online->save();

            $status_msg = "success";
        }
        $customer = Customers::where('customer_id', $bookings->customer_id)->first();
        $customerAddress = CustomerAddress::where('customer_id', $bookings->customer_id)->where('default_address', 1)->where('address_status', 0)->first();
        if ($customerAddress) {
            $area = Area::find($customerAddress->area_id);
            $areaName = $area->area_name;
        } else {
            $areaName = 'NA';
            $customerAddress = '';
        }
        $package = SubscriptionPackage::where('package_id', $bookings->package_id)->first();
        $service = ServiceType::where('service_type_id', $package->service_type_id)->first();

        // $dateFormat = date('l jS, F', strtotime($bookings->service_date));

        // $start = date("g:i a", strtotime($bookings->time_from));
        // $end = date("g:i a", strtotime($bookings->time_to));
        // $msg = 'Hi admin, you have a new booking (Ref - ' . $online->reference_id . ') on ' . $dateFormat . ' with ' . $customer->customer_name . ' at ' . $start . ' - ' . $end . ' for ' . $services->service_type_name . '. Please call ' . $customer->mobile_number_1 . ' for any queries.';
        //LoginController::send_sms('582864783',$msg);

        return response()->json(
            [
                'status' => $status_msg,
                'messages' => 'Package Booking saved successfully.',
                'data' => ['booking' => $bookings, 'customer' => $customer, 'address' => $customerAddress, 'services' => $service, 'serviceName' => $service->service_type_name, 'areaName' => $areaName, 'payment' => $online, 'package' => $package],
            ],
            200
        );
    }
}
