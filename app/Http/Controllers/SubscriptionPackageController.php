<?php

/**
 * Author: Samnad. S
 * Date: 04/01/2023
 */

namespace App\Http\Controllers;

use App\SubscriptionPackage;
use Illuminate\Support\Facades\Config;
use App\Customers;
use Illuminate\Http\Request;
use Response;
use DB;

class SubscriptionPackageController extends Controller
{
    public function getPackages(Request $request)
    {
        $packages = SubscriptionPackage::where('status', 1)->get();
        $packages = $packages->each(function ($item, $key) {
            $item->promotion_image_url = $item->promotion_image ? Config::get('url.package_promotion_image_prefix') . $item->promotion_image : null;
            $item->webview_url = Config::get('url.webview_base') . "package/view/" . $item->package_id."?mobile";
        });
        $response = array('status' => 'success', 'data' => $packages);
        return Response::json($response, 200, ['Content-type' => 'application/json; charset=utf-8'], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
    }
    public function getPackageById(Request $request,$id)
    {
        /*********************************************************** */
        if($request['customerId'] && $request['token']){
            if(!$data['customer'] = Customers::where('customer_id', $request['customerId'])->where('oauth_token', $request['token'])->first())
            {
            }
        }
        else if($request['customerId']){
            if(!$data['customer'] = Customers::where('customer_id', $request['customerId'])->first())
            {
            }
        }
        else{
        }
        /*********************************************************** */
        $package = SubscriptionPackage::where('status', 1)->where('package_id', $id)->first();
        $package->promotion_image_url = $package->promotion_image ? Config::get('url.package_promotion_image_prefix') . $package->promotion_image : null;
        $package->webview_url = Config::get('url.webview_base') . "package/view/" . $package->package_id."?mobile";
        //$response = array('status' => 'success', 'data' => $package);
        $data['package'] = $package;
        $response['status'] = 'success';
        $data['holiday_dates'] = array_column(DB::table('holidays')->where([['deleted_at', '=', null], ['date', '>=', date('Y-m-d')]])->get()->toArray(),'date');
        $data['weekends'] = array_column(DB::table('week_days')->where([['weekend', '=', 1]])->get()->toArray(),'week_day_id');
        $response['data'] = $data;
        $response['request'] = $request->all();
        return Response::json($response, 200, ['Content-type' => 'application/json; charset=utf-8'], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
    }
}
