<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bookings;
use App\BookingExtraServices;
use App\ServiceType;
use App\ExtraServices;
use App\CouponCode;
use App\BookingSlots;
use App\CustomerAddress;
use App\BookMapping;
use App\ServiceCategoryCost;
use App\Customers;
use App\OnlinePayment;
use App\CustomerPayments;
use App\Area;
use App\CustomerCoupons;
use App\BookingCancel;
use App\BookingCleaningSupplies;
use App\UserActivity;
use DateTime;
use Illuminate\Support\Facades\Config;
use DateInterval;
use Illuminate\Support\Facades\Mail;
use App\Mail\SuccessMailAdmin;
use App\Mail\SuccessMail;
use App\Http\Controllers\LoginController;
use DB;
use Carbon\Carbon;
use stdClass;
use App\CustomerNotifications;



class PaymentController extends Controller
{
    /**
     * function to save payment details
     * Author:Karthika
     * Date:04/09/2020
     */
    public function makePayment(Request $request)
    {
        try {
            /******************************************************************** */
            /**
             * Purpose  :   date and time validation (blocks booking if minimum time not met)
             * Author   :   Samnad S
             * Date     :   10/10/2023
             */
            $service_date = $request['cleaning_date'];
            $time_from = $request['fromTime'];
            $current_time = Carbon::now()->format('Y-m-d H:i:s');
            //$current_time = "2023-10-10 13:01:00"; // debug
            $warm_up_time = 60; //in minutes (it's used with current time to create minimum booking start time)
            $available_from_date_time = Carbon::createFromFormat('Y-m-d H:i:s', $current_time)->addMinutes($warm_up_time)->format('Y-m-d H:i:s');
            $booking_date_time = Carbon::createFromFormat('Y-m-d H:i:s', $service_date . ' ' . $time_from)->format('Y-m-d H:i:s');
            try {
                if ($booking_date_time < $available_from_date_time) {
                    // throw exception
                    throw new \ErrorException('Please select time slot after ' . Carbon::createFromFormat('Y-m-d H:i:s', $available_from_date_time)->format('d/m/Y h:i A'));
                }
                else if ($holiday = DB::table('holidays')->where([['deleted_at', '=', null], ['date', '=', $service_date]])->first()) {
                    // selected date is holiday
                    throw new \ErrorException('Sorry, ' . Carbon::createFromFormat('Y-m-d', $service_date)->format('d/m/Y').' is holiday, please select another day.');
                }
            } catch (\Exception $e) {
                return response()->json(['status' => 'failed', 'messages' => $e->getMessage()]);
            }
            /******************************************************************** */
            DB::beginTransaction();
            $order_reference_id = "";
            $customerId = $request['customerId'];
            $payment_mode = $request['payment_mode'];
            $instructions = $request['instructions'];
            $crew_in = $request['crew_in'];
            $serviceId = $request['serviceId'];
            $noOfHours = $request['noOfHours'];
            $cleaningMaterialStatus = $request['cleaningMaterialStatus'];
            $noOfMaids = $request['noOfMaids'] > 0 ? $request['noOfMaids'] : 1;
            $no_of_maids = $request['noOfMaids'] > 0 ? $request['noOfMaids'] : 1;
            // $extraServices=$request['extraServices'];
            $how_often = $request['how_often'];
            $cleaning_date = $request['cleaning_date'];
            $toTime = $request['toTime'];
            $fromTime = $request['fromTime'];
            $monthDurations = $request['monthDurations'];
            $hour_rate = $request['hour_rate'];
            // $discount=$request['discount'];
            $service_charge = $request['service_charge'];
            $vat_charge = $request['vat_charge'];
            $total_amount = $request['total_amount'];
            // $net_cleaning_fee=$request['net_cleaning_fee'];
            // $net_service_charge=$request['net_service_charge'];
            // $net_discount=$request['net_discount'];
            // $net_vat_charge=$request['net_vat_charge'];
            // $total_net_amount=$request['total_net_amount'];
            $cleaning_material_fee = $request['cleaning_material_fee'];
            $coupon_code = $request['coupon_code'];
            $interior = $request['interior'];
            $ironing = $request['ironing'];
            $fridge = $request['fridge'];
            $oven = $request['oven'];
            $startDate = new DateTime($cleaning_date);
            $dateIntervel = new DateTime($cleaning_date);
            if ($how_often != 'OD') {
                if ($monthDurations == '') {
                    $monthDurations = 1;
                }
                if ($monthDurations == 'Continue') {
                    $actual_ending_date = $startDate;
                    $ending_date = $startDate;
                } else {
                    $dateIntervel->add(new DateInterval('P' . $monthDurations . 'M'));
                    $actual_ending_date = $dateIntervel->format('Y-m-d');
                    $ending_date = $dateIntervel->format('Y-m-d'); // 2016-01-02
                    $endDate = new DateTime($ending_date);
                }
            } else {
                $actual_ending_date = $startDate;
                $ending_date = $startDate;
            }
            /********************************************************************************************************************* */
            // insert seperate for each maids
            for ($i = 1; $i <= $no_of_maids; $i++) {
                $booking_row = new stdClass();
                $booking_row->booking_note = $instructions;
                $booking_row->booking_type = $how_often;
                $booking_row->booking_category = 'C';
                if ($monthDurations == 'Continue') {
                    $booking_row->service_end = 0;
                } else {
                    $booking_row->service_end = 1;
                }
                $booking_row->service_end_date = $ending_date;
                $booking_row->service_actual_end_date = $actual_ending_date;
                $booking_row->pending_amount = 0;
                $_payment_type_charge = 0;
                if ($payment_mode == 'cash') {
                    $booking_row->total_amount = $request['total_amount'] / $no_of_maids;
                    $booking_row->pay_by_cash_charge = Config::get('values.pay_by_cash_charge') / $no_of_maids;
                    $_payment_type_charge =  $booking_row->pay_by_cash_charge;
                } else {
                    $booking_row->total_amount = $request['total_amount'] / $no_of_maids;
                    $booking_row->pay_by_cash_charge = 0;
                    $_payment_type_charge =  $booking_row->pay_by_cash_charge;
                }
                $booking_row->_payment_type_charge = $_payment_type_charge;
                $booking_row->price_per_hr = $request['hour_rate'];
                $booking_row->service_charge = $request['service_charge'] / $no_of_maids;
                $booking_row->service_charge = ($request['total_amount'] - $request['vat_charge']) / $no_of_maids;
                $booking_row->vat_charge = $request['vat_charge'] / $no_of_maids;
                $booking_row->net_cleaning_fee = 0;
                $booking_row->net_service_charge = 0;
                $booking_row->net_discount = 0;
                $booking_row->crew_in = $crew_in;
                $booking_row->net_vat_charge = 0;
                $booking_row->total_net_amount = 0;
                $booking_row->cleaning_material_fee = $request['cleaning_material_fee'] / $no_of_maids;

                $booking_row->month_durations = $request['monthDurations']; //new field
                $booking_row->service_start_date = $request['cleaning_date']; //
                $booking_row->service_week_day = date('w', strtotime($request['cleaning_date']));

                $booking_row->time_from = $fromTime; //
                $booking_row->time_to = $toTime; //
                $booking_row->customer_id = $customerId; //

                if ($payment_mode == 'card') {
                    $booking_row->pay_by = 'Card';
                    $booking_row->booking_status = 3; // status 0 when payment done
                } else {
                    $booking_row->pay_by = 'Cash';
                    $booking_row->booking_status = 0;
                }

                $booking_row->payment_type_id = $request['payment_type_id'];
                $customer = Customers::where('customer_id', $customerId)->first();
                $customerAddress = CustomerAddress::where('customer_id', $customerId)->where('address_status', 0)->where('default_address', 1)->first();
                $booking_row->customer_address_id = $customerAddress['customer_address_id'];
                $areaaid = $customerAddress['area_id'];
                $areass = Area::where('area_id', $areaaid)->first();
                $booking_row->service_type_id = $request['serviceId'];
                $booking_row->no_of_maids = $no_of_maids;
                $booking_row->no_of_hrs = $request['noOfHours'];
                $booking_row->cleaning_material = $request['cleaningMaterialStatus'];
                $dt = new DateTime;
                $booking_row->reference_id = '';
                $booking_row->maid_id = null;
                $booking_row->is_locked = 0;
                $booking_row->booked_by = null;
                if ($request['is_device'] == 'mobile') {
                    $booking_row->booked_from = 'M';
                } else if ($request['is_device'] == 'web') {
                    $booking_row->booked_from = 'W';
                }
                //$booking_row->booking_status = 0;
                $booking_row->booked_datetime = $dt->format('y-m-d H:i:s');
                $booking_row->discount = 0;
                /************************************* */
                if($coupon_code > 0){
                    $booking_row->coupon_id = $coupon_code;
                    $booking_row->discount = $request['discount_price'] / $no_of_maids;
                    $booking_row->coupon_used = CouponCode::find($coupon_code)->coupon_name;
                }
                /************************************* */
                $booking_row->created_at = Carbon::now()->format('Y-m-d H:i:s');
                $booking_row->updated_at = Carbon::now()->format('Y-m-d H:i:s');
                /************************************* */
                //
                //$booking_row->_total_amount = $request['payment_type_charge'];
                //$booking_row->_payment_type_charge = $request['total_amount'];
                //$booking_row->_payment_type_charge = @$request['payment_type_charge'] ?:0;
                /************************************* */
                $id = Bookings::insertGetId((array) $booking_row);
                $booking = Bookings::find($id);
                $booking_common_id = @$booking_common_id ?: $id;
                if ($no_of_maids > 1) {
                    // for act as single booking use common id
                    $booking->booking_common_id = $booking_common_id;
                }
                $booking->reference_id = "MM-" . date("Y") . "-" . sprintf('%04d', $booking_common_id);
                $booking->save();
                $bookings[] = $booking;
            }
            if($coupon_code > 0){
                $customerCoupon = new CustomerCoupons();
                $customerCoupon->customer_id = $customerId;
                $customerCoupon->coupon_id = $request['coupon_code'];
                $customerCoupon->coupon_name = CouponCode::find($coupon_code)->coupon_name;
                $customerCoupon->booking_id = $booking_common_id;
                $customerCoupon->reference_id = $bookings[0]->reference_id;
                $customerCoupon->discount = $request['discount_price'];
                $customerCoupon->save();
            }
            /******************************************************************* */
            if ($payment_mode == 'card') {
                $transaction_charge = ($bookings[0]->total_amount * 0.03); // 3 percentage
                $transaction_charge = number_format((float) $transaction_charge, 2, '.', '');
                $onlinePayment = new OnlinePayment();
                $onlinePayment->booking_id = $bookings[0]->booking_id;
                $onlinePayment->reference_id = $bookings[0]->reference_id;
                $onlinePayment->amount = $bookings[0]->total_amount * $no_of_maids;
                $onlinePayment->transaction_charge = $transaction_charge * $no_of_maids;
                $onlinePayment->customer_id = $bookings[0]->customer_id;
                $onlinePayment->payment_status = 'initiated';
                $onlinePayment->payment_type = $payment_mode;
                if ($request['is_device'] == 'mobile') {
                    $onlinePayment->paid_from = 'M';
                } else if ($request['is_device'] == 'web') {
                    $onlinePayment->paid_from = 'W';
                }
                $onlinePayment->payment_datetime = $dt->format('y-m-d H:i:s');
                $onlinePayment->user_agent = '';
                $onlinePayment->post_data = '';
                $onlinePayment->save();
            } else {
                $transaction_charge = 0;
            }
            /********************************************************************************************************************* */
            $custom_supplies_amount = 0;
            /******************************************************************************** */
            $plan_based_supplies_amount = 0;
            /******************************************************************************** */
            // if extra services selected
            $extra_services_amount = 0;
            foreach ((array) @$request['extra_services'] as $key => $extra_service_id) {
                $booking_extra_services = new BookingExtraServices();
                $booking_extra_services->booking_id = $booking_common_id;
                $booking_extra_services->extra_service_id = $extra_service_id;
                $booking_extra_services->duration = $request['extra_services_duration'][$key];
                $booking_extra_services->unit_rate = $request['extra_services_cost'][$key];
                $booking_extra_services->quantity = 1;
                $booking_extra_services->total_amount = $booking_extra_services->unit_rate * $booking_extra_services->quantity;
                $extra_services_amount += $booking_extra_services->total_amount;
                $booking_extra_services->save();
                //
                $bookings_extra_services = 1;
                $bookings_extra_services_amount = $extra_services_amount;
            }
            /******************************************************************************** */
            // service category insert
            foreach ((array) @$request['_service_category'] as $key => $service_category) {
                $servicecategorycost = ServiceCategoryCost::find($service_category);
                $bookingservicemapping = new BookMapping();
                $bookingservicemapping->booking_id = $booking_common_id;
                $bookingservicemapping->service_id = $servicecategorycost->service_id;
                $bookingservicemapping->category_id = $servicecategorycost->category_id;
                $bookingservicemapping->sub_category_id = $servicecategorycost->sub_category_id;
                $bookingservicemapping->service_sub_furnish_id = $servicecategorycost->service_sub_funish_id;
                $bookingservicemapping->is_scrubbing = $servicecategorycost->is_scrubbing;
                $bookingservicemapping->name = $servicecategorycost->name;
                $bookingservicemapping->service_cost = $request['_service_category_rate'][$key];
                $bookingservicemapping->quantity = $request['_service_category_quantity'][$key];
                $bookingservicemapping->squarefeet_count = null;
                $bookingservicemapping->status = 1;
                $bookingservicemapping->save();
            }
            /******************************************************************************** */
            if (@$request['_category'] && @$request['_sqft'] && @$request['_sqft_rate']) {
                // squarefeet based data insert
                $bookingservicemapping = new BookMapping();
                $bookingservicemapping->booking_id = $booking_common_id;
                $bookingservicemapping->service_id = $request['serviceId'];
                $bookingservicemapping->category_id = $request['_category'];
                $bookingservicemapping->sub_category_id = 0;
                $bookingservicemapping->service_sub_furnish_id = 0;
                $bookingservicemapping->is_scrubbing = 0;
                $bookingservicemapping->name = $request['_sqft'] . ' Sq. Ft.';
                $bookingservicemapping->service_cost = $request['_sqft_rate'] * $request['_sqft'];
                $bookingservicemapping->quantity = 1;
                $bookingservicemapping->squarefeet_count = $request['_sqft'];
                $bookingservicemapping->status = 1;
                $bookingservicemapping->save();
            }
            /******************************************************************************** */
            // save customer property data
            //$bookings->property_details = $request['_property_details'] ?: null;
            /******************************************************************************** */
            if (@$request['_service_week_days']) {
                // squarefeet based data insert
                //$bookings->service_week_days = implode(',', $request['_service_week_days']);
            }
            /******************************************************************************** */
            //$bookings->save();
            foreach ($bookings as $key => $booking_sel) {
                $booking = Bookings::find($booking_sel['booking_id']);
                //$booking->property_details = $request['_property_details'] ?: null;
                //$booking->service_week_days = implode(',', $request['_service_week_days'] ?: []);
                //$booking->custom_supplies = @$bookings_custom_supplies ?: 0;
                //$booking->custom_supplies_amount = @$bookings_custom_supplies_amount ?: 0;
                //$booking->plan_based_supplies = @$bookings_plan_based_supplies ?: 0;
                //$booking->plan_based_supplies_amount = @$bookings_plan_based_supplies_amount ?: 0;
                // if super_visor_selected
                if (@$request['supervisor_status'] == "Y") {
                    //$booking->supervisor_selected = "Y";
                    //$booking->supervisor_charge_hourly = $request['supervisor_charge_hourly'];
                    //$booking->supervisor_charge_total = ($request['supervisor_charge_hourly'] * $request['noOfHours']) / $no_of_maids;
                } else {
                    //$booking->supervisor_selected = "N";
                }
                //$booking->extra_services = @$bookings_extra_services ?: 0;
                //$booking->extra_services_amount = @$bookings_extra_services_amount ?: 0;
                $booking->save();
            }
            /******************************************************************************** */
            $booking = Bookings::find($bookings[0]->booking_id);
            /******************************************************************************** */
            if ($payment_mode == 'cash') {
                /*$notification = new CustomerNotifications();
                $notification->customer_id = $customerId;
                $notification->booking_id = @$booking_common_id;
                $notification->service_date = @$cleaning_date;
                $notification->title = "{{booking_ref_id}}";
                $notification->content = "New booking created with Ref. No. {{booking_ref_id}}.";
                $notification->save();*/
            }
            /******************************************************************************** */
            $data = ['bookings' => $booking, 'transaction_charge' => $transaction_charge, 'customer' => $customer, 'customer_address' => $customerAddress, 'payment_mode' => $payment_mode];
            /************************************************************************** */
            if ($payment_mode == 'cash') {
                $mail = new AdminApiMailController;
                $mail->booking_confirmation_to_customer($booking->booking_id);
            }
            else if ($payment_mode == 'card') {
                $params = [
                    'method' => 'create',
                    'store' => Config::get('values.telr_store_id'),
                    'authkey' => Config::get('values.telr_auth_key'),
                    'framed' => '0',
                    "language" => "en",
                    "ivp_applepay" => "1",
                    "order" => [
                            "cartid" => $bookings[0]->booking_id,
                            "test" => Config::get('values.telr_test_environment'),
                            "amount" => ($onlinePayment->amount + $transaction_charge),
                            "currency" => "AED",
                            "description" => $bookings[0]->reference_id,
                            "trantype" => "sale"
                        ],
                    "customer" => [
                        "ref" => "custref11",
                        "email" => $customer->email_address,
                        "name" => [
                                "forenames" => $customer->customer_name,
                                "surname" => null
                            ],
                        "address" => [
                            "line1" => $customerAddress->customer_address,
                            "city" => 'city',
                            "country" => "UAE"
                        ],
                        "phone" => $customer->mobile_number_1
                    ],
                    "return" => [
                        "authorised" => Config::get('values.web_app_url') . ('api/telr/process/' . $onlinePayment->payment_id),
                        "declined" => Config::get('values.web_app_url').('payment-error/' . $bookings[0]->reference_id . '/' . $bookings[0]->booking_id),
                        "cancelled" => Config::get('values.web_app_url').('payment-error/' . $bookings[0]->reference_id . '/' . $bookings[0]->booking_id),
                    ]
                ];
                $client = new \GuzzleHttp\Client(['verify' => false]);
                $response = $client->request('POST', 'https://secure.telr.com/gateway/order.json', [
                    'body' => json_encode($params),
                    'headers' => [
                            'Content-Type' => 'application/json',
                            'accept' => 'application/json',
                        ],
                ]);
                $data['telr'] = json_decode((string) $response->getBody(), true);
                $onlinePayment->transaction_id = $data['telr']['order']['ref'];
                $onlinePayment->save();
            }
            DB::commit();
            return response()->json(['status' => 'success', 'messages' => 'Booking saved successfully.', 'data' => $data], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 'failed', 'messages' => $e->getMessage().' '.$e->getLine(),'error_line' => $e->getLine()]);
        }
    }
    // public function encrypt($plainText,$key)
    // {  
    //     // return $plainText;
    //     $secretKey = PaymentController::hextobin(md5($key));
    // 	$initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
    //   	$openMode = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '','cbc', '');
    //   	$blockSize = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, 'cbc');
    // 	$plainPad = PaymentController::pkcs5_pad($plainText, $blockSize);
    //   	if (mcrypt_generic_init($openMode, $secretKey, $initVector) != -1) 
    // 	{
    // 	      $encryptedText = mcrypt_generic($openMode, $plainPad);
    //       	      mcrypt_generic_deinit($openMode);

    // 	} 
    // 	return bin2hex($encryptedText);
    // }

    // public function decrypt($encryptedText,$key)
    // {
    // 	$secretKey = PaymentController::hextobin(md5($key));
    // 	$initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
    // 	$encryptedText=PaymentController::hextobin($encryptedText);
    //   	$openMode = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '','cbc', '');
    // 	mcrypt_generic_init($openMode, $secretKey, $initVector);
    // 	$decryptedText = mdecrypt_generic($openMode, $encryptedText);
    // 	$decryptedText = rtrim($decryptedText, "\0");
    //  	mcrypt_generic_deinit($openMode);
    // 	return $decryptedText;

    // }
    // //*********** Padding Function *********************

    // public static function pkcs5_pad ($plainText, $blockSize)
    // {
    //     $pad = $blockSize - (strlen($plainText) % $blockSize);
    //     return $plainText . str_repeat(chr($pad), $pad);
    // }

    // //********** Hexadecimal to Binary function for php 4.0 version ********

    // public static function hextobin($hexString) 
    // { 
    //         $length = strlen($hexString); 
    //     	$binString="";   
    //     	$count=0; 
    //     	while($count<$length) 
    //     	{       
    //     	    $subString =substr($hexString,$count,2);           
    //             $packedString = pack("H*",$subString); 
    //     	    if ($count==0)
    //             {
    //                 $binString.=$packedString;
    //             } 
    //             else 
    //             {
    //                 $binString.=$packedString;
    //             } 
    //             $count+=2; 

    //     	} 
    //         return $binString; 
    // } 
	
	public function updateTamaraPayment(Request $request)
    {
		$reference_id = $request['reference_id'];
        $order_id = $request['order_id'];
        $checkout_id = $request['checkout_id'];
		
		$online = OnlinePayment::where('reference_id', $reference_id)->first();
        if($online && $online->payment_status != "success") {
            $online->transaction_id = $order_id;
            $online->checkout_id = $checkout_id;
            $online->save();
			return response()->json(
				[
					'status' => 'success',
					'messages' => 'Detail saved successfully.',
				],
				200
			);
        } else {
			return response()->json(
				[
					'status' => 'error',
					'messages' => 'Something went wrong. Try again.',
				],
				200
			);
		}
    }
	
	
    /**
     * function to calculate total price
     * Author:Karthika
     * Date:05/09/2020
     */
    public function calculateTotalPrice(Request $request)
    {
        $service = ServiceType::find($request['serviceId']);
        if (!$service) {
            return response()->json(
                [
                    'status' => 'Failed',
                    'messages' => 'Service type not found.',
                ]
            );
        }
        $no_hrs = $request['numberHours'];
        $no_maids = $request['numberMaids'];
        $cleaningStatus = $request['cleaningStatus'];
        // $extraServices = $request['extraServices'];
        $coupon = ucfirst($request['coupon']);
        $customerId = $request['customerId'];
        $bookDate = $request['bookedDate'];
        $monthDuration = $request['monthDuration'];
        $visitType = $request['visitType'];
        $interior = $request['interior'];
        $ironing = $request['ironing'];
        $fridge = $request['fridge'];
        $oven = $request['oven'];
        if ($bookDate != '') {

            $weekday = date('w', strtotime($bookDate));
            $startDate = new DateTime($bookDate);
            if ($visitType != 'OD') {
                $dateIntervel = new DateTime($bookDate);
                if ($monthDuration == '') {
                    $monthDuration = 1;
                }
                if ($monthDuration == 'Continue') {
                    $actual_ending_date = $startDate;
                    $ending_date = $startDate;
                } else {
                    $dateIntervel->add(new DateInterval('P' . $monthDuration . 'M'));
                    $actual_ending_date = $dateIntervel->format('Y-m-d');
                    $ending_date = $dateIntervel->format('Y-m-d');  // 2016-01-02
                    $endDate = new DateTime($ending_date);
                    $resultDays = array(
                        '0' => 0,
                        '1' => 0,
                        '2' => 0,
                        '3' => 0,
                        '4' => 0,
                        '5' => 0,
                        '6' => 0
                    );
                    // iterate over start to end date 
                    while ($startDate <= $endDate) {
                        // find the timestamp value of start date 
                        $timestamp = strtotime($startDate->format('d-m-Y'));
                        // find out the day for timestamp and increase particular day 
                        $weekDay = date('w', $timestamp);
                        $resultDays[$weekDay] = $resultDays[$weekDay] + 1;
                        // increase startDate by 1 
                        $startDate->modify('+1 day');
                    }
                    $number_of_weeks = $resultDays[$weekday];
                }
            } else {
                $actual_ending_date = $startDate;
                $endDate = $startDate;
            }


            $no_visits = 1;
            // if ($visitType == 'OD') {
            //     $no_visits = 1;
            // } elseif ($visitType == 'WE') {
            //     $no_visits = $number_of_weeks;
            // } elseif ($visitType == 'BW') {
            //     $no_visits = round($number_of_weeks/2);
            // }
        }
        $vat_percentage = Config::get('values.vat_amount');

        if ($customerId != "") {
            $cust_id = $customerId;
        } else {
            $cust_id = "";
        }
        // // $coupon = ucfirst($this->input->post('coupon_code'));
        // $booktype = $this->input->post('hiddenbooktype');
        // // $no_hrs = $this->input->post('select_hours');
        // $no_maids = $this->input->post('select_maids');
        // $no_visits = $this->input->post('no_of_visits');
        // $service_types = $this->input->post('service_types');


        //$interior_cost = $this->input->post('interiorcost');
        //$fridge_cost = $this->input->post('fridgecost');
        //$iron_cost = $this->input->post('ironingcost');
        //$oven_cost = $this->input->post('ovencost');
        // $clean_val = $this->input->post('hiddencleaningval');
        // $get_fee_details = $this->default_model->get_servicefee_details($service_types);
        //        $get_fee_details = $this->default_model->get_fee_details();
        $bookedDate = date("Y-m-d", strtotime($bookDate));
        if ($cleaningStatus == "N") {
            //$per_hour_rate = $get_fee_details[0]->price_n;//new
            $per_hour_rate = $service->service_rate;
            $cleaning_material_rate = 0;
            $price_to_show = (int)$service->service_rate;
        } else {
            //$per_hour_rate = $get_fee_details[0]->price_c;
            //$per_hour_rate = $get_fee_details[0]->price_n;//new
            $per_hour_rate = $service->service_rate;
            if ($service->material_incl == "Y") {
                $cleaning_material_rate = 0;
            } else {
                $cleaning_material_rate = Config::get('values.cleaning_amount');
            }
            $price_to_show = (int)$service->service_rate;
        }
        if ($no_visits != 0) {
            $a_service_rate = ((($no_hrs * $per_hour_rate) * $no_maids) * $no_visits);
            $cleaning_rates = ((($no_hrs * $cleaning_material_rate) * $no_maids) * $no_visits);
        } else {
            $a_service_rate = (($no_hrs * $per_hour_rate) * $no_maids);
            $cleaning_rates = (($no_hrs * $cleaning_material_rate) * $no_maids);
        }
        $service_rate = ($a_service_rate + $cleaning_rates);
        $vat_charge = ($service_rate * ($vat_percentage / 100));
        $gross_amount = ($service_rate + $vat_charge);
        $total_service_rate = 0;
        $discount = 0;



        if ($coupon == "") {
            $data = array('vat_charge' => $vat_charge, 'service_rate' => $service_rate, 'gross_amount' => $gross_amount, 'hour_rate' => $per_hour_rate, 'net_amount' => $total_service_rate, 'discount' => $discount, 'price_to_show' => $price_to_show, 'cleaningrate' => $cleaning_rates, 'serv_rate' => $a_service_rate);
            return response()->json([
                'status' => "success",
                'data' => $data,
                'message' => 'Amount details fetched!'
            ], 200);
        } else {
            $today = date('Y-m-d');


            $get_coupon_id = $this->default_model->get_coupon_id($coupon);
            $couponCode = CouponCode::where('coupon_name', $coupon)
                ->where('type', 'C')
                ->where('status', 1)
                ->where('expiry_date >=', $today)->first();
            if ($service_types != 1) {
                return response()->json([
                    'status' => "success",
                    'data' => array('vat_charge' => $vat_charge, 'service_rate' => $service_rate, 'gross_amount' => $gross_amount, 'hour_rate' => $per_hour_rate, 'net_amount' => $total_service_rate, 'discount' => $discount, 'price_to_show' => $price_to_show, 'cleaningrate' => $cleaning_rates, 'serv_rate' => $a_service_rate),
                    'message' => 'Coupon not valid for the selected service!'
                ], 200);
            }
            if ($no_hrs < $couponCode->min_hrs) {
                return response()->json([
                    'status' => "success",
                    'data' => array('vat_charge' => $vat_charge, 'service_rate' => $service_rate, 'gross_amount' => $gross_amount, 'hour_rate' => $per_hour_rate, 'net_amount' => $total_service_rate, 'discount' => $discount, 'price_to_show' => $price_to_show, 'cleaningrate' => $cleaning_rates, 'serv_rate' => $a_service_rate),
                    'message' => 'Coupon only valid for minimum ' . $get_coupon_id->min_hrs . 'hours booking!'
                ], 200);
            }
            $coupon_type = $couponCode->coupon_type;
            $expiry_date = $couponCode->expiry_date;
            $offer_type = $couponCode->offer_type;

            if ($expiry_date >= $bookedDate) {
                if ($offer_type == "P") {
                    if ($coupon_type == "FT") {
                        if ($cust_id != "") {
                            $checkbooking = Bookings::where('customer_id', $cust_id)->get();
                            if (count($checkbooking) != 0) {
                                $msg = 'Coupon valid for first booking only.';
                                return response()->json([
                                    'status' => "success",
                                    'data' => array('vat_charge' => $vat_charge, 'service_rate' => $service_rate, 'gross_amount' => $gross_amount, 'hour_rate' => $per_hour_rate, 'net_amount' => $total_service_rate, 'discount' => $discount, 'price_to_show' => $price_to_show, 'cleaningrate' => $cleaning_rates, 'serv_rate' => $a_service_rate),
                                    'message' => $msg
                                ], 200);
                            }
                        }
                    }

                    $v_week_day = $couponCode->valid_week_day;
                    $weekArray = explode(',', $v_week_day);

                    if (in_array($weekday, $weekArray)) {
                        if ($cleaningStatus == "N") {
                            $price_to_show = (int)$per_hour_rate;
                            $per_hour_rate = $per_hour_rate;
                            $cleaning_material_rate = 0;
                        } else {
                            $price_to_show = (int)$per_hour_rate;
                            $per_hour_rate = $per_hour_rate;
                            if ($service->material_incl == "Y") {
                                $cleaning_material_rate = 0;
                            } else {
                                $cleaning_material_rate = Config::get('values.cleaning_amount');
                            }
                        }

                        if ($no_visits != 0) {
                            $a_service_rate = ((($no_hrs * $per_hour_rate) * $no_maids) * $no_visits);
                            $cleaning_rates = ((($no_hrs * $cleaning_material_rate) * $no_maids) * $no_visits);
                        } else {
                            $a_service_rate = (($no_hrs * $per_hour_rate) * $no_maids);
                            $cleaning_rates = (($no_hrs * $cleaning_material_rate) * $no_maids);
                        }
                        $service_rate_new = ($a_service_rate_new + $cleaning_rates_new);

                        $perhrrate = $couponCode->percentage;
                        $aservicerate_new = ((($no_hrs * $perhrrate) * $no_maids) * $no_visits);
                        $serviceratenew = ($aservicerate_new + $cleaning_rates_new);

                        $discount = ($service_rate - $serviceratenew);
                        $getfee = ($service_rate - $discount);
                        $total_service_rate = $getfee;
                        $vat_charge = ($getfee * ($vat_percentage / 100));
                        $gross_amount = ($getfee + $vat_charge);
                        $msg = 'Coupon applied successfully.';
                        return response()->json([
                            'status' => "success",
                            'data' => array('vat_charge' => $vat_charge, 'service_rate' => $service_rate, 'gross_amount' => $gross_amount, 'hour_rate' => $per_hour_rate, 'net_amount' => $total_service_rate, 'discount' => $discount, 'price_to_show' => $price_to_show, 'cleaningrate' => $cleaning_rates_new, 'serv_rate' => $a_service_rate_new, 'coupon_id' => $couponCode->coupon_id),
                            'message' => $msg
                        ], 200);
                    } else {
                        $msg = 'Coupon not valid for selected day.';
                        return response()->json([
                            'status' => "success",
                            'data' => array('vat_charge' => $vat_charge, 'service_rate' => $service_rate, 'gross_amount' => $gross_amount, 'hour_rate' => $per_hour_rate, 'net_amount' => $total_service_rate, 'discount' => $discount, 'price_to_show' => $price_to_show, 'cleaningrate' => $cleaning_rates, 'serv_rate' => $a_service_rate),
                            'message' => $msg
                        ], 200);
                    }
                } else {
                    if ($coupon_type == "FT") {
                        if ($cust_id != "") {
                            $checkbooking = Bookings::where('customer_id', $cust_id)->get();
                            if (count($checkbooking) != 0) {
                                $msg = 'Coupon valid for first booking only.';
                                return response()->json([
                                    'status' => "success",
                                    'data' => array('vat_charge' => $vat_charge, 'service_rate' => $service_rate, 'gross_amount' => $gross_amount, 'hour_rate' => $per_hour_rate, 'net_amount' => $total_service_rate, 'discount' => $discount, 'price_to_show' => $price_to_show, 'cleaningrate' => $cleaning_rates, 'serv_rate' => $a_service_rate),
                                    'message' => $msg
                                ], 200);
                            }
                        }
                    }

                    $v_week_day = $couponCode->valid_week_day;
                    $weekArray = explode(',', $v_week_day);

                    if (in_array($weekday, $weekArray)) {
                        if ($cleaningStatus == "N") {
                            $per_hour_rate = $service->service_rate;
                            $cleaning_material_rate = 0;
                        } else {
                            $per_hour_rate = $service->service_rate;
                            if ($service->material_incl == "Y") {
                                $cleaning_material_rate = 0;
                            } else {
                                $cleaning_material_rate = 10;
                            }
                        }
                        if ($no_visits != 0) {
                            $a_service_rate = ((($no_hrs * $per_hour_rate) * $no_maids) * $no_visits);
                            $cleaning_rates = ((($no_hrs * $cleaning_material_rate) * $no_maids) * $no_visits);
                        } else {
                            $a_service_rate = (($no_hrs * $per_hour_rate) * $no_maids);
                            $cleaning_rates = (($no_hrs * $cleaning_material_rate) * $no_maids);
                        }


                        $service_rate_new = ($a_service_rate_new + $cleaning_rates_new);

                        $discount = $couponCode->percentage;

                        $getfee = ($service_rate - $discount);
                        $total_service_rate = $getfee;
                        $vat_charge = ($getfee * ($vat_percentage / 100));
                        $gross_amount = ($getfee + $vat_charge);
                        $msg = 'Coupon applied successfully.';
                        return response()->json([
                            'status' => "success",
                            'data' => array('vat_charge' => $vat_charge, 'service_rate' => $service_rate, 'gross_amount' => $gross_amount, 'hour_rate' => $per_hour_rate, 'net_amount' => $total_service_rate, 'discount' => $discount, 'price_to_show' => $per_hour_rate, 'cleaningrate' => $cleaning_rates_new, 'serv_rate' => $a_service_rate_new, 'coupon_id' => $couponCode->coupon_id),
                            'message' => $msg
                        ], 200);
                    } else {
                        $msg = 'Coupon not valid for selected day.';
                        return response()->json([
                            'status' => "success",
                            'data' => array('vat_charge' => $vat_charge, 'service_rate' => $service_rate, 'gross_amount' => $gross_amount, 'hour_rate' => $per_hour_rate, 'net_amount' => $total_service_rate, 'discount' => $discount, 'price_to_show' => $price_to_show, 'cleaningrate' => $cleaning_rates, 'serv_rate' => $a_service_rate),
                            'message' => $msg
                        ], 200);
                    }
                }
            } else {
                $msg = 'Coupon expired.';
                return response()->json([
                    'status' => "success",
                    'data' => array('vat_charge' => $vat_charge, 'service_rate' => $service_rate, 'gross_amount' => $gross_amount, 'hour_rate' => $per_hour_rate, 'net_amount' => $total_service_rate, 'discount' => $discount, 'price_to_show' => $price_to_show, 'cleaningrate' => $cleaning_rates, 'serv_rate' => $a_service_rate),
                    'message' => $msg
                ], 200);
            }
        }
    }
    public function calcAvailability($date, $s_hour, $week_day,$service_id)
    {
        $input['date'] = $date;
        $min_slot = "08:00:00";
        $max_slot = "16:00:00";
        $slot_interval = 60; // in minutes
        $warm_up_time = 60; //in minutes
        $min_work_time = 120; // minutes
        $current_time = Carbon::now()->format('Y-m-d H:i:s');
        $response['debug']['current_time'] = $current_time;
        $response['debug']['blocked_times'] = [];
        $response['debug']['past_times'] = [];
        $response['available_times'] = [];
        if (Carbon::createFromFormat('Y-m-d', $input['date'])->format('Y-m-d') < Carbon::now()->format('Y-m-d')) {
            $input['date'] = Carbon::now()->format('Y-m-d');
        }
        for ($i = Carbon::createFromFormat('H:i:s', $min_slot)->addMinutes(-$slot_interval)->format('H:i:s'); $i < $max_slot; $i = Carbon::createFromFormat('H:i:s', $i)->addMinutes($slot_interval)->format('H:i:s')) {
            $slot = Carbon::createFromFormat('H:i:s', $i)->addMinutes($slot_interval)->format('H:i:s');
            $blocked_slot = DB::table('booking_slots as bs')
                ->select(
                    'bs.*',
                )
                ->where(['bs.date' => $input['date'], 'bs.status' => 1])
                ->where(function ($query) use ($slot) { // matching more fields
                    $query->where('bs.from_time', '=', $slot);
                    $query->orWhere([['bs.from_time', '<', $slot], ['bs.to_time', '>', $slot]]);
                })->first();
            if ($blocked_slot) {
                // blocked time slot found
                $response['debug']['blocked_times'][] = Carbon::createFromFormat('H:i:s', $slot)->format('H:i:s');
            } else if (Carbon::createFromFormat('Y-m-d H:i:s', $input['date'] . " " . $slot)->format('Y-m-d H:i:s') >= Carbon::createFromFormat('Y-m-d H:i:s', $current_time)->addMinutes($warm_up_time)->format('Y-m-d H:i:s')) {
                // if > current time - then valid
                $response['available_times'][] = Carbon::createFromFormat('H:i:s', $slot)->format('H:i:s');
            } else {
                // if > current time - then valid
                $response['debug']['past_times'][] = Carbon::createFromFormat('H:i:s', $slot)->format('H:i:s');
            }
        }
        $html = '<div class="clear"></div>';
        $err = 0;
        $allowed_times = [];
        $show_times = [];
        if ($s_hour == 2) {
            $allowed_times = ["08:00:00", "09:00:00", "13:00:00", "14:00:00", "16:00:00"];
        } else if ($s_hour == 3) {
            $allowed_times = ["08:00:00", "09:00:00", "13:00:00", "14:00:00"];
        } else if ($s_hour == 4) {
            $allowed_times = ["08:00:00", "09:00:00", "13:00:00", "14:00:00"];
        } else if ($s_hour == 5) {
            $allowed_times = ["08:00:00", "09:00:00", "13:00:00"];
        } else if ($s_hour == 6) {
            $allowed_times = ["08:00:00", "12:00:00"];
        } else if ($s_hour == 7) {
            $allowed_times = ["08:00:00", "09:00:00", "10:00:00"];
        } else if ($s_hour == 8) {
            $allowed_times = ["08:00:00", "09:00:00"];
        }
        /***************************************** */
        // remove 04:00 PM from services
        $service_ids_remove_4pm = [52, 53, 54, 55, 56, 57, 59, 60, 61, 62];
        if(in_array($service_id, $service_ids_remove_4pm)){
            if (($key = array_search("16:00:00", $allowed_times)) !== false) {
                unset($allowed_times[$key]);
            }
        }
        /***************************************** */
        foreach ($response['available_times'] as $slot) {
            if (in_array($slot, $allowed_times)) {
                $show_times[] = $slot;
                $html .= '<li>';
                $html .= '<div class="tick-mark"></div>';
                $html .= '<div class="tick-text" id="' . $slot . '">' . Carbon::createFromFormat('H:i:s', $slot)->format('g:i A'). '</div>';
                $html .= '<div class="clear"></div></li>';
            }
        }
        /***************************************** */
        //$holiday = DB::table('holidays')->where([['deleted_at', '=', null], ['date', '=', $input['date']]])->first();
        //$weekend = DB::table('week_days')->where([['week_day_id', '=', $week_day], ['weekend', '=', 1]])->first();
        /***************************************** */
        if(!$show_times || @$weekend || @$holiday){
            // empty slots or weekends or holidays ? goto next day
            $input['date'] = Carbon::createFromFormat('Y-m-d',$input['date'])->addDays(1)->format('Y-m-d');
            return PaymentController::calcAvailability($input['date'], $s_hour, date('w', strtotime($input['date'])),$service_id);
        }
        else{
            $html .= '<div class="clear"></div>';
        }
        return array('err' => $err, 'html' => $html, 'selectedDate' => $input['date']);
    }
    public function calcAvailabilityOld($date, $s_hour, $week_day)
    {
        // dump($s_hour);
        $html = "";
        $times = array();
        $current_hour_index = 0;
        $time = '07:00 AM';
        $time_stamp = strtotime($time);
        $err = 0;
        $available_times = [];
        $oneDimensionalArray = [];
        // $available_times = BookingSlots::where('date', $date)->where('status', 1)->pluck('from_time')->toArray();
        $available_timess = BookingSlots::where('date',$date)->where('status',1)->select('from_time','to_time')->get(); 
        // dump($available_timess);die;
        foreach($available_timess as $at){
            $diff=ceil((strtotime($at->to_time) - strtotime($at->from_time))/(60*60));
            $f_time=$at->from_time;
            $t_time=$at->to_time;
            for($i=0;$i<$diff;$i++){
                if($f_time!=$t_time){
                    $available_times[]=$f_time;
                    $f_time=date('H:i:s', strtotime('+60mins', strtotime($f_time)));
                }
            }
        }
		$available_time_new = array();
		if($s_hour == 2)
		{
			$available_time_new = array("10:00:00","11:00:00","12:00:00","15:00:00","17:00:00","18:00:00");
		} else if($s_hour == 3)
		{
			$available_time_new = array("10:00:00","11:00:00","12:00:00","15:00:00","16:00:00","17:00:00","18:00:00");
		} else if($s_hour == 4)
		{
			$available_time_new = array("10:00:00","11:00:00","12:00:00","15:00:00","16:00:00","17:00:00","18:00:00");
		} else if($s_hour == 5)
		{
			$available_time_new = array("10:00:00","11:00:00","12:00:00","14:00:00","15:00:00","16:00:00","17:00:00","18:00:00");
		} else if($s_hour == 6)
		{
			$available_time_new = array("09:00:00","10:00:00","11:00:00","13:00:00","14:00:00","15:00:00","16:00:00","17:00:00","18:00:00");
		} else if($s_hour == 7)
		{
			$available_time_new = array("11:00:00","12:00:00","13:00:00","14:00:00","15:00:00","16:00:00","17:00:00","18:00:00");
		} else if($s_hour == 8)
		{
			$available_time_new = array("10:00:00","11:00:00","12:00:00","13:00:00","14:00:00","15:00:00","16:00:00","17:00:00","18:00:00");
		}
		$available_times = array_unique(array_merge($available_times,$available_time_new), SORT_REGULAR);
		// array_push($available_times,$available_time_new);
        // dump($available_times);die;
        // $html .= '<p>Available time <strong class="selecteddate">'.$request->input('bookedDate').'</strong><span class="hiddenfromtime_error val_errr" style="display:none;"></span></p>';
        for ($i = 0; $i < 10; $i++) {
            $oneDimensionalArray = $available_times;

            $time_stamp = strtotime('+60mins', strtotime($time));
            $timess = date('H:i:s', $time_stamp);
            $time = date('g:i A', $time_stamp);
            // dump($oneDimensionalArray);die;
            if (in_array($timess, $oneDimensionalArray)) {
            } else {
                if ($date == date('Y-m-d')) {
                    $t_shrt = date('H:i:s', strtotime($time));
                    $cur_shrt = date('H:i:s');
                    $hours = ((strtotime($t_shrt) - strtotime($cur_shrt)) / 3600);
                    // dump($hours);
                    if ($hours >= 2) {
                        // dump($hours);
                        $err = 0;
                        $to_time = date('H', strtotime($timess . '+' . $s_hour . ' hour'));
                        // if (((int) $to_time) <= 19 && ((int) $to_time) > 9) {
                        //     $html.= '<li>';
                        //     $html.= '<div class="tick-mark">&nbsp;</div>';
                        //     $html .= '<div class="tick-text" id="'.$timess.'">'.$time.'</div>';
                        //     $html .= '<div class="clear"></div></li>';
                        //     //$html .= '<li data-id="'.$timess.'">'.$time.'</li>';
                        // }
                        // dump($to_time);
                        if ($s_hour == 1) {
                            if (((int) $to_time) <= 18 && ((int) $to_time) > 6) {
                                $html .= '<li>';
                                $html .= '<div class="tick-mark"></div>';
                                $html .= '<div class="tick-text" id="' . $timess . '">' . $time . '</div>';
                                $html .= '<div class="clear"></div></li>';
                            }
                        } else {
                            if (((int) $to_time) <= 18 && ((int) $to_time) > 6) {
                                $html .= '<li>';
                                $html .= '<div class="tick-mark"></div>';
                                $html .= '<div class="tick-text" id="' . $timess . '">' . $time . '</div>';
                                $html .= '<div class="clear"></div></li>';
                            }
                        }
                    } else {
                        $err = 1;
                    }
                } else {
                    $to_time = date('H', strtotime($timess . '+' . $s_hour . ' hour'));
                    // dump($to_time);
                    // if (((int) $to_time) <= 19 && ((int) $to_time) > 9) {
                    //     $html.= '<li>';
                    //     $html.= '<div class="tick-mark"></div>';
                    //     $html.= '<div class="tick-text" id="'.$timess.'">'.$time.'</div>';
                    //     $html.= '<div class="clear"></div></li>';
                    //     $err = 0;
                    // } 
                    if ($s_hour == 1) {
                        if (((int) $to_time) <= 18 && ((int) $to_time) > 6) {
                            $html .= '<li>';
                            $html .= '<div class="tick-mark"></div>';
                            $html .= '<div class="tick-text" id="' . $timess . '">' . $time . '</div>';
                            $html .= '<div class="clear"></div></li>';
                            $err = 0;
                        }
                    } else {
                        if (((int) $to_time) <= 18 && ((int) $to_time) > 6) {
                            $html .= '<li>';
                            $html .= '<div class="tick-mark"></div>';
                            $html .= '<div class="tick-text" id="' . $timess . '">' . $time . '</div>';
                            $html .= '<div class="clear"></div></li>';
                            $err = 0;
                        }
                    }
                    // dump($err);

                }
            }
        }
        // dump($html);

        // if($err == 1)
        // {
        //     $html.='<div class="col-md-12 col-sm-12 comment-box no-left-right-padding">
        //                                     <p id="no-shift-message" class="black">Currently No Shift Available...</p>
        //                                     </div>';
        // }
        // dump($err);
        $html .= '<div class="clear"></div>';
        if ($err == 0) {
            return array('err' => $err, 'html' => $html, 'selectedDate' => $date);
        } else {
            // return array('err'=>$err,'html'=>$html,'selectedDate'=>$date);
            $tomorrow = new DateTime('tomorrow');
            $date = $tomorrow->format('Y-m-d');
            $week_day = date('w', strtotime($date));
            return PaymentController::calcAvailability($date, $s_hour, $week_day);
        }
    }
    public function timeAvailability(Request $request)
    {
        $bookDate = $request['bookedDate'];
        $date = date("Y-m-d", strtotime($bookDate));
        $week_day = date('w', strtotime($date));
        $s_hour = $request['numberHours'] ?: 2;
        $timeslots = PaymentController::calcAvailability($date, $s_hour, $week_day, @$request['service_id']);
        return response()->json([
            'status' => "success",
            'data' => ['html' => $timeslots['html'], 'date' => $timeslots['selectedDate']],
            'message' => 'Time details fetched successfully'
        ], 200,[], JSON_PRETTY_PRINT);
    }
    public function cashPaymentSuccess(Request $request)
    {
        $refId = $request['ref_no'];
        $bookings = Bookings::where('reference_id', $refId)->first();
        $booking = Bookings::find($bookings->booking_id);
        $bookings->pay_by_cash_charge = 0;
        $bookings->pay_by = 'Cash';
        if ($bookings->pay_by == 'Card') {
            //$bookings->total_amount = $bookings->total_amount + 10;
        }
        if($bookings->booking_status == 3 || $bookings->booking_status == 0) {
            $bookings->booking_status = 0; 
        }
        $bookings->save();
        $customer = Customers::where('customer_id', $bookings->customer_id)->first();
        $customerAddress = CustomerAddress::where('customer_id', $bookings->customer_id)->where('default_address',1)->where('address_status',0)->first();
        $services = ServiceType::where('service_type_id', $bookings->service_type_id)->first();
        if ($customerAddress) {
            $area = Area::find($customerAddress->area_id);
            $areaName = $area->area_name;
        } else {
            $areaName = 'NA';
            $customerAddress = '';
        }
        $date = $bookings->service_start_date;
        $old_date_timestamp = strtotime($date);
        $dateFormat = date('l jS, F', $old_date_timestamp);
        $start = date("g:i a", strtotime($bookings->time_from));
        $end = date("g:i a", strtotime($bookings->time_to));
        // $msg = 'Hi admin, you have a new booking (Ref - ' . $refId . ') on ' . $dateFormat . ' with ' . $customer->customer_name . ' at ' . $start . ' - ' . $end . ' for ' . $services->service_type_name . '. Please call ' . $customer->mobile_number_1 . ' for any queries.';
        // LoginController::send_sms('582864783',$msg);
	    if($bookings->booking_type == 'OD') {
            $val = 'One Day';
        } else if($bookings->booking_type == 'WE') {
            $val = 'Weekly';
        } else {
            $val = 'Bi-weekly';
        }
        $dt = new DateTime;
        $userActivity = new UserActivity();
        $userActivity->added_user = (int)1;
        $userActivity->booking_type = $val;
        $userActivity->shift = date("H:i", strtotime($bookings->time_from)).'-'.date("H:i", strtotime($bookings->time_to));
        $userActivity->action_type = 'Booking_add';
        $actionContent = 'A new booking for customer: '.$customer->customer_name.' at '.$start.' - '.$end.' is received from website.';
        $userActivity->action_content = $actionContent;
        $userActivity->addeddate = $dt->format('y-m-d H:i:s');
        $userActivity->date_time_added = $dt->format('y-m-d H:i:s');
        $userActivity->save();
        /******************************************************************* */
        $booking = Bookings::where('reference_id', $refId)->first();
        // get more booking details
        // selected cleaning supplies
        /*$data['service_data']['cleaning_supplies'] = BookingCleaningSupplies::select(
                'cs.name as cleaning_supply_name',
                'booking_cleaning_supplies.unit_rate as cleaning_supply_unit_rate',
                'booking_cleaning_supplies.quantity as cleaning_supply_quantity',
                'booking_cleaning_supplies.total_amount as cleaning_supply_total_amount'
            )
            ->leftJoin('config_cleaning_supplies as cs', 'booking_cleaning_supplies.cleaning_supply_id', 'cs.id')
            ->where('booking_id', $booking->booking_id)->get();*/
        // selected package type data
        /*$data['service_data']['service_mappings'] = BookMapping::select(
                'bookingservicemapping.name',
                'bookingservicemapping.service_cost',
                'bookingservicemapping.quantity',
                'bookingservicemapping.squarefeet_count',
                'sc.service_category_name',
                'ssc.sub_category_name'
            )
            ->leftJoin('servicecategory as sc', 'bookingservicemapping.category_id', 'sc.id')
            ->leftJoin('servicesubcategory as ssc', 'bookingservicemapping.sub_category_id', 'ssc.id')
            ->where('booking_id', $booking->booking_id)->get();*/
        // selected extra services data
        /*$data['service_data']['extra_services'] = BookingExtraServices::select(
                'es.service as extra_service_name',
                'booking_extra_services.duration as extra_service_duration',
                'booking_extra_services.unit_rate as extra_service_unit_rate',
                'booking_extra_services.quantity as extra_service_quantity',
                'booking_extra_services.total_amount as extra_service_total_amount'
            )
            ->leftJoin('extra_services as es', 'booking_extra_services.extra_service_id', 'es.id')
            ->where('booking_id', $booking->booking_id)->get();*/
        // used coupon details
        $data['service_data']['coupon'] = CustomerCoupons::select(
                'customer_coupons.*'
            )
            ->leftJoin('coupon_code as cc', 'customer_coupons.coupon_id', 'cc.coupon_id')
            ->where('customer_coupons.booking_id', $booking->booking_id)->first();
        /******************************************************************* */
        // DON'T SAVE PLZ just show !!!!!!!!!!!!!!
        $bookings->service_charge = $bookings->service_charge * $bookings->no_of_maids;
        $bookings->vat_charge = $bookings->vat_charge * $bookings->no_of_maids;
        $bookings->total_amount = $bookings->total_amount * $bookings->no_of_maids;
        $bookings->discount = $bookings->discount * $bookings->no_of_maids;
        /******************************************************************* */
        return response()->json(
            [
                'status' => 'success',
                'messages' => 'Booking saved successfully.',
                'data' => ['service_data' => $data['service_data'], 'bookings' => $bookings, 'customer' => $customer, 'customer_address' => $customerAddress, 'services' => $services, 'serviceName' => $services->service_type_name, 'areaName' => $areaName],
            ],
            200
        );
    }
    public function paymentSuccess(Request $request)
    {
        $refId = $request['ref_no'];
        $trackId = $request['trackId'];
        $bookings = Bookings::where('reference_id', $refId)->first();
        $bookings->pay_by_cash_charge = 0;
        $online = OnlinePayment::where('reference_id', $refId)->first();
        $status_msg = "refresh_success";
        /*if($online && $online->payment_status != "success") {
            $online->transaction_id = $trackId;
            $online->payment_status = "success";
            $online->save();
            if($bookings->booking_status == 3 || $bookings->booking_status == 0) {
                $bookings->booking_status = 0; 
            }
            $bookings->save();
            $status_msg ="success";
            // save also in customer payments
            $customerPay = new CustomerPayments();
            $customerPay->online_payment_id = $online->payment_id;
            $customerPay->verified_status = 1;
            $customerPay->customer_id = $online->customer_id;
            $customerPay->paid_amount = $online->amount;
            $customerPay->paid_at = 'O';
            if ($online->reference_id == '') {
                $customerPay->ps_no = 'MM-ON/2021/' . $refid;
                //$customerPay->receipt_no = 'DHK-ON/2021/' . $refid;
                
            } elseif ($online->reference_id != '') {
                $customerPay->ps_no = $online->reference_id;
                //$customerPay->receipt_no = $online->reference_id;
            }
            $customerPay->receipt_no = $trackId;
            $customerPay->paid_at_id = 1;
            $customerPay->payment_method = 1;
            $customerPay->day_service_id = 0;
            $customerPay->paid_datetime = date('Y-m-d H:i:s');
            $customerPay->balance_amount = $online->amount;
            $customerPay->save();
        }*/
        $customer = Customers::where('customer_id', $bookings->customer_id)->first();
        $customerAddress = CustomerAddress::where('customer_id', $bookings->customer_id)->where('default_address',1)->where('address_status',0)->first();
        if ($customerAddress) {
            $area = Area::find($customerAddress->area_id);
            $areaName = $area->area_name;
        } else {
            $areaName = 'NA';
            $customerAddress = '';
        }
        $services = ServiceType::where('service_type_id', $bookings->service_type_id)->first();
        $date = $bookings->service_start_date;
        $old_date_timestamp = strtotime($date);
        $dateFormat = date('l jS, F', $old_date_timestamp);
        $start = date("g:i a", strtotime($bookings->time_from));
        $end = date("g:i a", strtotime($bookings->time_to));
        // $msg = 'Hi admin, you have a new booking (Ref - ' . $refId . ') on ' . $dateFormat . ' with ' . $customer->customer_name . ' at ' . $start . ' - ' . $end . ' for ' . $services->service_type_name . '. Please call ' . $customer->mobile_number_1 . ' for any queries.';
        // LoginController::send_sms('582864783',$msg);

	   if($bookings->booking_type == 'OD') {
            $val = 'One Day';
        } else if($bookings->booking_type == 'WE') {
            $val = 'Weekly';
        } else {
            $val = 'Bi-weekly';
        }
        $dt = new DateTime;
        $userActivity = new UserActivity();
        $userActivity->added_user = (int)1;
        $userActivity->booking_type = $val;
        $userActivity->shift = date("H:i", strtotime($bookings->time_from)).'-'.date("H:i", strtotime($bookings->time_to));
        $userActivity->action_type = 'Booking_add';
        $actionContent = 'A new booking for customer: '.$customer->customer_name.' at '.$start.' - '.$end.' is received from website.';
        $userActivity->action_content = $actionContent;
        $userActivity->addeddate = $dt->format('y-m-d H:i:s');
        $userActivity->date_time_added = $dt->format('y-m-d H:i:s');
        $userActivity->save();
        $bookings->save();
        /******************************************************************* */
        $booking = Bookings::where('reference_id', $refId)->first();
        // get more booking details
        // selected cleaning supplies
        /*$data['service_data']['cleaning_supplies'] = BookingCleaningSupplies::select(
                'cs.name as cleaning_supply_name',
                'booking_cleaning_supplies.unit_rate as cleaning_supply_unit_rate',
                'booking_cleaning_supplies.quantity as cleaning_supply_quantity',
                'booking_cleaning_supplies.total_amount as cleaning_supply_total_amount',
            )
            ->leftJoin('config_cleaning_supplies as cs', 'booking_cleaning_supplies.cleaning_supply_id', 'cs.id')
            ->where('booking_id', $booking->booking_id)->get();*/
        // selected package type data
        /*$data['service_data']['service_mappings'] = BookMapping::select(
                'bookingservicemapping.name',
                'bookingservicemapping.service_cost',
                'bookingservicemapping.quantity',
                'bookingservicemapping.squarefeet_count',
                'sc.service_category_name',
                'ssc.sub_category_name',
            )
            ->leftJoin('servicecategory as sc', 'bookingservicemapping.category_id', 'sc.id')
            ->leftJoin('servicesubcategory as ssc', 'bookingservicemapping.sub_category_id', 'ssc.id')
            ->where('booking_id', $booking->booking_id)->get();*/
        // selected extra services data
        /*$data['service_data']['extra_services'] = BookingExtraServices::select(
                'es.service as extra_service_name',
                'booking_extra_services.duration as extra_service_duration',
                'booking_extra_services.unit_rate as extra_service_unit_rate',
                'booking_extra_services.quantity as extra_service_quantity',
                'booking_extra_services.total_amount as extra_service_total_amount',
            )
            ->leftJoin('extra_services as es', 'booking_extra_services.extra_service_id', 'es.id')
            ->where('booking_id', $booking->booking_id)->get();*/
        // used coupon details
        $data['service_data']['coupon'] = CustomerCoupons::select(
                'customer_coupons.*',
            )
            ->leftJoin('coupon_code as cc', 'customer_coupons.coupon_id', 'cc.coupon_id')
            ->where('customer_coupons.booking_id', $booking->booking_id)->first();
        /******************************************************************* */
        // DON'T SAVE PLZ just show !!!!!!!!!!!!!!
        $bookings->service_charge = $bookings->service_charge * $bookings->no_of_maids;
        $bookings->vat_charge = $bookings->vat_charge * $bookings->no_of_maids;
        $bookings->total_amount = $bookings->total_amount * $bookings->no_of_maids;
        $bookings->discount = $bookings->discount * $bookings->no_of_maids;
        /******************************************************************* */
        return response()->json(
            [
                'status' => $status_msg,
                'messages' => 'Booking saved successfully.',
                'data' => ['service_data' => $data['service_data'],'bookings' => $bookings, 'customer' => $customer, 'customer_address' => $customerAddress, 'services' => $services, 'serviceName' => $services->service_type_name, 'areaName' => $areaName, 'online' => $online],
            ],
            200
        );
    }
	public function paymentTamaraSuccess(Request $request)
    {
        $trackId = $request['trackId'];
        $online = OnlinePayment::where('transaction_id', $trackId)->first();
        $status_msg = "refresh_success";
        if($online && $online->payment_status != "success") {
			$bookings = Bookings::where('reference_id', $online->reference_id)->first();
            $online->transaction_id = $trackId;
            $online->payment_status = "success";
            $online->save();
            if($bookings->booking_status == 3 || $bookings->booking_status == 0) {
                $bookings->booking_status = 0; 
            }
        
            $bookings->save();
            $status_msg ="success";
        }
        $customer = Customers::where('customer_id', $bookings->customer_id)->first();
        $customerAddress = CustomerAddress::where('customer_id', $bookings->customer_id)->where('default_address',1)->where('address_status',0)->first();
        if ($customerAddress) {
            $area = Area::find($customerAddress->area_id);
            $areaName = $area->area_name;
        } else {
            $areaName = 'NA';
            $customerAddress = '';
        }
        $services = ServiceType::where('service_type_id', $bookings->service_type_id)->first();
        $date = $bookings->service_start_date;
        $old_date_timestamp = strtotime($date);
        $dateFormat = date('l jS, F', $old_date_timestamp);
        $start = date("g:i a", strtotime($bookings->time_from));
        $end = date("g:i a", strtotime($bookings->time_to));
        // $msg = 'Hi admin, you have a new booking (Ref - ' . $online->reference_id . ') on ' . $dateFormat . ' with ' . $customer->customer_name . ' at ' . $start . ' - ' . $end . ' for ' . $services->service_type_name . '. Please call ' . $customer->mobile_number_1 . ' for any queries.';
        // LoginController::send_sms('582864783',$msg);

	   if($bookings->booking_type == 'OD') {
            $val = 'One Day';
        } else if($bookings->booking_type == 'WE') {
            $val = 'Weekly';
        } else {
            $val = 'Bi-weekly';
        }
        $dt = new DateTime;
        $userActivity = new UserActivity();
        $userActivity->added_user = (int)1;
        $userActivity->booking_type = $val;
        $userActivity->shift = date("H:i", strtotime($bookings->time_from)).'-'.date("H:i", strtotime($bookings->time_to));
        $userActivity->action_type = 'Booking_add';
        $actionContent = 'A new booking for customer: '.$customer->customer_name.' at '.$start.' - '.$end.' is received from website.';
        $userActivity->action_content = $actionContent;
        $userActivity->addeddate = $dt->format('y-m-d H:i:s');
        $userActivity->date_time_added = $dt->format('y-m-d H:i:s');
        $userActivity->save();
        return response()->json(
            [
                'status' => $status_msg,
                'messages' => 'Booking saved successfully.',
                'data' => ['bookings' => $bookings, 'customer' => $customer, 'customer_address' => $customerAddress, 'services' => $services, 'serviceName' => $services->service_type_name, 'areaName' => $areaName, 'online' => $online],
            ],
            200
        );
    }
    public function paymentFailed(Request $request)
    {
        $refId = $request['ref_no'];
        $trackId = $request['trackId'];
        //$bookings = Bookings::where('reference_id', $refId)->first();
        $bookings = Bookings::where(function ($q) use ($refId) {
            $q->where('reference_id', $refId)->orWhere('booking_id', $refId);
        })->first();
        //$online = OnlinePayment::where('reference_id', $refId)->first();
        $online = OnlinePayment::where(function ($q) use ($refId) {
            $q->where('reference_id', $refId)->orWhere('booking_id', $refId);
        })->first();
        $online->transaction_id = $trackId ?: null;
        $online->payment_status = 'failed';
        $online->save();
        $customer = Customers::where('customer_id', $bookings->customer_id)->first();
        $customerAddress = CustomerAddress::where('customer_id', $bookings->customer_id)->where('default_address',1)->where('address_status',0)->first();
        $services = ServiceType::where('service_type_id', $bookings->service_type_id)->first();
        if ($customerAddress) {
            $area = Area::find($customerAddress->area_id);
            $areaName = $area->area_name;
        } else {
            $areaName = 'NA';
            $customerAddress = '';
        }
        return response()->json(
            [
                'status' => 'success',
                'messages' => 'Booking failed.',
                'data' => ['bookings' => $bookings, 'customer' => $customer, 'customer_address' => $customerAddress, 'services' => $services, 'serviceName' => $services->service_type_name, 'areaName' => $areaName, 'online' => $online],
            ],
            200
        );
    }
	
	public function tamaraPaymentFailed(Request $request)
    {
        $trackId = $request['trackId'];
        $online = OnlinePayment::where('transaction_id', $trackId)->first();
        $online->payment_status = $request['status'];
        $online->save();
		
		$bookings = Bookings::where('reference_id', $online->reference_id)->first();
        $customer = Customers::where('customer_id', $bookings->customer_id)->first();
        $customerAddress = CustomerAddress::where('customer_address_id', $bookings->customer_address_id)->where('address_status',0)->first();
        $services = ServiceType::where('service_type_id', $bookings->service_type_id)->first();
        if ($customerAddress) {
            $area = Area::find($customerAddress->area_id);
            $areaName = $area->area_name;
        } else {
            $areaName = 'NA';
            $customerAddress = '';
        }
        return response()->json(
            [
                'status' => 'success',
                'messages' => 'Booking failed.',
                'data' => ['bookings' => $bookings, 'customer' => $customer, 'customer_address' => $customerAddress, 'services' => $services, 'serviceName' => $services->service_type_name, 'areaName' => $areaName, 'online' => $online],
            ],
            200
        );
    }
	
    public function saveOnlinePay(Request $request)
    {

        $customer_id = $request['customerId'];
        $description = $request['description'];
        $amount = $request['amount'];


        $data = array();
        if ($customer_id != "") {

            $data_post = serialize($_POST);

            // $this->load->library('user_agent');
            // if ($this->agent->is_browser())
            // {
            //     $agent = $this->agent->browser().' '.$this->agent->version();
            // }
            // elseif ($this->agent->is_robot())
            // {
            //     $agent = $this->agent->robot();
            // }
            // elseif ($this->agent->is_mobile())
            // {
            //     $agent = $this->agent->mobile();
            // }
            // else
            // {
            $agent = 'Unidentified User Agent';
            // }

            // $transaction_charg = $amount * 0.03;
            $transaction_charg = 0;
            $transaction_charge = number_format((float)$transaction_charg, 2, '.', '');
			//$transaction_charge = 0;



            $onlinePayment = new OnlinePayment();
            $onlinePayment->description = $description;
            $onlinePayment->transaction_charge = $transaction_charge;
            $onlinePayment->amount = $amount;
            $onlinePayment->customer_id = $customer_id;
            $onlinePayment->paid_from = 'O';
            $onlinePayment->payment_datetime = date('Y-m-d H:i:s');
            $onlinePayment->user_agent = $agent;
            $onlinePayment->post_data = $data_post;
            $onlinePayment->ip_address = '';
            $onlinePayment->payment_status = 'initiated';
            $onlinePayment->save();

            $customer = Customers::find($customer_id);
            $customer_address_id = $customer->customer_address_id;
            $customerAddress = CustomerAddress::where('customer_id', $customer_id)->where('default_address',1)->where('address_status',0)->first();
            if ($customerAddress) {
                $address = $customerAddress->customer_address;
                $area = Area::find($customerAddress->area_id);
                $areaName = $area->area_name;
            } else {
                $address = '';
                $areaName = 'NA';
            }
            $customer_name = $customer->customer_name;
            $customer_mobile = $customer->mobile_number_1;
            $email_id = $customer->email_address;
            $data['customer_name'] = $customer_name;
            $data['customer_email'] = $email_id;
            $data['description'] = $description;
            $data['amount'] = $amount;
            $data['transaction_charge'] = $transaction_charge;
            $data['gross_amount'] = number_format((float)($amount + $transaction_charge), 2, '.', '');
            $data['phone_number'] = $customer_mobile;
            $data['address'] = $address;
            $data['area'] = $areaName;
            $data['pay_details'] = $onlinePayment;
            $data['reference_id'] = '';
            return response()->json(
                [
                    'status' => 'success',
                    'messages' => 'Payment details added.',
                    'data' => $data,
                ],
                200
            );
        } else {
            return response()->json(
                [
                    'status' => 'failed',
                    'messages' => 'Customer not found.',
                    'data' => null,
                ]
            );
        }
    }
    public function onlinePaymentSuccess(Request $request)
    {
        $payId = $request['payId'];
        $trackId = $request['trackId'];
        $refid = (1000 + $payId);
        $status_msg = "refresh_success";
        $online = OnlinePayment::where('payment_id', $payId)->first();
        if($online && $online->payment_status !='success') {
            if ($online->reference_id == '') {
                $online->reference_id = 'DHK-ON/2021/' . $refid;
            } elseif ($online->reference_id != '') {
                $bookings = Bookings::where('reference_id', $online->reference_id)->first();
                if($bookings) {
                    if($bookings->booking_status == 3 || $bookings->booking_status == 0) {
                        $bookings->booking_status = 0; 
                    }
                
                    $bookings->save();
                }
            }
            $online->payment_status = "success";
            $online->transaction_id = $trackId;
            $online->save();
            $customerPay = new CustomerPayments();
            $customerPay->verified_status = 1;
            $customerPay->customer_id = $online->customer_id;
            $customerPay->paid_amount = $online->amount;
            $customerPay->paid_at = 'O';
            if ($online->reference_id == '') {
                $customerPay->ps_no = 'DHK-ON/2021/' . $refid;
                $customerPay->receipt_no = 'DHK-ON/2021/' . $refid;
            } elseif ($online->reference_id != '') {
                $customerPay->ps_no = $online->reference_id;
                $customerPay->receipt_no = $online->reference_id;
            }
            $customerPay->paid_at_id = 1;
            $customerPay->payment_method = 1;
            $customerPay->day_service_id = 0;
            $customerPay->paid_datetime = date('Y-m-d H:i:s');
            $customerPay->save();
            $status_msg = "success";
        }
            
        $customer = Customers::where('customer_id', $online->customer_id)->first();
        $customerAddress = CustomerAddress::where('customer_id', $online->customer_id)->where('default_address',1)->where('address_status',0)->first();
        if ($customerAddress) {
            $area = Area::find($customerAddress->area_id);
            $areaName = $area->area_name;
        } else {
            $areaName = 'NA';
            $customerAddress = '';
        }

        // Mail::send(new SuccessMail($customer->email_address,$customer,$bookings,$customerAddress,$areaName,$services,$online));
        // Mail::send(new SuccessMailAdmin($customer->email_address,$customer,$bookings,$customerAddress,$areaName,$services,$online));
        return response()->json(
            [
                'status' => $status_msg,
                'messages' => 'Booking saved successfully.',
                'data' => ['customer' => $customer, 'customer_address' => $customerAddress, 'serviceName' => 'Other Payments', 'areaName' => $areaName, 'paymentData' => $online],
            ],
            200
        );
    }
    public function onlinePaymentFailed(Request $request)
    {
        $payId = $request['payId'];
        $trackId = $request['trackId'];
        $online = OnlinePayment::where('payment_id', $payId)->first();
        if ($online->reference_id == '') {
	    $reference_id =(1000 + $payId);
            $online->reference_id = 'DHK-ON/2021/'.$reference_id;
        }
        $online->transaction_id = $trackId;
        $online->payment_status = 'failed';
        $online->save();
        $customer = Customers::where('customer_id', $online->customer_id)->first();
        $customerAddress = CustomerAddress::where('customer_id', $online->customer_id)->where('default_address',1)->where('address_status',0)->first();
        if ($customerAddress) {
            $area = Area::find($customerAddress->area_id);
            $areaName = $area->area_name;
        } else {
            $areaName = 'NA';
            $customerAddress = '';
        }

        return response()->json(
            [
                'status' => 'success',
                'messages' => 'Booking failed.',
                'data' => ['customer' => $customer, 'customer_address' => $customerAddress, 'serviceName' => 'Other Payments', 'areaName' => $areaName, 'paymentData' => $online],
            ],
            200
        );
    }
    public function saveBookinglistPay(Request $request)
    {
        $data = array();
		
        $bookings = Bookings::where('booking_id', $request['bookingId'])->first();
        $amount = $bookings->total_amount * $bookings->no_of_maids;
        // $transaction_charg = $amount * 0.03;
        $transaction_charg = 0;
        $transaction_charge = number_format((float)$transaction_charg * $bookings->no_of_maids, 2, '.', '');
        $bookings->pay_by_cash_charge = 0;
        if ($bookings->pay_by == 'Cash' || ($bookings->pay_by == 'Card' && $bookings->booked_from == 'A' )) {
            
            if($bookings->booked_from=='W' || $bookings->booked_from=='M') {
                //$amount = $amount - 10;
            }
            $bookings->pay_by = 'Card';
            //$bookings->total_amount = $amount;
            $bookings->save();
            $agent = 'Unidentified User Agent';
            $onlinePayment = new OnlinePayment();
            $onlinePayment->description = '';
            $onlinePayment->booking_id = $bookings->booking_id;
            $onlinePayment->reference_id = $bookings->reference_id;
            $onlinePayment->transaction_charge = $transaction_charge;
            $onlinePayment->amount = $amount;
            $onlinePayment->customer_id = $bookings->customer_id;
           if($request['is_device']=='mobile') {
		        $onlinePayment->paid_from = 'M';
            } else if($request['is_device']=='web') {
                $onlinePayment->paid_from = 'W';
            }
            $onlinePayment->payment_datetime = date('Y-m-d H:i:s');
            $onlinePayment->user_agent = $agent;
            $onlinePayment->post_data = '';
            $onlinePayment->ip_address = '';
            $onlinePayment->payment_status = 'initiated';
            $onlinePayment->save();
        }
        //$onlinePayment = OnlinePayment::where('reference_id', $bookings->reference_id)->first();

        $onlinePayment = OnlinePayment::where(function ($q) use ($bookings) {
            $q->where('reference_id', $bookings->reference_id)->orWhere('booking_id', $bookings->booking_id);
        })->first();


        $customer = Customers::find($bookings->customer_id);
        $customer_address_id = $customer->customer_address_id;
        $customerAddress = CustomerAddress::where('customer_id', $customer->customer_id)->where('default_address',1)->where('address_status',0)->first();
        $customer_name = $customer->customer_name;
        $customer_mobile = $customer->mobile_number_1;
        $email_id = $customer->email_address;
        if ($customerAddress) {
            $address = $customerAddress->customer_address;
            $area = Area::find($customerAddress->area_id);
            $areaName = $area->area_name;
        } else {
            $address = 'NA';
            $areaName = 'NA';
        }


        $data['customer_name'] = $customer_name;
        $data['customer_email'] = $email_id;
        $data['amount'] = $amount;
        $data['transaction_charge'] = $transaction_charge;
        $data['gross_amount'] = number_format((float)$amount, 2, '.', '');
        $data['phone_number'] = $customer_mobile;
        $data['address'] = $address;
        $data['area'] = $areaName;
        $data['reference_id'] = $bookings->reference_id;
        $data['payment_id'] = $onlinePayment->payment_id;
        $data['bookings'] = $bookings;
        $data['customer'] = $customer;
        $data['customer_address'] = $customerAddress;
        $bookings->save();
        if($bookings->pay_by == "Cash"){
            $onlinePayment->transaction_charge = 10;
        }
        elseif($bookings->pay_by == "Card"){
            $onlinePayment->transaction_charge = $onlinePayment->amount * .03;
        }
        $onlinePayment->save();
        return response()->json(
            [
                'status' => 'success',
                'messages' => 'Payment details added.',
                'data' => $data,
            ],
            200
        );
    }
    public function cancelBooking(Request $request)
    {
        $cancelBooking = Bookings::find($request['bookingId']);
        $cancelBooking->is_cancelled = 'yes';
        $cancelBooking->booking_status = 2;
        $cancelBooking->cancel_date = date('Y-m-d H:i:s');
        $cancelBooking->save();
        // $BookingCancel = new BookingCancel();
        // $BookingCancel->booking_id = $request['bookingId'];
        // $BookingCancel->service_date = $cancelBooking->service_start_date;
        // $BookingCancel->deleted_by = $request['customerId'];
        // $BookingCancel->added_datetime =  date('Y-m-d H:i:s');
        // $BookingCancel->save();
        $customer = Customers::find($cancelBooking->customer_id);
        $customer_address_id = $customer->customer_address_id;
        $customerAddress = CustomerAddress::where('customer_id', $customer->customer_id)->where('default_address',1)->where('address_status',0)->first();
        if ($customerAddress) {
            $address = $customerAddress->customer_address;
            $area = Area::find($customerAddress->area_id);
            $areaName = $area->area_name;
        } else {
            $address = 'NA';
            $areaName = 'NA';
        }
        $customer_name = $customer->customer_name;
        $customer_mobile = $customer->mobile_number_1;
        $email_id = $customer->email_address;

        $data = array();
        $data['customer_name'] = $customer_name;
        $data['customer_email'] = $email_id;
        $data['phone_number'] = $customer_mobile;
        $data['address'] = $address;
        $data['area'] = $areaName;
        $data['reference_id'] = $cancelBooking->reference_id;
        $data['time_from'] = $cancelBooking->time_from;
        $data['service_start_date'] = $cancelBooking->service_start_date;
        $data['time_to'] = $cancelBooking->time_to;
        $data['pay_by'] = $cancelBooking->pay_by;
        /**************************************** */
        $mail = new AdminApiMailController;
        $mail->schedule_cancel_to_customer($request['bookingId'], $cancelBooking->service_start_date);
        //$mail->schedule_cancel_to_admin($request['bookingId'], $cancelBooking->service_start_date);
        return response()->json(
            [
                'status' => 'success',
                'messages' => 'Booking cancelled successfully.',
                'data' => $data,
            ],
            200
        );
    }
    public function cancelBookingNew(Request $request)
    {
        $cancelBooking = Bookings::find($request['bookingId']);
        $cancelBooking->is_cancelled = 'yes';  
        $cancelBooking->cancel_reason = $request['cancelReason'];  
        $cancelBooking->cancel_comments = $request['cancelcomments'];  
        $cancelBooking->booking_status = 2;
        $cancelBooking->cancel_date = date('Y-m-d H:i:s');
        $cancelBooking->save();
        // $BookingCancel = new BookingCancel();
        // $BookingCancel->booking_id = $request['bookingId'];
        // $BookingCancel->service_date = $cancelBooking->service_start_date;
        // $BookingCancel->deleted_by = $request['customerId'];
        // $BookingCancel->added_datetime =  date('Y-m-d H:i:s');
        // $BookingCancel->save();
        $customer = Customers::find($cancelBooking->customer_id);
        $customer_address_id = $customer->customer_address_id;
        $customerAddress = CustomerAddress::where('customer_id',$customer->customer_id)->where('default_address',1)->where('address_status',0)->first();
        if($customerAddress) {
            $address = $customerAddress->customer_address;
            $area = Area::find($customerAddress->area_id);
            $areaName= $area->area_name; 
        } else {
            $address ='NA';
            $areaName = 'NA';
        }
        $customer_name = $customer->customer_name;
        $customer_mobile = $customer->mobile_number_1;
        $email_id = $customer->email_address; 
        $data=array();   
        $data['customer_name']=$customer_name;
        $data['customer_email']=$email_id;
        $data['phone_number']=$customer_mobile;
        $data['address']=$address;
        $data['area']=$areaName;
        $data['reference_id'] = $cancelBooking->reference_id;
        $data['time_from']=$cancelBooking->time_from;
        $data['service_start_date']=$cancelBooking->service_start_date;
        $data['time_to'] = $cancelBooking->time_to;
        $data['pay_by']=$cancelBooking->pay_by;
        return response()->json(
            [
                'status' => 'success',
                'messages' => 'Booking cancelled successfully.',
                'data' =>$data,
            ],
            200
        ); 
    }
	
	public function tamaraHookSuccess(Request $request)
    {
		$orderId = $request['order_id'];
		$order_reference_id = $request['order_reference_id'];
		
		$secret_key="eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhY2NvdW50SWQiOiJkNTJjMjMzZC05MmFjLTRlMTYtYTc1My0yNTc3ZWZjY2Q1YmYiLCJ0eXBlIjoibWVyY2hhbnQiLCJzYWx0IjoiZjQzOWE1YTVjNzY3ZTgzZTdmOGEwNzgyYTBkYzVkMTgiLCJpYXQiOjE2NTU5ODk3ODAsImlzcyI6IlRhbWFyYSJ9.LIIEkaOTy_quWbdNrb8g0F2gBI3O3GC29jjI-FDK904IFGDslIT7-JlGYqhffd6YY3FeNCYFEuKwSQS-yFwwmufH-Rjs95GxRNPWPkwrwXCzxuqyzxfxTMbcUxJ6O3FFKgZ3Hzl3NG9Rt0rDkr9vJIHrx8zMscJiCnMR-dRDLJYvyJXoJrrM3j1XsqiT1Y0JFm2eNXVjnPgvN1PsMHCaSBX3n3BNfgcFM3gUQNA1wAQg5YUKLOWFpSNLKpPXaEOy7cmmVGhi1yVzhAcGa_IK-Wfh9imFsqYqb6QQ-oQ2ZS3WhiNjeO0KX_LusYcMIyRh0luf5HLDCENFNq99a3K3NA";
		$Headers  = array("Authorization: Bearer ".$secret_key, "Content-Type: application/json", "Accept: application/json");
		$payServiceURL="https://api-sandbox.tamara.co/orders/".$orderId."/authorise";
		$postdata = array();
		$availabilitycheck = $this->execFunc("POST", $payServiceURL, $postdata, $Headers);
		
		if(!empty($availabilitycheck['data']))
		{
			if($availabilitycheck['data']->status == 'authorised')
			{
				$trackId = $request['order_id'];
				$online = OnlinePayment::where('transaction_id', $trackId)->first();
				if($online && ($online->payment_status != "success" || $online->payment_status != "authorised")) {
					$bookings = Bookings::where('reference_id', $online->reference_id)->first();
					$online->transaction_id = $trackId;
					$online->payment_status = "authorised";
					$online->save();
					if($bookings->booking_status == 3 || $bookings->booking_status == 0) {
						$bookings->booking_status = 0; 
					}
				
					$bookings->save();
				}
				return response()->json(
					[
						'status' => 'success',
						'messages' => 'Booking saved successfully.',
						'data' => ['bookings' => $bookings, 'online' => $online],
					],
					200
				);
			} else {
				return response()->json(
					[
						'status' => 'failed',
						'messages' => 'Try again.',
					]
				);
				exit();
			}
		} else {
			return response()->json(
				[
					'status' => 'failed',
					'messages' => 'Try again.',
				]
			);
			exit();
		}
    }
	
	public static function execFunc($method, $url, $obj = array(),$headers) {
     
    $curl = curl_init();
     
    switch($method) {
      case 'GET':
        if(strrpos($url, "?") === FALSE) {
          $url .= '?' . http_build_query($obj);
        }
        break;

      case 'POST': 
        curl_setopt($curl, CURLOPT_POST, TRUE);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($obj));
        break;

      case 'PUT':
      case 'DELETE':
      default:
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, strtoupper($method)); // method
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($obj)); // body
    }

    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers); 
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HEADER, TRUE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, TRUE);
    
    // Exec
    $response = curl_exec($curl);
    $info = curl_getinfo($curl);
    curl_close($curl);
    
    // Data
    $header = trim(substr($response, 0, $info['header_size']));
    $body = substr($response, $info['header_size']);
     
    return array('data' => json_decode($body));
  }
}
