<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingExtraServices extends Model
{
    protected $table = 'booking_extra_services';
    public $timestamps = false;
    protected $primaryKey = 'id';
}
