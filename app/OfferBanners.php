<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfferBanners extends Model
{
    protected $table = 'offer_banners';
    public $timestamps = false;
    protected $primaryKey = 'id';

}
