<?php
    /**
     * Author: Samnad. S
     * Date: 04/01/2023
     */
namespace App;
use Illuminate\Database\Eloquent\Model;

class SubscriptionPackage extends Model
{
   protected $table = 'package';
    public $timestamps = false;
    protected $primaryKey = 'package_id';
}
