<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bookings extends Model
{
    protected $table = 'bookings';
    public $timestamps = false;
    protected $primaryKey = 'booking_id';
}