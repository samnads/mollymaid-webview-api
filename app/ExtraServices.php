<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExtraServices extends Model
{
    protected $table = 'extra_services';
}
