<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceType extends Model
{
    protected $table = 'service_types';
    public $timestamps = false;

    protected $primaryKey = 'service_type_id';
    public function getCategory() {
		return $this->hasMany('App\ServiceCategory','service_type_id','service_type_id');
    }
    public function getCost() {
		return $this->hasMany('App\ServiceCategoryCost','service_id','service_type_id');
	}
}
