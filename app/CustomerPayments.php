<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerPayments extends Model
{
    protected $table = 'customer_payments';
    protected $primaryKey = 'payment_id';

    public $timestamps = false;
}
