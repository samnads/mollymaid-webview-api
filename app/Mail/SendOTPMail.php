<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Config;

class SendOTPMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

     public $name;
     public $email;
     public $msg;
    public function __construct($email,$message,$name)
    {
        $this->name=$name;
        $this->email=$email;
        $this->msg=$message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(Config::get('values.to_mail'), 'OTP')
            ->to($this->email,@$this->name)
            ->subject('OTP')
            ->view('emails.sendOTP');  
    }
}
