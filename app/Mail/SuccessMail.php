<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Config;

class SuccessMail extends Mailable
{
    use Queueable, SerializesModels;
    public $email;
    public $customer;
    public $bookings;
    public $customerAddress;
    public $areaName;
    public $services;
    public $online;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email,$customer,$bookings,$customerAddress,$areaName,$services,$online)
    {
        $this->email = $email;
        $this->customer = $customer;
        $this->bookings = $bookings;
        $this->customerAddress = $customerAddress;
        $this->areaName = $areaName;
        $this->services = $services;
        $this->online = $online;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mailSubject = 'Cleaning Booking Confirmation - '.$this->bookings['reference_id'];
        return $this->from(Config::get('values.to_mail'), 'Dubai Housekeeping')
            // ->replyTo('test1@gmail.com', 'EMAID')
            ->to($this->email,@$this->customer['customer_name'])
            // ->to('karthika.prasad@azinova.info',@$this->name)

            ->subject($mailSubject)
            ->view('emails.booking_confirmation_template_email');  
    }
}
