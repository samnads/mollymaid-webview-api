<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Config;


class ForgotPassword extends Mailable
{
    use Queueable, SerializesModels;
    public $email;
    public $password;
    public $name;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email,$password,$name)
    {
        $this->email = $email;
        $this->password = $password;
        $this->name = $name;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mailSubject = 'Password Recovery - Dubai Housekeeping Account Password';
        return $this->from(Config::get('values.to_mail'), 'Dubai Housekeeping')
            // ->replyTo('test1@gmail.com', 'EMAID')
            ->to($this->email,@$this->name)
            // ->to('karthika.prasad@azinova.info',@$this->name)
            ->subject($mailSubject)
            ->view('emails.forgot_password');    
    }
}
