<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingSlots extends Model
{
    protected $table = 'booking_slots';
}
