<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {

    public $layout = 'static_page_layout';

    public function __construct() {
        parent::__construct();
		$this->load->helper('url', 'form');

    }

    public function about() {
        $data               = array();
        $data['css_files']  = array();
        $data['js_files']   = array();
        $data['meta_title'] = '';
        $data['meta_description'] = '';
		$data['meta_keywords'] = '';

        $this->load->view('pages/about', $data);
    }

    public function service() {
       $data                = array();
        $data['css_files']  = array();
        $data['js_files']   = array();
        $data['meta_title'] = '';
        $data['meta_description'] = '';
		$data['meta_keywords'] = '';

        $this->load->view('pages/service', $data);
    }
	
	public function cleaning_service() {
       $data                = array();
        $data['css_files']  = array();
        $data['js_files']   = array();
        $data['meta_title'] = 'Maid Cleaning Company Dubai | Part Time Maid Service Dubai';
        $data['meta_description'] = 'Premium cleaning company in Dubai utilizing part time maids to dispense effective maid cleaning services across Dubai, UAE affordably.';
		$data['meta_keywords'] = 'maid service dubai,cleaning company dubai,maid cleaning service Dubai,maid company dubai,cleaning dubai';

        $this->load->view('pages/cleaning_service', $data);
    }
	
	public function maintenance_service() {
       $data                = array();
        $data['css_files']  = array();
        $data['js_files']   = array();
        $data['meta_title'] = 'Maintenance Companies Dubai| Best Home Maintenance Services';
        $data['meta_description'] = 'The foremost among maintenance companies that can deliver maintenance services to homes and villas across Dubai, UAE at affordable rates.';
		$data['meta_keywords'] = 'maintenance companies in dubai, home maintenance services dubai,villa maintenance companies in dubai,best maintenance companies in dubai,maintenance services dubai ';

        $this->load->view('pages/maintenance_service', $data);
    }

    public function contact() {
       $data                = array();
        $data['css_files']  = array();
        $data['js_files']   = array();
        $data['meta_title'] = '';
        $data['meta_description'] = '';
		$data['meta_keywords'] = '';

        $this->load->view('pages/contact', $data);
    }
    public function houseCleaning() {
        $this->layout = "";
       $data                = array();
        $data['css_files']  = array();
        $data['js_files']   = array();
        $data['meta_title'] = 'House Cleaning Services Dubai | Part Time Housemaid Dubai';
        $data['meta_description'] = 'House cleaning services in Dubai with effective housemaid working part time at hourly rate to make your house look beautiful using effective cleaning services.';
		$data['meta_keywords'] = 'house cleaning dubai,house cleaning services,Part time housemaid in dubai,house cleaning services dubai';
         $this->load->view('layouts/header', $data);
        $this->load->view('layouts/inner-banner');
        $this->load->view('pages/house_cleaning', $data);
        $this->load->view('layouts/footer_maid_cleaning');
    }
    public function moveIn() {
       $data                = array();
        $data['css_files']  = array();
        $data['js_files']   = array();
        $data['meta_title'] = 'Move In/Out Cleaning Service Dubai | Deep Cleaning Dubai';
        $data['meta_description'] = 'Move out and move-in cleaning service that cleans your homes after you move out and before you move in with effective cleaning that makes the location spotless.';
		$data['meta_keywords'] = 'move in cleaning dubai,move out cleaning service,move-in/out deep cleaning,move out cleaning prices,moving out cleaning services';

        $this->load->view('pages/move_in', $data);
    }
    public function windowCleaning() {
       $data                = array();
        $data['css_files']  = array();
        $data['js_files']   = array();
        $data['meta_title'] = 'Residential Window Cleaning Dubai | Window Cleaner Dubai';
        $data['meta_description'] = 'Professional window cleaners and window cleaning services for your residence, effective cleaning service to your windows to make them shine.';
		$data['meta_keywords'] = 'window cleaning ,window cleaner,professional window cleaning , residential window cleaning';

        $this->load->view('pages/window_cleaning', $data);
    }
    public function partyCleaning() {
       $data                = array();
        $data['css_files']  = array();
        $data['js_files']   = array();
        $data['meta_title'] = 'After Party Cleaning Services Dubai | Party Cleaners';
        $data['meta_description'] = 'Party and after party cleaning services that helps you setup the party and cleans all the mess after the party using maids with effective cleaning services.';
		$data['meta_keywords'] = 'after party cleaning,after party cleaning services,party cleaning services,party clean up service,party cleaners';

        $this->load->view('pages/party_cleaning', $data);
    }
    public function floorCleaning() {
       $data                = array();
        $data['css_files']  = array();
        $data['js_files']   = array();
        $data['meta_title'] = 'Professional Floor Cleaners Dubai | Floor Cleaning Services';
        $data['meta_description'] = 'Floor cleaning services done by professional floor cleaners, who will provide floor cleaning services on all types of floor materials at an affordable rate.';
		$data['meta_keywords'] = 'floor cleaner,floor cleaning services,professional floor cleaners';

        $this->load->view('pages/floor_cleaning', $data);
    }
    public function regularCleaning() {
       $data                = array();
        $data['css_files']  = array();
        $data['js_files']   = array();
        $data['meta_title'] = '';
        $data['meta_description'] = '';
		$data['meta_keywords'] = '';

        $this->load->view('pages/regular_cleaning', $data);
    }
    public function babySitting() {
       $data                = array();
        $data['css_files']  = array();
        $data['js_files']   = array();
        $data['meta_title'] = 'Nanny Service Dubai | Babysitting/Babysitter Services Dubai';
        $data['meta_description'] = 'Babysitting services and nanny services in Dubai that nurtures and takes care of your child through professional babysitters and nannies in Dubai, UAE.';
		$data['meta_keywords'] = 'babysitting dubai,nanny in dubai,baby sitter dubai,babysitter in dubai,babysitting services dubai,babysitting in dubai';

        $this->load->view('pages/baby_sitting', $data);
    }
    public function painting() {
		$this->layout = "";
       $data                = array();
        $data['css_files']  = array();
        $data['js_files']   = array();
        $data['meta_title'] = 'Painters In Dubai | Painting Services Dubai';
        $data['meta_description'] = 'Painting services in Dubai – providing wholesome painting service for residents across Dubai, painters that are experts and experienced at paint jobs.';
		$data['meta_keywords'] = 'painting services dubai,painters in dubai,painting companies in dubai';
		$this->load->view('layouts/header', $data);
		$this->load->view('layouts/inner-banner');
        $this->load->view('pages/painting', $data);
		$this->load->view('layouts/footer_painting');
    }
    public function acService() {
       $data                = array();
        $data['css_files']  = array();
        $data['js_files']   = array();
        $data['meta_title'] = 'AC Repair Dubai | AC Maintenance Services & Repair Services';
        $data['meta_description'] = 'AC Maintenance services in Dubai providing affordable and quick repairs for all kinds of ACs in Dubai, UAE with the expert maintenance technicians.';
		$data['meta_keywords'] = 'ac repair dubai,ac maintenance dubai,ac repair dubai';

        $this->load->view('pages/ac_service', $data);
    }
    public function plumbing() {
       $data                = array();
        $data['css_files']  = array();
        $data['js_files']   = array();
        $data['meta_title'] = 'Plumber Dubai | Plumbing Services Dubai';
        $data['meta_description'] = 'Plumbing services in Dubai, the best among plumbing companies who can deliver emergency plumbers at short notice anywhere around Dubai, UAE.';
		$data['meta_keywords'] = 'plumber dubai,Plumbing Services Dubai,plumbing companies in dubai ,emergency plumber dubai';

        $this->load->view('pages/plumbing', $data);
    }
    public function electricalServices() {
       $data                = array();
        $data['css_files']  = array();
        $data['js_files']   = array();
        $data['meta_title'] = 'Electrical Services Dubai | Electric Repair Service';
        $data['meta_description'] = 'Electrical services in Dubai – affordable electric service providing quick and easy electrical maintenance for residents across Dubai, UAE';
		$data['meta_keywords'] = 'Electrical Services Dubai, electric service company,Electric Repair';

        $this->load->view('pages/electrical_services', $data);
    }
    public function waterTankCleaning() {
       $data                = array();
        $data['css_files']  = array();
        $data['js_files']   = array();
        $data['meta_title'] = 'Water Tank Cleaning Companies Dubai | Water Tank Cleaning';
        $data['meta_description'] = 'Water tank cleaning where water is cleaned for safe consumption and usage. The go-to among water tank cleaning companies in Dubai; providing the best cleaners.';
		$data['meta_keywords'] = 'water tank cleaning dubai,water tank cleaning companies in dubai,water tank cleaning';

        $this->load->view('pages/water_tank_cleaning', $data);
    }
    
    public function handymanServices() {
       $data                = array();
        $data['css_files']  = array();
        $data['js_files']   = array();
        $data['meta_title'] = 'Handyman In Dubai | Handyman Services Dubai';
        $data['meta_description'] = 'Handyman services in Dubai, with professionals who can perform a plethora of miscellaneous services for an affordable price in Dubai.';
		$data['meta_keywords'] = 'Handyman Dubai,Handyman Services Dubai,handyman services';

        $this->load->view('pages/handymanServices', $data);
    }
    
    public function faq() {
       $data                = array();
        $data['css_files']  = array();
        $data['js_files']   = array();
        $data['meta_title'] = '';
        $data['meta_description'] = '';
		$data['meta_keywords'] = '';

        $this->load->view('pages/faq', $data);
    }
    
    public function privacy_policy(){
        $data                = array();
        $data['css_files']  = array();
        $data['js_files']   = array();
        $data['meta_title'] = '';
        $data['meta_description'] = '';
		$data['meta_keywords'] = '';

        $this->load->view('pages/privacy_policy', $data);
    }
    public function testimonial(){
        $data                = array();
        $data['css_files']  = array();
        $data['js_files']   = array();
        $data['meta_title'] = '';
        $data['meta_description'] = '';
		$data['meta_keywords'] = '';

        $this->load->view('pages/testimonial', $data);
    }
    
    public function refund_cancelleation()
    {
        $data                = array();
        $data['css_files']  = array();
        $data['js_files']   = array();
        $data['meta_title'] = '';
        $data['meta_description'] = '';
		$data['meta_keywords'] = '';

        $this->load->view('pages/refund_cancelleation', $data);
    }
    
    public function delivery_shipment_policy()
    {
        $data                = array();
        $data['css_files']  = array();
        $data['js_files']   = array();
        $data['meta_title'] = '';
        $data['meta_description'] = '';
		$data['meta_keywords'] = '';

        $this->load->view('pages/delivery_shipment_policy', $data);
    }
    
    public function office_cleaning()
    {
        $data                = array();
        $data['css_files']  = array();
        $data['js_files']   = array();
        $data['meta_title'] = 'Office Cleaning Services Dubai | Commercial Cleaning Services';
        $data['meta_description'] = 'Office and commercial cleaning service that can clean your office spaces as well as warehouses, the very best among commercial cleaning companies.  ';
		$data['meta_keywords'] = 'office cleaning services dubai,commercial cleaning,warehouse cleaning,commercial cleaning companies in dubai';

        $this->load->view('pages/office_cleaning', $data);
    }
    
     public function painting_marina()
    {
        $data                = array();
        $data['css_files']  = array();
        $data['js_files']   = array();
        $data['meta_title'] = 'Painting Services Dubai Marina| Painters in Dubai Marina';
        $data['meta_description'] = 'House painting is more about your selves than us, as we at spectrum painting services provides painting services in dubai';
        $data['meta_keywords'] = '';

        $this->load->view('pages/painting_marina', $data);
    }

    public function maid_jabel()
    {

        $data                = array();
        $data['css_files']  = array();
        $data['js_files']   = array();
        $data['meta_title'] = 'Maid service in Dubai | Part time maids in Dubai | Jabel Ali ';
        $data['meta_description'] = 'Maid services in Jabel ali Dubai with a fruitful cleaning.We working part time at hourly rate to make your house look beautiful and clean.';
        $data['meta_keywords'] = '';        
        $this->load->view('pages/maid_cleaning _jabel_ali', $data);

    }
    public function maid_falcon()
    {

        $data                = array();
        $data['css_files']  = array();
        $data['js_files']   = array();
        $data['meta_title'] = 'Cleaning company dubai | Part time maids in Dubai | Falcon city ';
        $data['meta_description'] = 'We have been a part of the maid service industry for a long time and we can provide you with the best cleaning service in Dubai at affordable rates.';
        $data['meta_keywords'] = '';        
        $this->load->view('pages/maid_cleaning _falcon', $data);

    }

    public function contact_request()
    {
        $contact_name = trim($this->input->post('full_name'));
        $contact_number = trim($this->input->post('contact_number'));
        $message = trim($this->input->post('message'));
        $emailid = trim($this->input->post('email_id'));

        $this->sent_main_contact_to_admin_mail($contact_name, $contact_number, $message, $emailid);


        $response = array();
        $response['status'] = 'success';
        $response['message'] = 'Message sent successfully...';

        echo json_encode($response);
        exit();
    }
    
    function sent_main_contact_to_admin_mail($customer_name, $customer_mobile, $message, $emailid)
    {   
        $this->load->library('email');            

        $c_message = 'Customer : <i>' . $customer_name . '</i>';
        $c_message .= '<br />Mobile : <i>' . $customer_mobile . '</i>';
        $c_message .= '<br />Email : <i>' . $emailid . '</i>';
        $c_message .= '<br />Message : <i>' . $message . '</i>';            
        $b_message = $c_message;
        
        $html = '<html>
                <head>
                </head>
                <body>
                    <div style="margin:0;padding:0;background-color:#f2f2f2;min-height:100%!important;width:100%!important">
                        <center>
                            <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="border-collapse:collapse;margin:0;padding:0;background-color:#f2f2f2;height:100%!important;width:100%!important">
                                <tbody>
                                  <tr>
                                    <td align="center" valign="top" style="margin:0;padding:20px;border-top:0;height:100%!important;width:100%!important"><table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;border:0">
                                        <tbody>
                                          <tr>
                                            <td align="center" valign="top"><table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;background-color:#ffffff;border-top:0;border-bottom:0">
                                                <tbody>
                                                  <tr>
                                                    <td valign="top"></td>
                                                  </tr>
                                                </tbody>
                                              </table></td>
                                          </tr>
                                          <tr>
                                            <td align="center" valign="top"><table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;background-color:#ffffff;border-top:0;border-bottom:0">
                                                <tbody>
                                                  <tr>
                                                    <td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                                                        <tbody>
                                                          <tr>
                                                            <td valign="top" style="padding:0px"><table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse">
                                                                <tbody>
                                                                  <tr>
                                                                    <td valign="top" style="padding-right:0px;padding-left:0px;padding-top:0;padding-bottom:0"><img align="left" alt="" src="'. base_url().'assets/images/email_banner.jpg" width="600" style="max-width:1144px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;outline:none;text-decoration:none" class="CToWUd a6T" tabindex="0">
                                                                      <div class="a6S" dir="ltr" style="opacity: 0.01; left: 632px; top: 304px;">
                                                                        <div id=":1dx" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" title="Download" role="button" tabindex="0" aria-label="Download attachment " data-tooltip-class="a1V">
                                                                          <div class="aSK J-J5-Ji aYr"></div>
                                                                        </div>
                                                                      </div></td>
                                                                  </tr>
                                                                </tbody>
                                                              </table></td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                      <table width="600" cellspacing="0" cellpadding="0" border="0" align="left" style="border-collapse:collapse">
                               <tbody>
                               <tr>

                               <td valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px;color:#606060;font-family:Helvetica;font-size:15px;line-height:150%;text-align:left">

                               <br>
                                Dear Admin,<br>
                                <br>New Enquiry Received
                                <br>
                                <br> ' . $b_message . '<br>

                                <br>
                                    Thanks & Regards<br/>
                                    Spectrum Services<br />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                      <tbody>
                        <tr>
                          <td valign="top"><table align="left" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse">
                              <tbody>
                                <tr>
                                  <td valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px;color:#606060;font-family:Helvetica;font-size:15px;line-height:150%;text-align:left"><div style="text-align:center"><em style="color:#606060;font-family:helvetica;font-size:11px;line-height:15.6199998855591px;text-align:center">Copyright &copy; 2017 Spectrum Services.&nbsp;All rights reserved.</em><br style="color:#606060;font-family:Helvetica;font-size:11px;text-align:center;line-height:15.6199998855591px">
                                      <span style="color:#606060;font-family:helvetica;font-size:11px;line-height:15.6199998855591px;text-align:center">You are receiving this email because you signed up to Spectrum Services</span></div>
                                    </td>
                                </tr>
                              </tbody>
                            </table></td>
                        </tr>
                      </tbody>
                    </table>
                                                      </td>
                                                  </tr>
                                                </tbody>
                                              </table></td>
                                          </tr>

                                        </tbody>
                                      </table></td>
                                  </tr>
                                </tbody>
                              </table>
                        </center>
                    </div>
                </body>
                </html>'; 
            $config['mailtype'] = 'html';
            $config['charset'] = 'iso-8859-1';
            $this->email->initialize($config);

            //$this->email->to('info@urbanhousekeeping.com', 'Urban Housekeeping');
            //$this->email->from('support@homemaids.org', 'Homemaids Support Team');
            //$this->email->from('web@spectrumservices.ae', $customer_name);
            $this->email->from('info@spectrumservices.ae', $customer_name);
            //$this->email->cc('geethumol@azinova.info');
            //$this->email->to('info@urbanhousekeeping.com', 'Urban Housekeeping');
            $this->email->to('info@spectrumservices.ae,specservices2018@gmail.com','Spectrum');
            //$this->email->cc('vishnu.m@azinova.info','Spectrum');
            //$this->email->bcc('vishnu.m@azinova.info','Pick Maid');

            $this->email->subject('New Enquiry Recieved');

            $this->email->message($html);

            $this->email->send();
            
            
            //echo $this->email->print_debugger();
    }
    public function comingsoon() {
       $data                = array();
        $data['css_files']  = array();
        $data['js_files']   = array();
        $data['meta_title'] = '';
        $data['meta_description'] = '';
		$data['meta_keywords'] = '';

        $this->load->view('pages/coming-soon', $data);
    }
    public function delivery() {
       $data                = array();
        $data['css_files']  = array();
        $data['js_files']   = array();
        $data['meta_title'] = '';
        $data['meta_description'] = '';
		$data['meta_keywords'] = '';

        $this->load->view('pages/coming-soon', $data);
    }
    public function terms() {
       $data                = array();
        $data['css_files']  = array();
        $data['js_files']   = array();
        $data['meta_title'] = '';
        $data['meta_description'] = '';
		$data['meta_keywords'] = '';

        $this->load->view('pages/terms', $data);
    }
    public function pricing() {
       $data                = array();
        $data['css_files']  = array();
        $data['js_files']   = array();
        $data['meta_title'] = '';
        $data['meta_description'] = '';
		$data['meta_keywords'] = '';

        $this->load->view('pages/pricing', $data);
    }
    
    
    public function packages() {
       $data                = array();
        $data['css_files']  = array();
        $data['js_files']   = array('packages.js');
        $data['meta_title'] = '';
        $data['meta_description'] = '';
		$data['meta_keywords'] = '';

        $this->load->view('pages/packages', $data);
    }
	
	public function offer_upload() {
		
       $data                = array();
	   
	   if($this->input->post('submit'))
	   {
		   $config = array(
				'upload_path' => "./offerimage/",
				'allowed_types' => "gif|jpg|png|jpeg",
				'overwrite' => TRUE,
				'file_name' => 'offerimage'
			);
			$this->load->library('upload', $config);
			$img = $this->input->post('userfile');
			if ($this->upload->do_upload('userfile')) {
                    $this->session->set_flashdata('upload_success', "Uploaded Successfully");
					redirect('pages/offer_upload');
                    
                } else {
                    $error = array(
                        'error' => $this->upload->display_errors()
                    );
					$this->session->set_flashdata('upload_error', "Something went wrong...");
					redirect('pages/offer_upload');
                }
		}
        $data['css_files']  = array();
        $data['js_files']   = array();
        $data['meta_title'] = '';
        $data['meta_description'] = '';
		$data['meta_keywords'] = '';

        $this->load->view('pages/offer_upload', $data);
    }
	
	public function offer_upload_process()
	{
		
		$config = array(
			'upload_path' => "./offerimage/",
			'allowed_types' => "gif|jpg|png|jpeg|pdf",
			'overwrite' => TRUE,
			'encrypt_name' => TRUE,
			'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
			'max_height' => "768",
			'max_width' => "1024"
		);
		
		$this->load->library('upload', $config);
		if ($this->upload->do_upload()) {
			$data = array(
				'upload_data' => $this->upload->data()
			);
			$this->load->view('upload_success', $data);
		} else {
			$error = array(
				'error' => $this->upload->display_errors()
			);
			$this->load->view('file_view', $error);
		}
	}
   
    
    
    
    function sent_package_mail()
    {   
        $this->load->library('email');            
        $customer_name=trim($this->input->post('package_name'));
        $customer_mobile=trim($this->input->post('package_number'));
        $emailid=trim($this->input->post('package_email'));
        $customer_area=trim($this->input->post('package_area'));
        $package_name=trim($this->input->post('pckg_name'));
        $rooms=trim($this->input->post('package_rooms'));
        
        
        $c_message = 'Package : <i><b>' . $package_name . '</b></i>';
        $c_message .= '<br />Customer : <i><b>' . $customer_name . '</b></i>';
        $c_message .= '<br />Mobile : <i><b>' . $customer_mobile . '</b></i>';
        $c_message .= '<br />Email : <i><b>' . $emailid . '</b></i>';
        $c_message .= '<br />Area : <i><b>' . $customer_area . '</b></i>';
        $c_message .= '<br />Rooms : <i><b>' . $rooms . '</b></i>';            
        $b_message = $c_message;
        
        $html = '<html>
                <head>
                </head>
                <body>
                    <div style="margin:0;padding:0;background-color:#f2f2f2;min-height:100%!important;width:100%!important">
                        <center>
                            <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="border-collapse:collapse;margin:0;padding:0;background-color:#f2f2f2;height:100%!important;width:100%!important">
                                <tbody>
                                  <tr>
                                    <td align="center" valign="top" style="margin:0;padding:20px;border-top:0;height:100%!important;width:100%!important"><table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;border:0">
                                        <tbody>
                                          <tr>
                                            <td align="center" valign="top"><table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;background-color:#ffffff;border-top:0;border-bottom:0">
                                                <tbody>
                                                  <tr>
                                                    <td valign="top"></td>
                                                  </tr>
                                                </tbody>
                                              </table></td>
                                          </tr>
                                          <tr>
                                            <td align="center" valign="top"><table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;background-color:#ffffff;border-top:0;border-bottom:0">
                                                <tbody>
                                                  <tr>
                                                    <td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                                                        <tbody>
                                                          <tr>
                                                            <td valign="top" style="padding:0px"><table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse">
                                                                <tbody>
                                                                  <tr>
                                                                    <td valign="top" style="padding-right:0px;padding-left:0px;padding-top:0;padding-bottom:0"><img align="left" alt="" src="'. base_url().'assets/images/email_banner.jpg" width="600" style="max-width:1144px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;outline:none;text-decoration:none" class="CToWUd a6T" tabindex="0">
                                                                      <div class="a6S" dir="ltr" style="opacity: 0.01; left: 632px; top: 304px;">
                                                                        <div id=":1dx" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" title="Download" role="button" tabindex="0" aria-label="Download attachment " data-tooltip-class="a1V">
                                                                          <div class="aSK J-J5-Ji aYr"></div>
                                                                        </div>
                                                                      </div></td>
                                                                  </tr>
                                                                </tbody>
                                                              </table></td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                      <table width="600" cellspacing="0" cellpadding="0" border="0" align="left" style="border-collapse:collapse">
                               <tbody>
                               <tr>

                               <td valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px;color:#606060;font-family:Helvetica;font-size:15px;line-height:150%;text-align:left">

                               <br>
                                Dear Admin,<br>
                                <br>New Package Enquiry Received
                                <br>
                                <br> ' . $b_message . '<br>

                                <br>
                                    Thanks & Regards<br/>
                                    Spectrum Services<br />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                      <tbody>
                        <tr>
                          <td valign="top"><table align="left" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse">
                              <tbody>
                                <tr>
                                  <td valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px;color:#606060;font-family:Helvetica;font-size:15px;line-height:150%;text-align:left"><div style="text-align:center"><em style="color:#606060;font-family:helvetica;font-size:11px;line-height:15.6199998855591px;text-align:center">Copyright &copy; 2017 Spectrum Services.&nbsp;All rights reserved.</em><br style="color:#606060;font-family:Helvetica;font-size:11px;text-align:center;line-height:15.6199998855591px">
                                      <span style="color:#606060;font-family:helvetica;font-size:11px;line-height:15.6199998855591px;text-align:center">You are receiving this email because you signed up to Spectrum Services</span></div>
                                    </td>
                                </tr>
                              </tbody>
                            </table></td>
                        </tr>
                      </tbody>
                    </table>
                                                      </td>
                                                  </tr>
                                                </tbody>
                                              </table></td>
                                          </tr>

                                        </tbody>
                                      </table></td>
                                  </tr>
                                </tbody>
                              </table>
                        </center>
                    </div>
                </body>
                </html>'; 
            $config['mailtype'] = 'html';
            $config['charset'] = 'iso-8859-1';
            $this->email->initialize($config);

            //$this->email->to('info@urbanhousekeeping.com', 'Urban Housekeeping');
            //$this->email->from('support@homemaids.org', 'Homemaids Support Team');
            //$this->email->from('web@spectrumservices.ae', $customer_name);
            $this->email->from('info@spectrumservices.ae', $customer_name);
            //$this->email->to('jose.robert@azinova.info');
            //$this->email->to('info@urbanhousekeeping.com', 'Urban Housekeeping');
            $this->email->to('info@spectrumservices.ae,specservices2018@gmail.com','Spectrum');
            $this->email->cc('ahmed@spectrumservices.ae','Spectrum');
            //$this->email->bcc('vishnu.m@azinova.info','Pick Maid');

            $this->email->subject('New Package Enquiry Recieved');

            $this->email->message($html);

            if($this->email->send())
              {echo "success";}
            else
              {echo "error";}
            
            exit();
            //echo $this->email->print_debugger();
    }
    
    
    
}
